﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CalibrationEPlusE
{
    public partial class frmCalibrationSettings : Form
    {
        public frmCalibrationSettings()
        {
            InitializeComponent();
        }

        public static class ProgramSettings
        {
            //Current User using the program.
            public static string CurrentUser = "";

            //Referance Sensors
            public static string calibrationReferanceSensor = null;
            public static string EPlusEReferanceComPort = null;
            public static string HMP234ReferanceComPort = null;

            //Raidosonde Com ports
            public static string RadiosondeStartingComPort = null;
            public static int NumberofRadiosondes = 0;

            //Chamber Settings
            public static string ChamberController = null;
            public static string ChamberControllerComPort = null;

            //Humidity Settings
            public static string humiditySource = null;
            //Thunder 3900 Low Humidity Generator
            public static string ThunderComPort = null;

            //Calc server
            public static string CalcServerAddress = null;

            //Raw File Folder Storage Location
            public static string RAWFileLOC = null;

            //Coef cal storage loacation
            public static string CoefLOC = null;

            //Alert settings
            public static string AudioFile = null;
            public static bool alertAudio = false;
            public static string TextMessageNumber = null;
            public static bool alertTextMessage = false;
            public static string EmailAddress = null;
            public static bool alertEmail = false;
            public static string OutgoingAddress = "";
            public static string OutgoingServer = "";
            public static string OutgoingPort = "";
            public static string OutgoingUsername = "";
            public static string OutgoingPassword = "";
            public static bool OutgoingSSL = false;

            //Colors for pass, fails, radiosonde types, and other things like that.
            public static Color colorPass = System.Drawing.Color.Green;
            public static Color colorFail = System.Drawing.Color.Red;
            public static Color colorReady = System.Drawing.Color.Red;
            public static Color colorInProcess = System.Drawing.Color.Red;
            public static Color colorRepeatElement = System.Drawing.Color.Red;

            //Calibration SetPoints
            public static string[,] calibrationSetPoint;

            //Radiosonde Acceptable Tolerances
            public static double tolAcceptableAirTempDiff = 0;
            public static double tolHumidityFreqMean = 0;
            public static double tolHumidityFrewPlusMinus = 0;

            //Valve control settings
            public static string modeValve = "";

            //Relay controller settings
            public static string relayComPort = "";
            public static string[,] relays = new string[16, 3];

        }

        private void frmCalibrationSettings_Load(object sender, EventArgs e)
        {
            //Loading setting from the config file.
            loadSettingsFromFile();

            //Updating the setting to the screen.
            updateSettingToScreen();

            //Loding comboboxes with all avaiable com ports.
            loadComPortToComboBox();

            //Selecting the current referance sensor to use during calibration.
            selectedReferanceSensor();
        }

        public void loadSettingsFromFile()
        {
            int calibrationSets = 0;
            string configLine = "";
            TextReader ProgramSettingsReader = new StreamReader("EPlusESettings.cfg");
            // read lines of text

            configLine = ProgramSettingsReader.ReadToEnd();
            configLine = configLine.Replace("\r", "");
            string[] configElement = configLine.Split('\n', '=');

            // close the stream
            ProgramSettingsReader.Close();

            //Setting up the global settings.
            for (int i = 0; i < configElement.Length; i++)
            {
                switch (configElement[i])
                {
                    case "calibrationReferanceSensor":
                        ProgramSettings.calibrationReferanceSensor = configElement[i + 1];
                        break;

                    case "EPlusEReferanceComPort":
                        ProgramSettings.EPlusEReferanceComPort = configElement[i + 1];
                        break;

                    case "HMP234ReferanceComPort":
                        ProgramSettings.HMP234ReferanceComPort = configElement[i + 1];
                        break;

                    case "RadiosondeStartingComPort":
                        ProgramSettings.RadiosondeStartingComPort = configElement[i + 1];
                        break;

                    case "NumberofRadiosondes":
                        ProgramSettings.NumberofRadiosondes = Convert.ToInt16(configElement[i + 1]);
                        break;

                    case "ChamberController":
                        ProgramSettings.ChamberController = configElement[i + 1];
                        break;

                    case "ChamberControllerComPort":
                        ProgramSettings.ChamberControllerComPort = configElement[i + 1];
                        break;

                    case "ThunderComPort":
                        ProgramSettings.ThunderComPort = configElement[i + 1];
                        break;

                    case "CalcServerAddress":
                        ProgramSettings.CalcServerAddress = configElement[i + 1];
                        break;

                    case "RAWFileLOC":
                        ProgramSettings.RAWFileLOC = configElement[i + 1];
                        break;

                    case "CoefLOC":
                        ProgramSettings.CoefLOC = configElement[i + 1];
                        break;

                    case "AudioFile":
                        ProgramSettings.AudioFile = configElement[i + 1];
                        break;

                    case "AlertAudio":
                        ProgramSettings.alertAudio = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case "TextMessageNumber":
                        ProgramSettings.TextMessageNumber = configElement[i + 1];
                        break;

                    case "AlertTextMessage":
                        ProgramSettings.alertTextMessage = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case "EmailAddress":
                        ProgramSettings.EmailAddress = configElement[i + 1];
                        break;

                    case "AlertEmail":
                        ProgramSettings.alertEmail = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case "Pass":
                        ProgramSettings.colorPass = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "Fail":
                        ProgramSettings.colorFail = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "Ready":
                        ProgramSettings.colorReady = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "InProcess":
                        ProgramSettings.colorInProcess = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "RepeatElement":
                        ProgramSettings.colorRepeatElement = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "CalibrationStep":
                        calibrationSets++;
                        break;

                    case "AcceptableAirTempDiff":
                        ProgramSettings.tolAcceptableAirTempDiff = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case "HumidityFreqMean":
                        ProgramSettings.tolHumidityFreqMean = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case "HumidityFreqPlusMinus":
                        ProgramSettings.tolHumidityFrewPlusMinus = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case "OutgoingAddress":
                        ProgramSettings.OutgoingAddress = configElement[i + 1];
                        break;

                    case "OutgoingServer":
                        ProgramSettings.OutgoingServer = configElement[i + 1];
                        break;

                    case "OutgoingPort":
                        ProgramSettings.OutgoingPort = configElement[i + 1];
                        break;

                    case "OutgoingUsername":
                        ProgramSettings.OutgoingUsername = configElement[i + 1];
                        break;

                    case "OutgoingPassword":
                        ProgramSettings.OutgoingPassword = configElement[i + 1];
                        break;

                    case "OutgoingSSL":
                        ProgramSettings.OutgoingSSL = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case "ValveMode":
                        ProgramSettings.modeValve = configElement[i + 1];
                        break;

                    case "relayComPort":
                        ProgramSettings.relayComPort = configElement[i + 1];
                        break;

                    case "HumiditySource":
                        ProgramSettings.humiditySource = configElement[i + 1];
                        break;

                }

                //Loading relay settings
                if (configElement[i] == "relay")
                {
                    string[] relayElement = configElement[i + 1].Split(',');
                    for (int loadRelay = 0; loadRelay < relayElement.Length; loadRelay++)
                    {
                        ProgramSettings.relays[Convert.ToInt16(relayElement[0]), loadRelay] = relayElement[loadRelay];
                    }

                }
            }

            //Loading calibration steps.
            ProgramSettings.calibrationSetPoint = new string[calibrationSets, 22];
            int iParent = 0;
            for (int x = 0; x < configElement.Length; x++)
            {
                if (configElement[x] == "CalibrationStep")
                {
                    string[] calibrationSetElement = configElement[x + 1].Split(',');
                    for (int j = 0; j < calibrationSetElement.Length; j++)
                    {
                        ProgramSettings.calibrationSetPoint[iParent, j] = calibrationSetElement[j];
                    }
                    iParent++;
                }
            }

            //Loading the humidity source settings.
            switch (ProgramSettings.humiditySource)
            {
                case"Thermotron":
                    radioButtonRHSourceThermotron.Checked = true;
                    break;

                case"Thunder":
                    radioButtonRHSourceThunder.Checked = true;
                    break;
            }

        }

        public void updateSettingToScreen()
        {
            comboBoxChamberController.Text = ProgramSettings.ChamberController;
            comboBoxComPortChamber.Text = ProgramSettings.ChamberControllerComPort;
            comboBoxComPortEPlusE.Text = ProgramSettings.EPlusEReferanceComPort;
            comboBoxComPortHMP234.Text = ProgramSettings.HMP234ReferanceComPort;
            comboBoxComPortRadiosondeEnd.Text = "COM" + (Convert.ToInt16(ProgramSettings.RadiosondeStartingComPort.Substring(3, (ProgramSettings.RadiosondeStartingComPort.Length - 3))) + ProgramSettings.NumberofRadiosondes - 1).ToString();
            comboBoxComPortRadiosondeStart.Text = ProgramSettings.RadiosondeStartingComPort;

            //Radiosonde Tolerances
            textBoxRadiosondeAirTempDiff.Text = ProgramSettings.tolAcceptableAirTempDiff.ToString();
            textBoxRadiosondeUFreqMean.Text = ProgramSettings.tolHumidityFreqMean.ToString();
            textBoxRadiosondeUFreqPlusMinus.Text = ProgramSettings.tolHumidityFrewPlusMinus.ToString();

            comboBoxComPortRelay.Text = ProgramSettings.relayComPort;
            comboBoxComPortThunder.Text = ProgramSettings.ThunderComPort;
            textBoxCalcServerAddress.Text = ProgramSettings.CalcServerAddress;
            textBoxRAWStorageLocation.Text = ProgramSettings.RAWFileLOC;
            textBoxCoefStorageLoc.Text = ProgramSettings.CoefLOC;
            
            //Audio Alert Settings
            textBoxAlertAudioSoundFile.Text = ProgramSettings.AudioFile;
            checkBoxAlertAudioEnable.Checked = ProgramSettings.alertAudio;

            //Text Message Alert Settings
            textBoxAlertPhoneNumber.Text = ProgramSettings.TextMessageNumber;
            checkBoxAlertTextMessageEnable.Checked = ProgramSettings.alertTextMessage;

            //Email Alert Settings
            textBoxAlertEmailAddress.Text = ProgramSettings.EmailAddress;
            checkBoxAlertEmailEnable.Checked = ProgramSettings.alertEmail;

            //Sending Colors to the screen
            panelColorFail.BackColor = ProgramSettings.colorFail;
            panelColorInProcess.BackColor = ProgramSettings.colorInProcess;
            panelColorPass.BackColor = ProgramSettings.colorPass;
            panelColorReady.BackColor = ProgramSettings.colorReady;
            panelColorRepeat.BackColor = ProgramSettings.colorRepeatElement;

            //updateing server options.
            textBoxEmailAddress.Text = ProgramSettings.OutgoingAddress;
            textBoxServerAddress.Text = ProgramSettings.OutgoingServer;
            textBoxServerPort.Text = ProgramSettings.OutgoingPort;
            textBoxEmailUsername.Text = ProgramSettings.OutgoingUsername;
            textBoxEmailPassword.Text = ProgramSettings.OutgoingPassword;
            checkBoxSSL.Checked = ProgramSettings.OutgoingSSL;

            comboBoxValveControl.Text = ProgramSettings.modeValve;
            

            //Sending calibration points to the screen.
            if (ProgramSettings.calibrationSetPoint.Length / 22 > 1)
            {
                dataGridViewCalibrationPoints.Rows.Add((ProgramSettings.calibrationSetPoint.Length / 22)-1);
            }
            
            //Sending data to screen.
            for (int i = 0; i < (ProgramSettings.calibrationSetPoint.Length/22); i++)
            {
                for (int j = 0; j < 22; j++)
                {
            
                    dataGridViewCalibrationPoints.Rows[i].Cells[j].Value = ProgramSettings.calibrationSetPoint[i, j];
                   
                }
            }

            //Sending Relay configuration to screen.
            int counterActiveRelays = 0;
            string usedRelays = "";
            for (int findRelays = 0; findRelays < 16; findRelays++)
            {
                if (ProgramSettings.relays[findRelays, 0] != null)
                { 
                    counterActiveRelays++;
                    usedRelays += findRelays.ToString() + " ";
                }
                
            }
            
            dataGridViewRelays.Rows.Add(counterActiveRelays - 1);
            string [] usedRelaysItems = usedRelays.Split(' ');

            for (int displayRelay = 0; displayRelay <= counterActiveRelays-1; displayRelay++)
            {
                for (int fillRelayGrid = 0; fillRelayGrid < 3; fillRelayGrid++)
                {
                    dataGridViewRelays.Rows[displayRelay].Cells[fillRelayGrid].Value = ProgramSettings.relays[Convert.ToInt16(usedRelaysItems[displayRelay]), fillRelayGrid];
                }
            }


            
        }

        public void loadComPortToComboBox()
        {
            string[] allComPorts = System.IO.Ports.SerialPort.GetPortNames();

            for (int i = 0; i < allComPorts.Length; i++)
            {
                comboBoxComPortChamber.Items.Add(allComPorts[i]);
                comboBoxComPortEPlusE.Items.Add(allComPorts[i]);
                comboBoxComPortHMP234.Items.Add(allComPorts[i]);
                comboBoxComPortRadiosondeStart.Items.Add(allComPorts[i]);
                comboBoxComPortRadiosondeEnd.Items.Add(allComPorts[i]);
                comboBoxComPortRelay.Items.Add(allComPorts[i]);
                comboBoxComPortThunder.Items.Add(allComPorts[i]);
            }
        }

        public void selectedReferanceSensor()
        {
            switch (ProgramSettings.calibrationReferanceSensor)
            {
                case "HMP234":
                    radioButtonSensorHMP234.Checked = true;
                    break;

                case "EPlusE":
                    radioButtonSensorEE31.Checked = true;
                    break;
            }
        }

        public void saveSettings()
        {
            // create a writer and open the file
            FileInfo t = new FileInfo("EPlusESettings.cfg");

            // write a line of text to the file
            StreamWriter programSettingFile = t.CreateText();
            programSettingFile.WriteLine("#This is the settings file for the E+E calibration software. Editing this file could cause the program to not load.");
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Referance Sensors");
            programSettingFile.WriteLine("EPlusEReferanceComPort=" + comboBoxComPortEPlusE.Text);
            programSettingFile.WriteLine("HMP234ReferanceComPort=" + comboBoxComPortHMP234.Text);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Sensor to be used for calibration");

            //Writing the type of sensor option selected.
            if (radioButtonSensorEE31.Checked) { programSettingFile.WriteLine("calibrationReferanceSensor=EPlusE"); }
            if (radioButtonSensorHMP234.Checked) { programSettingFile.WriteLine("calibrationReferanceSensor=HMP234"); }
            
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Raidosonde Com ports");
            programSettingFile.WriteLine("RadiosondeStartingComPort=" + comboBoxComPortRadiosondeStart.Text);
            programSettingFile.WriteLine("NumberofRadiosondes=" + Convert.ToString((Convert.ToInt16(comboBoxComPortRadiosondeEnd.Text.Substring(3,comboBoxComPortRadiosondeEnd.Text.Length-3)) - Convert.ToInt16(comboBoxComPortRadiosondeStart.Text.Substring(3,comboBoxComPortRadiosondeStart.Text.Length-3)) + 1)));
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Radiosonde Acceptable Tolerances");
            programSettingFile.WriteLine("AcceptableAirTempDiff=" + textBoxRadiosondeAirTempDiff.Text);
            programSettingFile.WriteLine("HumidityFreqMean=" + textBoxRadiosondeUFreqMean.Text);
            programSettingFile.WriteLine("HumidityFreqPlusMinus=" + textBoxRadiosondeUFreqPlusMinus.Text);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Valve control mode");
            programSettingFile.WriteLine("ValveMode=" + comboBoxValveControl.Text);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Chamber Settings");
            programSettingFile.WriteLine("ChamberController=" + comboBoxChamberController.Text);
            programSettingFile.WriteLine("ChamberControllerComPort=" + comboBoxComPortChamber.Text);
            programSettingFile.WriteLine("");
            
            programSettingFile.WriteLine("#Relay Controller Settings");
            programSettingFile.WriteLine("relayComPort=" + comboBoxComPortRelay.Text);
            //Writing all the valid relays and the settings.
            for(int relays = 0; relays < 16; relays++)
            {
                if (ProgramSettings.relays[relays, 0] != null)
                {
                    programSettingFile.WriteLine("relay=" + ProgramSettings.relays[relays,0] + "," + ProgramSettings.relays[relays,1] + "," + ProgramSettings.relays[relays,2]);
                }
            }

            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Humidity Generator");
            programSettingFile.WriteLine("HumiditySource=Thunder");

            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Thunder 3900 Low Humidity Generator");
            programSettingFile.WriteLine("ThunderComPort=" + comboBoxComPortThunder.Text);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Calc Server Address");
            programSettingFile.WriteLine("CalcServerAddress=" + textBoxCalcServerAddress.Text);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Raw file storage loaction");
            programSettingFile.WriteLine("RAWFileLOC=" + textBoxRAWStorageLocation.Text);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Coef Storage Location");
            programSettingFile.WriteLine("CoefLOC=" + textBoxCoefStorageLoc.Text);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Alert settings");
            programSettingFile.WriteLine("AudioFile=" + textBoxAlertAudioSoundFile.Text);
            programSettingFile.WriteLine("AlertAudio=" + checkBoxAlertAudioEnable.Checked);
            programSettingFile.WriteLine("TextMessageNumber=" + textBoxAlertPhoneNumber.Text);
            programSettingFile.WriteLine("AlertTextMessage=" + checkBoxAlertTextMessageEnable.Checked);
            programSettingFile.WriteLine("EmailAddress=" + textBoxAlertEmailAddress.Text);
            programSettingFile.WriteLine("AlertEmail=" + checkBoxAlertEmailEnable.Checked);
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("OutgoingAddress=" + textBoxEmailAddress.Text);
            programSettingFile.WriteLine("OutgoingServer=" + textBoxServerAddress.Text);
            programSettingFile.WriteLine("OutgoingPort=" + textBoxServerPort.Text);
            programSettingFile.WriteLine("OutgoingUsername=" + textBoxEmailUsername.Text);
            programSettingFile.WriteLine("OutgoingPassword=" + textBoxEmailPassword.Text);
            programSettingFile.WriteLine("OutgoingSSL=" + checkBoxSSL.Checked.ToString());
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Colors");
            programSettingFile.WriteLine("Pass=" + panelColorPass.BackColor.ToArgb().ToString());
            programSettingFile.WriteLine("Fail=" + panelColorFail.BackColor.ToArgb().ToString());
            programSettingFile.WriteLine("Ready=" + panelColorReady.BackColor.ToArgb().ToString());
            programSettingFile.WriteLine("InProcess=" + panelColorInProcess.BackColor.ToArgb().ToString());
            programSettingFile.WriteLine("RepeatElement=" + panelColorRepeat.BackColor.ToArgb().ToString());
            programSettingFile.WriteLine("");
            programSettingFile.WriteLine("#Calibration points");
            programSettingFile.WriteLine("#Step,Enclosure Temp Set Point,Acceptable Enclosure Temp Range,Enclosure Temp Stability Rate,Chamber Temp Set Point,Chamber Temp Range,Chamber Temp Stability Rate,Temp Stability Time,Enclosure Humidity Set Point,Enclosure Humidity Range,Enclosure Humidity Stability Rate,Humidity Stability Time,Thunder SATUR psi,Thunder SATIR °C,Thunder Flow,Thunder Mode,ValveA,ValveB,ValveC,Valve1,Valve2,Valve3");

            //Saveing calibration steps
            for(int i = 0; i<dataGridViewCalibrationPoints.Rows.Count; i++)
            {
                if (dataGridViewCalibrationPoints.Rows[i].Cells[0].Value == null)
                {
                    break;
                }

                programSettingFile.WriteLine("CalibrationStep=" + dataGridViewCalibrationPoints.Rows[i].Cells[0].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[1].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[2].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[3].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[4].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[5].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[6].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[7].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[8].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[9].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[10].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[11].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[12].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[13].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[14].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[15].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[16].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[17].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[18].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[19].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[20].Value + "," +
                    dataGridViewCalibrationPoints.Rows[i].Cells[21].Value                  
                    );
            }


            // close the stream
            programSettingFile.Close();

        }

        private void buttonSaveSettings_Click(object sender, EventArgs e)
        {
            //Creating access to parent frm.
            frmCalibrationMain parent = (frmCalibrationMain)this.Owner;

            //Checking to make sure the radiosonde com port selection in not negative
            if ((Convert.ToInt16(comboBoxComPortRadiosondeEnd.Text.Substring(3, comboBoxComPortRadiosondeEnd.Text.Length - 3)) - Convert.ToInt16(comboBoxComPortRadiosondeStart.Text.Substring(3, comboBoxComPortRadiosondeStart.Text.Length - 3))) < 0)
            {
                MessageBox.Show(this, "Error in Radiosonde COM's.\nCOM Range can not go negative.\nCheck setting and try again.", "Radiosonde COM Error.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Checking to see if a calibration is in progress.
            /*
            if (parent.getStatusCalibrationInProcess())
            {
                if (MessageBox.Show(this, "Calibration currently in progress. To make changes the current calibration must be stopped.\nDo you want to cancel the current calibration?", "Calibration in progress.", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    //somehow cancel the current calibration.
                }
                else
                {
                    return;
                }
            }
            */

            //A small pause to allow the background workers servicing the components to finish.
            System.Threading.Thread.Sleep(500);

            //Saving settings/
            saveSettings();

            //Sending updated config to main form
            parent.loadSettingsFromFile();

            //Restarting Referance Sensors
            parent.startReferanceSensor();

            //Restating the humidity gen.
            parent.startHumidityGen();

            //Restarting the chamber.
            parent.startChamber();

            //Updateing the main label
            parent.updateMainStatusLabel("Settings updated.");

            //Restarting data collection
            System.Threading.Thread.Sleep(1000);
            parent.setCommunicationControls(true);

            //Close window.
            this.Dispose();

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //Updating main status label and closing window.

            //Updating the mains settings.
            frmCalibrationMain parent = (frmCalibrationMain)this.Owner;

            //Restarting Referance Sensors
            parent.startReferanceSensor();

            //Restating the humidity gen.
            parent.startHumidityGen();

            //Restarting the chamber.
            parent.startChamber();

            //Updateing the main label
            parent.updateMainStatusLabel("Settings window closed. No changes made.");

            //Restarting data collection
            System.Threading.Thread.Sleep(1000);
            parent.setCommunicationControls(true);

            //Close window.
            this.Dispose();
        }

        private void buttonRAWFileStorageLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialogRAW = new FolderBrowserDialog();

            if (folderBrowserDialogRAW.ShowDialog() == DialogResult.OK)
            {
                this.textBoxRAWStorageLocation.Text = folderBrowserDialogRAW.SelectedPath + "\\";
            }

        }

        private void buttonBrowseAudioFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog soundFileBrowser = new OpenFileDialog();

            if (soundFileBrowser.ShowDialog() == DialogResult.OK)
            {
                this.textBoxAlertAudioSoundFile.Text = soundFileBrowser.FileName;
            }
        }

        #region Code pertaining to selecting a color for an operation.

        private string selectColor()
        {
            ColorDialog colorDlg = new ColorDialog();
            string colorSelected = "";

            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                colorSelected = colorDlg.Color.ToArgb().ToString();
                return colorSelected;
            }

            return "Error";
        }

        private void panelColorPass_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorPass = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingToScreen();
            }
        }

        private void panelColorFail_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorFail = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingToScreen();
            }
        }

        private void panelColorReady_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorReady = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingToScreen();
            }
        }

        private void panelColorInProcess_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorInProcess = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingToScreen();
            }
        }

        private void panelColorRepeat_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorRepeatElement = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingToScreen();
            }
        }

        #endregion

        private void frmCalibrationSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Restarting the elements.
            buttonCancel_Click(null, null);
        }

        #region Code for working with the data grid
        private void buttonAddPoint_Click(object sender, EventArgs e)
        {
            int countRows = dataGridViewCalibrationPoints.Rows.Count - 1;
            dataGridViewCalibrationPoints.Rows.Clear();

            dataGridViewCalibrationPoints.Rows.Add(1);
            //dataGridViewCalibrationPoints.Rows[dataGridViewCalibrationPoints.Rows.Count].Cells[0].Value = dataGridViewCalibrationPoints.Rows.Count.ToString();
            updateSettingToScreen();
        }


        #endregion


    }
}
