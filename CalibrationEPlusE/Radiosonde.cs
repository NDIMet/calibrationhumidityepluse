﻿///This is a Call designed to decode InterMet Systems radiosondes.
///Writen by the Transformer Justin and Bearded Jones.
///
///Changes
///11/20/2010 - Added methods for reading and loading coefficients.
///11/23/2010 - Added methods for reading GPS and decoded data from a sonde.
///01/12/2011 - Added filtering to the Bell202 to prevent false starts of decodeding from junk data from receiver.
///02/23/2011 - Added support for bell202 maintance interface.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace Radiosonde
{
    #region iMet1 TX Area. This is used for decodeing iMet-1 radiosonde.
    public class iMet1
    {
        //
        //Constructers
        //
        public iMet1()
        {         
        }

        public iMet1(string ComPortName)
        {
            try
            {
                ComPort = new SerialPort(ComPortName, 9600, Parity.None, 8, StopBits.One);
                ComPort.NewLine = "\r\n";
            }

            catch
            {
                return;
            }
        }

        //
        //Public Variables
        //

        public SerialPort ComPort;
        public StreamWriter sondeFile;

        public string serialNumber;

        public static bool dataON;
        public static bool hasGPS;
        public static bool hasTX;

        //
        //Private Variables
        //
        private byte[] bSondeData; //Latest sonde data in byte form
        public string sSondeData; //Latest sonde data in string form (eg. 00-FF)
        //
        //Public Functions
        //

        public void Initialize()
        {
            try
            {
                if (ComPort.IsOpen != true) //This allows the radiosonde to be re-initialized if needed without error.
                {
                    ComPort.Open();
                }
                ComPort.WriteLine("$sonde,dataon");
                Thread.Sleep(50);
                ComPort.WriteLine("$sonde,binarydata");
                Thread.Sleep(50);
            }
            catch
            {
                return;
            }
        }
        //name: detectSonde
        //input: N/A
        //output: TRUE or FALSE
        //description: Checks for the existance of a radiosonde for the given object
        //returns "TRUE" if a sonde is detected, "FALSE" if not
        public bool detectSonde()
        {
            try
            {
                if (!ComPort.IsOpen)
                {
                    ComPort.Open();
                }
                string sondeResponse = getLastHexData();
                if (sondeResponse.StartsWith("79-01"))
                {
                    getSerialNum();
                    return true;
                }
                else 
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }//End of detectSonde() method

        
        //name: getSondeHexData
        //input: none
        //output: string
        //description: reads one hex data packet from the radiosonde
        //and displays it as a string. Returns "No sonde data" if it fails or times out.
        public string getNextSondeHexData()
        {
            bool startPacket = false;
            ComPort.ReadTimeout = 1200;
            bSondeData = new byte[124];
            int packetSize = 0;

            try
            {
                ComPort.DiscardInBuffer();

                while (startPacket == false)
                {
                    bSondeData[0] = (byte)ComPort.ReadByte();

                    if (bSondeData[0] == 0x79)
                    {
                        packetSize++;

                        bSondeData[1] = (byte)ComPort.ReadByte();
                        if (bSondeData[1] == 0x01)
                        {
                            packetSize++;
                            startPacket = true;
                        }
                    }

                }

                for (int i = 2; i < 130; i++)
                {
                    bSondeData[i] = (byte)ComPort.ReadByte();

                    if (bSondeData[i] == 0x10 && bSondeData[i - 1] == 0x10)
                    {
                        i--;
                    }

                    if (bSondeData[i] == 0x03 && bSondeData[i - 1] == 0x10)
                    {
                        break;
                    }

                    packetSize = i;
                }

                if (bSondeData != null)
                {
                    return BitConverter.ToString(bSondeData);
                }
                else
                {
                    return "Bad Packet";
                }

            }

            catch
            {
                return "No sonde data";
            }
        }
        
        //name: getLastHexData
        //input: none
        //output: string
        //description: reads the last hex data packet transmitted by the radiosonde
        //and displays it as a string. Returns "Packet not ready" if there is no data in the buffer.
        public string getLastHexData()
        {
            int numberOfPackets = 0;
            int packetSize = 124;
            byte[] hexData = new byte[packetSize];
            bool startPacket = false;
            ComPort.ReadTimeout = 100;

            if (ComPort.BytesToRead < packetSize)
            {
                return "Packet not ready!";
            }
            else 
            {
                //Determine the number of unread packets in the buffer
                numberOfPackets = ComPort.BytesToRead / packetSize;
            }
            
            //Clear the buffer of unread packets
            for (int i = 0; i < (numberOfPackets-1)*packetSize; i++) 
            {
                ComPort.ReadByte();
            }
            
            //Look for the start of the sonde packet
            while (startPacket == false)
            {
              
                hexData[0] = (byte)ComPort.ReadByte();
                

                if (hexData[0] == 0x79)
                {
                    hexData[1] = (byte)ComPort.ReadByte();
                    if (hexData[1] == 0x01)
                    {
                        startPacket = true;
                    }
                }
            }
            //Loop and read data until the end of the packet is reached
            for (int i = 2; i < packetSize; i++)
            {
                try  //Change made by bill to keep things running is the full packet is not recieved.
                {
                    hexData[i] = (byte)ComPort.ReadByte();
                }
                catch
                {
                    return "not ready";
                }
                //Remove '10' packet when the sonde sends out two consecutively
                if (hexData[i] == 0x10 && hexData[i - 1] == 0x10)
                {
                    i--;
                }
                //Find the end of the packet and break
                if (hexData[i] == 0x03 && hexData[i - 1] == 0x10)
                {
                    break;
                }
            }
            bSondeData = hexData;
            sSondeData = BitConverter.ToString(hexData);
            
            return sSondeData;
        }
        
        //name: convertStringToByte
        //input: string of hex data in the 'XX-YY' format
        //output: byte array
        //description: 

        private byte[] convertStringToByte(string hexData) 
        {
            string[] sBytes = hexData.Split('-');

            byte[] bHexData = new byte[sBytes.Length];

            for (int i = 0; i < bHexData.Length; i++) 
            {
                bHexData[i] = Convert.ToByte(sBytes[i], 16);
            }

            bSondeData = bHexData;

            return bHexData;
        }

        //name: getLastDecodedHexData
        //input: none
        //output: string
        //description: reads the last doecoded hex data packet transmitted by the radiosonde throw a iMet1 decoder.
        //then it displays it as a string. Returns "Packet not ready" if there is no data in the buffer.
        public string getLastDecodedHexData()
        {
            int numberOfPackets = 0;
            int packetSize = 188;
            byte[] hexData = new byte[packetSize];
            bool startPacket = false;
            ComPort.ReadTimeout = 100;

            if (ComPort.BytesToRead < packetSize)
            {
                return "Packet not ready!";
            }
            else
            {
                //Determine the number of unread packets in the buffer
                numberOfPackets = ComPort.BytesToRead / packetSize;
            }

            //Clear the buffer of unread packets
            for (int i = 0; i < (numberOfPackets - 1) * packetSize; i++)
            {
                ComPort.ReadByte();
            }

            //Look for the start of the sonde packet
            while (startPacket == false)
            {

                hexData[0] = (byte)ComPort.ReadByte();


                if (hexData[0] == 0x10)
                {
                    hexData[1] = (byte)ComPort.ReadByte();
                    if (hexData[1] == 0xB9)
                    {
                        startPacket = true;
                    }
                }
            }
            //Loop and read data until the end of the packet is reached
            for (int i = 1; i < packetSize; i++)
            {
                hexData[i] = (byte)ComPort.ReadByte();

                //Remove '10' packet when the sonde sends out two consecutively
                if (hexData[i] == 0x10 && hexData[i - 1] == 0x10)
                {
                    i--;
                }
                //Find the end of the packet and break
                if (hexData[i] == 0x03 && hexData[i - 1] == 0x10)
                {
                    break;
                }

            }

            //Checking to make sure the data is good before returning it.
            
            if (checkLastDecodedHexData(BitConverter.ToString(hexData)) == false)
            {
                return "Packet not ready!";
            }
            
            bSondeData = hexData;
            sSondeData = BitConverter.ToString(hexData);

            return sSondeData;
        }

        //name: checkLastDecodedHexData()
        //input: Current Hex packet collected from buffer.
        //output: bool, Reports True if the data is good and False if bad.
        //description: The method checks the current data packet to see if it is correct.
        public bool checkLastDecodedHexData(string hexPacket)
        {
            if (hexPacket.Substring(0, 5) != "10-01")
            {
                return false;
            }
            if (hexPacket.Substring(hexPacket.Length - 5, 5) != "10-03")
            {
                return false;
            }
            if (hexPacket.Length != 563)
            {
                return false;
            }

            return true;

        }

        
        //name: getSerialNum()
        //input: None
        //output: String representing the Radiosonde serial number
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the bSondeData[] global field
        
        public string getSerialNum()
        {
            string serNum = "";

                for (int i = 12; i <= 24; i++)
                {

                    if ((char)bSondeData[i] == '\0')
                    {
                        break;
                    }



                    serNum += (char)bSondeData[i];
                }

            serialNumber = serNum;
            return serNum;     
        }

        //name: setSerialNum(string SondeSN)
        //input: string of the desired serial number
        //output: bool returned true if the write works, or false if it fails.
        //description: This will write a string to the serial number of a radiosonde.

        public bool setSerialNum(string SondeSN)
        {
            try
            {
                ComPort.WriteLine("$sonde,setserialnumber," + SondeSN); //sending sonde serial number.
                return true;
            }
            catch
            {
                return false;
            }
        }


        //name: getTUProbeID()
        //input: None
        //output: String representing the Radiosonde probe ID
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the bSondeData[] global field

        public string getTUProbeID()
        {
            string TUProbeID = "";

            for (int i = 25; i <= 37; i++)
            {
                if ((char)bSondeData[i] == '\0')
                {
                    break;
                }

                TUProbeID += (char)bSondeData[i];
            }

            return TUProbeID;
        }

        //name: setTUProbeID()
        //input: string containing the probe number desired.
        //output: bool that reports weather the job was done.
        //description: This will stop the data from sonde and write the probe id then restart the data.

        public bool setTUProbeID(string ProbeID)
        {
            try
            {
                ComPort.WriteLine("$sonde,setprobeid," + ProbeID); //sending probe id.
                return true;
            }
            catch
            {
                return false;
            }
        }


        //name: getTrackingID()
        //input: None
        //output: String representing the Radiosonde tracking number
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the bSondeData[] global field

        public string getTrackingID()
        {
            string trackingID = "";

            for (int i = 38; i <= 50; i++)
            {
                if ((char)bSondeData[i] == '\0')
                {
                    break;
                }

                trackingID += (char)bSondeData[i];
            }

            return trackingID;
        }

        //name: setTrackingID(string TrackingID)
        //input: string of the desired tracking id
        //output: bool of true if the write works and false if it fails.
        //description: This will write a string to the Tracking ID of a radiosonde.

        public bool setTrackingID(string TrackingID)
        {
            try
            {
                ComPort.WriteLine("$sonde,setsondeid," + TrackingID); //sending tracking id to sonde.
                return true;
            }
            catch
            {
                return false;
            }
        }

        //name: getFirmwareVersion()
        //input: None
        //output: String representing the Radiosonde firmware version
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the bSondeData[] global field

        public string getFirmwareVersion()
        {
            string firmwareVersion = "";

            for (int i = 51; i <= 59; i++)
            {
                if (bSondeData[i] == '\0')
                {
                    break;
                }
                firmwareVersion += (char)bSondeData[i];
            }
            if (firmwareVersion.Length > 5)
            {
                firmwareVersion = firmwareVersion.Insert(2, ".");
                firmwareVersion = firmwareVersion.Insert(5, ".");
            }
            else
            {
                firmwareVersion = "";
            }

            return firmwareVersion;
        }

        //name: getRH()
        //input: None
        //output: String representing the Radiosonde %RH
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getRH()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double rawRH = Convert.ToInt16(splitSondeData[62] + splitSondeData[61], 16) / 100.0;
            return rawRH.ToString("0.000");
        }

        //name: getRHFreq()
        //input: None
        //output: String representing the Radiosonde RH Count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getRHFreq()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double frequency = Convert.ToUInt32(splitSondeData[65] + splitSondeData[64] + splitSondeData[63], 16)/ 1000.0;
            return frequency.ToString("0.000");           
        }

        //name: getRHFreqEPlusE()
        //input: None
        //output: String representing the Radiosonde RH Count from a e+e radiosonde
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getRHFreqEPlusE()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double frequency = Convert.ToUInt32(splitSondeData[65] + splitSondeData[64] + splitSondeData[63], 16) / 100.0;
            return frequency.ToString("0.000");
        }

        //name: getRHTranslated()
        //input: None
        //output: String representing the Radiosonde Translated Humidity.
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getRHTranslated()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double rawRHTranslated = Convert.ToInt16(splitSondeData[67] + splitSondeData[66], 16) / 100.0;
            return rawRHTranslated.ToString("0.000");
        }

        //name: getPressure()
        //input: None
        //output: String representing the radiosonde pressure
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getPressure()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double pressure = Convert.ToUInt32(splitSondeData[70] + splitSondeData[69]+splitSondeData[68], 16) / 1000.0;
            return pressure.ToString("0.000");
        }

        //name: getPressureCount()
        //input: None
        //output: String representing the radiosonde pressure count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getPressureCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 pCount = Convert.ToInt32(splitSondeData[73] + splitSondeData[72] + splitSondeData[71], 16);
            return pCount.ToString();
        }

        //name: getPressureRefCount()
        //input: None
        //output: String representing the radiosonde pressure reference count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getPressureRefCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 pRefCount = Convert.ToInt32(splitSondeData[76] + splitSondeData[75] + splitSondeData[74], 16);
            return pRefCount.ToString();
        }

        //name: getPressureTemp()
        //input: None
        //output: String representing the radiosonde pressure temperature
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getPressureTemp()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double pressureTemp = Convert.ToUInt32(splitSondeData[79] + splitSondeData[78] + splitSondeData[77], 16) / 1000.0 - 273.15;
            return pressureTemp.ToString("0.000");
        }

        //name: getPressureTempCount()
        //input: None
        //output: String representing the radiosonde pressure temperature count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getPressureTempCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 pressureTempCount = Convert.ToInt32(splitSondeData[82] + splitSondeData[81] + splitSondeData[80], 16);
            return pressureTempCount.ToString();
        }

        //name: getPressureTempRefCount()
        //input: None
        //output: String representing the radiosonde pressure temperature reference count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getPressureTempRefCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 pressureTempRefCount = Convert.ToInt32(splitSondeData[85] + splitSondeData[84] + splitSondeData[83], 16);
            return pressureTempRefCount.ToString();
        }

        //name: getPacketCount()
        //input: None
        //output: String representing the radiosonde Packet Count.
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field. May not work if number returned is negative.

        public string getPacketCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            int intPacketData = 0;
            int intPacetCounter = 0;

            intPacketData = Convert.ToInt32(splitSondeData[2], 16);
            intPacetCounter = Convert.ToInt32(splitSondeData[3], 16);

            intPacketData = intPacketData + (intPacetCounter * 254);


            return intPacketData.ToString();
        }

        //name: getAirTemp()
        //input: None
        //output: String representing the radiosonde air temperature
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getAirTemp()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double airTemp = Convert.ToUInt32(splitSondeData[88] + splitSondeData[87] + splitSondeData[86], 16) / 1000.0 - 273.15;
            return airTemp.ToString("0.000");
        }

        //name: getAirTempCount()
        //input: None
        //output: String representing the radiosonde air temperature count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getAirTempCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            UInt32 airTempCount = Convert.ToUInt32(splitSondeData[91] + splitSondeData[90] + splitSondeData[89], 16);
            return airTempCount.ToString();
        }

        //name: getAirTempRefCount()
        //input: None
        //output: String representing the radiosonde air temperature reference count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getAirTempRefCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 airTempRefCount = Convert.ToInt32(splitSondeData[94] + splitSondeData[93] + splitSondeData[92], 16);
            return airTempRefCount.ToString();
        }

        //name: getHumidityTemp()
        //input: None
        //output: String representing the radiosonde humidity temperature 
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getHumidityTemp()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double humTemp = Convert.ToUInt32(splitSondeData[97] + splitSondeData[96] + splitSondeData[95], 16) / 1000.0 - 273.15;
            return humTemp.ToString("0.000");
        }

        //name: getHumidityTempCount()
        //input: None
        //output: String representing the radiosonde humidity temperature count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getHumidityTempCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            UInt32 humTempCount = Convert.ToUInt32(splitSondeData[100] + splitSondeData[99] + splitSondeData[98], 16);
            return humTempCount.ToString();
        }

        //name: getHumidityTempRefCount()
        //input: None
        //output: String representing the radiosonde humidity temperature reference count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getHumidityTempRefCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 humTempRefCount = Convert.ToInt32(splitSondeData[103] + splitSondeData[102] + splitSondeData[101], 16);
            return humTempRefCount.ToString();
        }

        //name: getBatteryVoltage()
        //input: None
        //output: String representing the radiosonde battery voltage 
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getBatteryVoltage()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double voltage = Convert.ToUInt32(splitSondeData[106] + splitSondeData[105] + splitSondeData[104], 16) / 1000.0;
            return voltage.ToString("0.00");
        }

        //name: getBatteryVoltageCount()
        //input: None
        //output: String representing the radiosonde battery voltage count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getBatteryVoltageCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            UInt32 battVoltCount = Convert.ToUInt32(splitSondeData[109] + splitSondeData[108] + splitSondeData[107], 16);
            return battVoltCount.ToString();
        }

        //name: getBatteryVoltageRefCount()
        //input: None
        //output: String representing the radiosonde battery voltage reference count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getBatteryVoltageRefCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 battVoltRef = Convert.ToInt32(splitSondeData[112] + splitSondeData[111] + splitSondeData[110], 16);
            return battVoltRef.ToString();
        }

        //name: getInternalTemp()
        //input: None
        //output: String representing the radiosonde internal temperature 
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getInternalTemp()
        {
            string[] splitSondeData = sSondeData.Split('-');
            double internalTemp = Convert.ToUInt32(splitSondeData[115] + splitSondeData[114] + splitSondeData[113], 16) / 1000.0 - 273.15;
            return internalTemp.ToString("0.000");
        }

        //name: getInternalTempCount()
        //input: None
        //output: String representing the radiosonde internal temperature count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getInternalTempCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            UInt32 intTempCount = Convert.ToUInt32(splitSondeData[118] + splitSondeData[117] + splitSondeData[116], 16);
            return intTempCount.ToString();
        }

        //name: getInternalTempRefCount()
        //input: None
        //output: String representing the radiosonde internal temperature reference count
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getInternalTempRefCount()
        {
            string[] splitSondeData = sSondeData.Split('-');
            Int32 intTempRefCount = Convert.ToInt32(splitSondeData[121] + splitSondeData[120] + splitSondeData[119], 16);
            return intTempRefCount.ToString();
        }

        //name: getVelocityScale()
        //input: None
        //output: String representing the radiosonde velocity scale
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSVelocityScale()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSVelocityScaleData = "";

            for (int i = 127; i > 123; i--)
            {
                GPSVelocityScaleData = GPSVelocityScaleData + splitSondeData[i];
            }

            Int32 intGPSVelocityScale = Convert.ToInt32(GPSVelocityScaleData, 16);
            return GPSVelocityScaleData;
            //return intGPSVelocityScale.ToString();
        }

        //name: getGPSSNR()
        //input: None
        //output: String representing the radiosonde GPS SNR's
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSSNR()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string SNRData = "";
            int counter = 0;

            for (int i = 185; i > 173; i--)
            {
                SNRData = SNRData + Convert.ToString(Convert.ToInt32(splitSondeData[i], 16)) + ",";
                counter++;
            }

            return SNRData + "[" + counter + "]";
        }

        //name: getGPSPRN47()
        //input: None
        //output: String representing the radiosonde GPS PRN47's
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSPRN47()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string PNR47Data = "";
            int counter = 0;

            for (int i = 173; i > 161; i--)
            {
                PNR47Data = PNR47Data + Convert.ToString(Convert.ToInt32(splitSondeData[i],16)) + ",";
                counter++;
            }

            return PNR47Data + "[" + counter + "]";
        }

        //name: getGPSPRN8f()
        //input: None
        //output: String representing the radiosonde GPS PRN8f's
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSPRN8f()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string PRN8fData = "";
            int counter = 0;

            for (int i = 161; i > 149; i--)
            {
                PRN8fData = PRN8fData + Convert.ToString(Convert.ToInt32(splitSondeData[i], 16)) + ",";
                counter++;
            }

            return PRN8fData + "[" + counter + "]";
        }

        //name: getGPSSatCount47()
        //input: None
        //output: String representing the radiosonde GPS Sat Count 47
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSSatCount47()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string satCount47Data = "";
            
            satCount47Data = Convert.ToString(Convert.ToInt32(splitSondeData[149], 16));
            return satCount47Data;
        }

        //name: getGPSSatCount8f()
        //input: None
        //output: String representing the radiosonde GPS Sat Count 8f
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSSatCount8f()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string satCount8fData = "";

            satCount8fData = Convert.ToString(Convert.ToInt32(splitSondeData[148], 16));
            return satCount8fData;
        }

        //name: getGPSAlt()
        //input: None
        //output: String representing the radiosonde GPS Sat Count 8f
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSAlt()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSAltData = "";

            for (int i = 147; i > 143; i--)
            {
                GPSAltData = GPSAltData + splitSondeData[i];
            }

            Int32 intGPSAlt = Convert.ToInt32(GPSAltData,16);
            return intGPSAlt.ToString();

        }

        //name: getGPSLong()
        //input: None
        //output: String representing the radiosonde GPS Long
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string getGPSLong()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSLongData = "";

            for (int i = 143; i > 139; i--)
            {
                GPSLongData = GPSLongData + splitSondeData[i];
            }

            Int32 num = Convert.ToInt32(GPSLongData, 16);
            double scale=Math.Pow(2,31)/180;

            double longDegrees = num / scale;
            return longDegrees.ToString();

        }

        //name: getGPSLat()
        //input: None
        //output: String representing the radiosonde GPS Lat
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field. May not work if number returned is negative.

        public string getGPSlat()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSLatData = "";

            for (int i = 139; i > 135; i--)
            {
                GPSLatData = GPSLatData + splitSondeData[i];
            }

            Int32 num = Convert.ToInt32(GPSLatData, 16);
            double scale = Math.Pow(2, 31) / 180;

            double latDegrees = num / scale;
            return latDegrees.ToString();

        }

        //name: getGPSTime()
        //input: None
        //output: String representing the radiosonde GPS Time.
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field. May not work if number returned is negative.

        public string getGPSTime()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSTimeData = "";
            UInt32 intGPSTime = 0;

            for (int i = 135; i > 131; i--)
            {
                GPSTimeData = GPSTimeData + splitSondeData[i];
            }

            intGPSTime = Convert.ToUInt32(GPSTimeData, 16);            

            return intGPSTime.ToString();

        }

        //name: getGPSVerticalVelocity()
        //input: None
        //output: String representing the radiosonde GPS Vertical Velocity.
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field.

        public string getGPSVerticalVelocity()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSVerticalVelocityData = "";
            UInt32 intGPSVerticalVelocityData = 0;

            for (int i = 131; i > 129; i--)
            {
                GPSVerticalVelocityData = GPSVerticalVelocityData + splitSondeData[i];
            }

            intGPSVerticalVelocityData = Convert.ToUInt32(GPSVerticalVelocityData, 16);
            double doubleGPSVerticalVelocityData = (intGPSVerticalVelocityData * 0.005);

            if (doubleGPSVerticalVelocityData > 100)
            {
                doubleGPSVerticalVelocityData = 0;
            }

            return doubleGPSVerticalVelocityData.ToString();

        }

        //name: getGPSNorthVelocity()
        //input: None
        //output: String representing the radiosonde GPS North Velocity.
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field. May not work if number returned is negative.

        public string getGPSNorthVelocity()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSNorthVelocityData = "";
            UInt32 intGPSNorthVelocityData = 0;

            for (int i = 129; i > 127; i--)
            {
                GPSNorthVelocityData = GPSNorthVelocityData + splitSondeData[i];
            }

            intGPSNorthVelocityData = Convert.ToUInt32(GPSNorthVelocityData, 16);
            double doubleGPSNorthVelocityData = (intGPSNorthVelocityData * 0.005);

            if (doubleGPSNorthVelocityData > 100)
            {
                doubleGPSNorthVelocityData = 0;
            }

            return doubleGPSNorthVelocityData.ToString();

        }

        //name: getGPSEastVelocity()
        //input: None
        //output: String representing the radiosonde GPS East Velocity.
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field. May not work if number returned is negative.

        public string getGPSEastVelocity()
        {
            string[] splitSondeData = sSondeData.Split('-');
            string GPSEastVelocityData = "";
            UInt32 intGPSEastVelocityData = 0;

            for (int i = 129; i > 127; i--)
            {
                GPSEastVelocityData = GPSEastVelocityData + splitSondeData[i];
            }

            intGPSEastVelocityData = Convert.ToUInt32(GPSEastVelocityData, 16);
            double doubleGPSEastVelocityData = (intGPSEastVelocityData * 0.005);

            if (doubleGPSEastVelocityData > 100)
            {
                doubleGPSEastVelocityData = 0;
            }

            return doubleGPSEastVelocityData.ToString();

        }


        //Some coverters for GPS if needed.
        public double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        public double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        //name: decodeSondePacket()
        //input: None
        //output: Comma delimited string of decoded sonde data
        //description: Can only be run after the getLastHexData method is run
        //(ie. there must be data in the sSondeData global field

        public string decodeSondePacket()
        {
            string sondePacket = "";

            sondePacket += getSerialNum() + ", ";
            sondePacket += getTUProbeID() + ", ";
            sondePacket += getTrackingID() + ", ";
            sondePacket += getFirmwareVersion() + ", ";
            sondePacket += getRH() + ", ";
            sondePacket += getRHFreq() + ", ";
            sondePacket += getPressure() + ", ";
            sondePacket += getPressureCount() + ", ";
            sondePacket += getPressureRefCount() + ", ";
            sondePacket += getPressureTemp() + ", ";
            sondePacket += getPressureTempCount() + ", ";
            sondePacket += getPressureTempRefCount() + ", ";
            sondePacket += getAirTemp() + ", ";
            sondePacket += getAirTempCount() + ", ";
            sondePacket += getAirTempRefCount() + ", ";
            sondePacket += getHumidityTemp() + ", ";
            sondePacket += getHumidityTempCount() + ", ";
            sondePacket += getHumidityTempRefCount() + ", ";
            sondePacket += getBatteryVoltage() + ", ";
            sondePacket += getBatteryVoltageCount() + ", ";
            sondePacket += getBatteryVoltageRefCount() + ", ";
            sondePacket += getInternalTemp() + ", ";
            sondePacket += getInternalTempCount() + ", ";
            sondePacket += getInternalTempRefCount() + ", ";
           

            return sondePacket;
        }

#region Coefficents //This area deals with reading and writing coefficents to a sonde.
        //name: getcoefficent()
        //input: coefficent desired in int form.
        //output: String representing the radiosonde coefficent requested.
        //description: Method will stop the data from the radiosonde request the cofficent
        //then restart the data. After restarting the data the requested coffiecent will be
        //returned.

        public string getCoefficent(int coefficentNumber)
        {
            string currentCoefficent = "";
            string sondeData = "";

            try
            {
                ComPort.WriteLine("$sonde,dataoff"); //truning of data from sonde.
                Thread.Sleep(1000); //waiting for sonde to stop. needs about 1000ms to make sure.
                ComPort.DiscardInBuffer(); //Clearing out any left over data in buffer.

                ComPort.WriteLine("$sonde,getcal," + coefficentNumber.ToString()); //reqjusting coefficent.
                Thread.Sleep(1000); //Waiting for sonde to replay. Seem to take atleast 1000ms
                sondeData = ComPort.ReadLine(); //reading data from com port
                string[] sondeDataElement = sondeData.Split('='); //processing and breaking down the data.
                currentCoefficent = sondeDataElement[1];

                Initialize(); //restating the data.
                return currentCoefficent; //returning the coefficent data.
            }
            
            catch
            {
                Initialize();
                return "Error Requesting Coefficent.";
            }
            
        }

        //name: setcoefficent()
        //input: coefficent in string format and a int of the desired coeffivent to be written to in the sonde.
        //output: Nothing
        //description: Method will stop the data from the radiosonde. It will then write the coefficent to the sonde.
        //Then is checks to make sure the coefficent was written. If it is correct it restats the data.

        public void setCoefficent(int coefficentNumber, string coefficentValue)
        {
            string currentCoefficent = "";
            string sondeData = "";
            double coefficentNumberPart = 0;
            string coefficentExponent = "";

            //Making sure the coefficent is formated correctly.
            coefficentNumberPart = Convert.ToDouble(coefficentValue.Remove((coefficentValue.Length - 4), 4));
            coefficentExponent = coefficentValue.Substring((coefficentValue.Length - 4), 4);
            coefficentValue = coefficentNumberPart.ToString("0.00000") + coefficentExponent;


            ComPort.WriteLine("$sonde,dataoff"); //truning of data from sonde.
            Thread.Sleep(1000); //waiting for sonde to stop. needs about 1000ms to make sure.
            ComPort.DiscardInBuffer(); //Clearing out any left over data in buffer.
            ComPort.WriteLine("$sonde,setcal," + coefficentNumber.ToString()+","+coefficentValue); //sending coefficent.
            Thread.Sleep(1000);
            try
            {
                sondeData = ComPort.ReadLine(); //reading data from com port
            }
            catch
            {
                Thread.Sleep(100);
                sondeData = ComPort.ReadLine(); //reading data from com port
            }
            string[] sondeDataElement = sondeData.Split('='); //processing and breaking down the data.
            currentCoefficent = sondeDataElement[1];

            if (coefficentValue != currentCoefficent)
            {
                ComPort.WriteLine("$sonde,setcal," + coefficentNumber.ToString() + "," + coefficentValue); //sending coefficent.
            }
            Initialize(); //restating the data.
        }

        //name: getCoefficentArray(int[] requestedCoefficent)
        //input: Input of a int array that consist of the coefficents that you want.
        //output: Array of the requested coefficents.
        //description: This method stops the data, the request the coefficents then
        //restarts the data. The data retuned is in a string array with lines in the
        //format looks like: $sonde,cal[32]=N.NNNNNeNN

        public string[] getCoefficentArray(int[] requestedCoefficent)
        {
            string sondeData = "";

            try
            {
                ComPort.WriteLine("$sonde,dataoff"); //truning of data from sonde.
                Thread.Sleep(1000); //waiting for sonde to stop. needs about 1000ms to make sure.
                
                RetryGetCoefficent:
                ComPort.DiscardInBuffer(); //Clearing out any left over data in buffer.

                for (int i = 0; i < requestedCoefficent.Length; i++)
                {
                    ComPort.WriteLine("$sonde,getcal," + requestedCoefficent[i].ToString()); //reqjusting coefficent.
                    Thread.Sleep(500);
                }

                Thread.Sleep(2000); //Waiting for sonde to replay. Seem to take atleast 2000ms
                sondeData = ComPort.ReadExisting(); //reading data from com port
                sondeData = sondeData.Replace('\r',' ');
                sondeData = sondeData.Trim();
                string[] sondeDataElement = sondeData.Split('\n'); //processing and breaking down the data.

                for (int i = 0; i < sondeDataElement.Length; i++) //removing white spaces.
                {
                    sondeDataElement[i] = sondeDataElement[i].Trim();
                    if (sondeDataElement[i].Substring(0,10) != "$sonde,cal") //error check to make sure the right data is returned.
                    {
                        ComPort.DiscardInBuffer();
                        goto RetryGetCoefficent;
                    }
                }

                Initialize(); //restating the data.
                return sondeDataElement;
            }

            catch
            {
                Initialize();
                return null;
            }
        }

        
        //name: setcoefficentArray()
        //input: coefficent in string[] format and coefficent location in and int[] to be writen to the sonde.
        //output: Nothing
        //description: Method will stop the data from the radiosonde. After the data is stopped it will write the array
        //of coefficents to the sonde. Once done it will check to make sure the correct coefficents have been written.
        //If the correct coefficents are not reached in 3 loops it returns an error.

        public string[] setCoefficentArray(int[] coefficentNumber, string[] coefficentValue)
        {
            string sondeData = "";

            double coefficentNumberPart = 0;
            string coefficentExponent = "";

            int loopCounter = 0;

            //processing the incoming coefficent values to make sure they are in the corruct format for
            //sending to the sonde.
            for (int i = 0; i < coefficentValue.Length; i++)
            {
                coefficentNumberPart = Convert.ToDouble(coefficentValue[i].Remove((coefficentValue[i].Length-4),4));
                coefficentExponent = coefficentValue[i].Substring((coefficentValue[i].Length-4),4);
                coefficentValue[i] = coefficentNumberPart.ToString("0.00000") + coefficentExponent;
            }

            ComPort.WriteLine("$sonde,dataoff"); //truning of data from sonde.
            RetryWriteCoefficent:
            Thread.Sleep(1000); //waiting for sonde to stop. needs about 1000ms to make sure.
            ComPort.DiscardInBuffer(); //Clearing out any left over data in buffer.

            for (int i = 0; i < coefficentNumber.Length; i++)
            {
                ComPort.WriteLine("$sonde,setcal," + coefficentNumber[i].ToString() + "," + coefficentValue[i]); //sending coefficent.
                if (coefficentNumber.Length > 10)
                {
                    Thread.Sleep(1200);
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }
                       
            Thread.Sleep(2000);
            sondeData = ComPort.ReadExisting(); //reading data from com port
            sondeData = sondeData.Replace('\r', ' ');
            sondeData = sondeData.Trim();
            string[] sondeDataElement = sondeData.Split('\n'); //processing and breaking down the data.

            if (sondeDataElement.Length == 0)
            {
                sondeDataElement = new string[1];
                sondeDataElement[0] = "Error";
                return sondeDataElement;
            }

            int counter = 0;
            loopCounter++;
            if (loopCounter < 5)
            {
                for (int i = 0; i < sondeDataElement.Length; i++) //removing white spaces.
                {
                    sondeDataElement[i] = sondeDataElement[i].Trim();

                    if (sondeDataElement[i].ToString().Length < 9)//Just in case the return in blank.
                    {
                        goto RetryWriteCoefficent;
                    }

                    if (sondeDataElement[i] == "$data off")// just more dumb error handeling.
                    {
                        goto RetryWriteCoefficent;
                    }

                    if (sondeDataElement[i] == "$confused")// just more dumb error handeling.
                    {
                        goto RetryWriteCoefficent;
                    }

                    if (sondeDataElement[i].Substring(0, 10) != "$sonde,cal") //error check to make sure the right data is returned.
                    {
                        goto RetryWriteCoefficent;
                    }

                    int coefficentCount;
                    if (coefficentNumber[i] < 10)
                    {
                        coefficentCount = 1;
                    }
                    else
                    {
                        coefficentCount = 2;
                    }

                    if (Convert.ToInt16(sondeDataElement[i].Substring(11, coefficentCount)) != coefficentNumber[i] ||
                        Convert.ToDouble(sondeDataElement[i].Substring((11 + coefficentCount + 2), (sondeDataElement[i].Length - (11 + coefficentCount + 2)))) != Convert.ToDouble(coefficentValue[i]))
                    {
                        goto RetryWriteCoefficent;
                    }

                    counter++;
                }

                if (counter != coefficentNumber.Length) //Making sure all requesting coefficents are writen. If not retry.
                {
                    goto RetryWriteCoefficent;
                }
            }
            else
            {
                string[] sondeDataElementError = new string[1];
                sondeDataElementError[0] = "Error";
                return sondeDataElementError;
            }


            Initialize(); //restating the data.
            return sondeDataElement;

        }
        

        //name: setcoefficentArray2()
        //input: coefficent in string[] format and coefficent location in and int[] to be writen to the sonde.
        //output: Nothing
        //description: Method will stop the data from the radiosonde. After the data is stopped it will write the array
        //of coefficents to the sonde. Once done it will check to make sure the correct coefficents have been written.
        //If the correct coefficents are not reached in 3 loops it returns an error.

        public string[] setCoefficentArray2(int[] coefficentNumber, string[] coefficentValue, Boolean verbose)
        {
            string sondeData = "";
            double coefficentNumberPart = 0;
            string coefficentExponent = "";
            int loopCounter = 0;
            int timeoutCurrent = ComPort.ReadTimeout;
            ComPort.ReadTimeout = 1000;

            //Setting up visable display of status
            
            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            System.Windows.Forms.Label labelCurrentProcess = new System.Windows.Forms.Label();
            System.Windows.Forms.Label labelCurrentAction = new System.Windows.Forms.Label();
            //System.Windows.Forms.Button buttonCancel = new System.Windows.Forms.Button();
            System.Windows.Forms.ProgressBar progressBarCoeffients = new System.Windows.Forms.ProgressBar();

            form.ShowInTaskbar = false;
            form.Text = "Coefficent Write to Tracking Number: " + getTrackingID();
            labelCurrentProcess.Text = "Current Process";
            labelCurrentAction.Text = "Initializing coefficent write";
            progressBarCoeffients.Minimum = 0;
            if (coefficentNumber.Length > 20) { progressBarCoeffients.Maximum = coefficentNumber.Length + 6; }
            else { progressBarCoeffients.Maximum = coefficentNumber.Length + 1; }
            progressBarCoeffients.Step = 1;


            //buttonCancel.Text = "Cancel";
            //buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            labelCurrentProcess.SetBounds(9, 20, 372, 13);
            labelCurrentAction.SetBounds(9, 35, 372, 13);
            //buttonCancel.SetBounds(309, 72, 75, 23);
            progressBarCoeffients.SetBounds(0, 94, 394, 13);

            labelCurrentProcess.AutoSize = true;
            labelCurrentAction.AutoSize = true;
            //buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new System.Windows.Forms.Control[] { labelCurrentProcess, labelCurrentAction, progressBarCoeffients });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, labelCurrentProcess.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            //form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            
            //form.CancelButton = buttonCancel;

            if (verbose)
            {
                form.Show();
            }


            //processing the incoming coefficent values to make sure they are in the corruct format for
            //sending to the sonde.
            for (int i = 0; i < coefficentValue.Length; i++)
            {
                coefficentNumberPart = Convert.ToDouble(coefficentValue[i].Remove((coefficentValue[i].Length - 4), 4));
                coefficentExponent = coefficentValue[i].Substring((coefficentValue[i].Length - 4), 4);
                labelCurrentAction.Text = "Converting coeffient from: " + coefficentValue[i].ToString() + " to: " + coefficentNumberPart.ToString("0.00000") + coefficentExponent;
                form.Update();
                coefficentValue[i] = coefficentNumberPart.ToString("0.00000") + coefficentExponent;
            }

            labelCurrentAction.Text = "Sonde Data off.";
            form.Update();
            ComPort.WriteLine("$sonde,dataoff"); //truning of data from sonde.
        
            Thread.Sleep(2000); //waiting for sonde to stop. needs about 1000ms to make sure.
            labelCurrentAction.Text = "Clearing buffer";
            form.Update();
            ComPort.DiscardInBuffer(); //Clearing out any left over data in buffer.

            for (int i = 0; i < coefficentNumber.Length; i++)
            {
            RetryWriteCoefficent:
                //Creating a loop break if the coefficents are just not writing correctly.
                if (loopCounter == 3)
                {
                    string[] sondeDataElementError = new string[1];
                    sondeDataElementError[0] = "Error";
                    form.Dispose();
                    return sondeDataElementError;
                }
                labelCurrentAction.Text = "Sending coefficent[" + coefficentNumber[i].ToString() + "]=" + coefficentValue[i].ToString();
                form.Update();

                //Reading data form sonde. Error handeling to make sure is the sonde is dead, or not responding that it will error out
                //not crash the program.
                            
                ComPort.WriteLine("$sonde,setcal," + coefficentNumber[i].ToString() + "," + coefficentValue[i]); //sending coefficent.

                Thread.Sleep(1000); //Waiting for system to register the change.
                try
                {
                    sondeData = ComPort.ReadLine();
                }
                catch (TimeoutException error)
                {
                    Thread.Sleep(500);
                    labelCurrentAction.Text = "Unable to receive data from radiosonde trying agian.";
                    if (verbose) { Thread.Sleep(300); }
                    form.Update();
                    loopCounter++;
                    goto RetryWriteCoefficent;
                }
                

                string[] sondeDataElement = sondeData.Split('='); //processing and breaking down the data.

                //Checking to make sure the data is correct.
                if (Convert.ToDouble(sondeDataElement[1]) != Convert.ToDouble(coefficentValue[i]))
                {
                    labelCurrentAction.Text = "Coefficent write error. Rewriting coefficent.";
                    form.Update();
                    Thread.Sleep(1000);
                    ComPort.DiscardInBuffer();
                    loopCounter++; //Setting up a failure loop so we don't waste time.
                    goto RetryWriteCoefficent;
                }
                loopCounter = 0;
                labelCurrentAction.Text = "Coefficet Accepted.";
                progressBarCoeffients.PerformStep();
                form.Update();
                if (verbose) { Thread.Sleep(300); }

            }

            string[] returnElement = new string[1];
            returnElement[0] = "Pass";

            ComPort.ReadTimeout = timeoutCurrent;
            form.Dispose();
            Initialize(); //restating the data.
            return returnElement;

        }



#endregion


    }//End iMet1 class
    #endregion

    #region Bell202 This area is for decoding bell202 radiosondes.

    public class Bell202
    {
        //
        //Constructers
        //
        public Bell202()
        {
        }

        public Bell202(string ComPortName)
        {
            try
            {
                ComPort = new SerialPort(ComPortName, 1200, Parity.None, 8, StopBits.One);
                ComPort.NewLine = "\r\n";
            }

            catch
            {
                return;
            }
        }

        /// <summary>
        /// Setups the communication with a bell202 radiosonde through the maintance port.
        /// </summary>
        /// <param name="ComPortName"></param>
        public void Bell202Mantaince(string ComPortName, ref string status)
        {
            try
            {
                ComPort = new SerialPort(ComPortName, 9600, Parity.None, 8, StopBits.One);
                Initialize();
            }

            catch (Exception error)
            {
                status = error.Message;
                return;
            }
        }

        //
        //Public Variables
        //

        public SerialPort ComPort;
        public StreamWriter sondeFile;

        public string serialNumber;

        public static bool dataON;
        public static bool hasGPS;
        public static bool hasTX;

        public static string GPSType;
        public static string PTUType;

        //
        //Private Variables
        // Data from latest PTU Packet
        private byte[] bSondeDataPTU; //Latest sonde data in byte form
        public string sSondeDataPTU; //Latest sonde data in string form (eg. 00-FF)
        // Data from latest GPS Packet
        private byte[] bSondeDataGPS; //Latest sonde data in byte form
        public string sSondeDataGPS; //Latest sonde data in string form (eg. 00-FF)

        //
        //Current Maintance Data
        //
        public string currentSondeMaintancePTU;
        public string currentSondeMaintanceGPS;
        //
        //Public Functions
        //

        public void Initialize()
        {
            try
            {
                if (ComPort.IsOpen != true) //This allows the radiosonde to be re-initialized if needed without error.
                {
                    ComPort.Open();
                }
                ComPort.WriteLine("ptu=on");
                Thread.Sleep(50);
                ComPort.WriteLine("gps=on");
                Thread.Sleep(50);
                dataON = true;
                hasGPS = true;
                hasTX = true;
            }
            catch
            {
                return;
            }
        }

        //name: getLastPTUHexData
        //input: none
        //output: string
        //description: This method will find the start of a ptu or gps packet and then capture the rest
        //of the packet then send it to the global.

        public string getLastHexData()
        {
            int packetSize = 30;
            byte[] hexData = new byte[packetSize];
            byte[] hexDataPTU = null;
            byte[] hexDataGPS = null;
            bool bothPacket = false;
            bool PTUPacket = false;
            bool GPSPacket = false;

            ComPort.ReadTimeout = 20;

            while (bothPacket == false && ComPort.BytesToRead > packetSize * 1.5)
            {
                hexData[0] = (byte)ComPort.ReadByte();

                if (hexData[0] == 01)
                {
                    hexData[1] = (byte)ComPort.ReadByte();
                    if (hexData[1] == 01) //PTU Packet
                    {
                        hexDataPTU = new byte[14];
                        hexDataPTU[0] = hexData[0];
                        hexDataPTU[1] = hexData[1];

                        for (int i = 2; i < hexDataPTU.Length; i++)
                        {
                            hexDataPTU[i] = (byte)ComPort.ReadByte();
                        }
                        bSondeDataPTU = hexDataPTU;
                        PTUType = "PTU";
                        PTUPacket = true;
                    }

                    if (hexData[1] == 02) //GPS Packet
                    {
                        hexDataGPS = new byte[18];
                        hexDataGPS[0] = hexData[0];
                        hexDataGPS[1] = hexData[1];

                        for (int i = 2; i < hexDataGPS.Length; i++)
                        {
                            hexDataGPS[i] = (byte)ComPort.ReadByte();
                        }
                        bSondeDataGPS = hexDataGPS;
                        GPSType = "GPS";
                        GPSPacket = true;
                    }

                    if (hexData[1] == 05) //GPSX Packet
                    {
                        hexDataGPS = new byte[30];
                        hexDataGPS[0] = hexData[0];
                        hexDataGPS[1] = hexData[1];

                        for (int i = 2; i < hexDataGPS.Length; i++)
                        {
                            hexDataGPS[i] = (byte)ComPort.ReadByte();
                        }
                        bSondeDataGPS = hexDataGPS;
                        GPSType = "GPSX";
                        GPSPacket = true;
                    }

                    if (hexData[1] == 04) //PTUX Packet
                    {
                        hexDataPTU = new byte[20];
                        hexDataPTU[0] = hexData[0];
                        hexDataPTU[1] = hexData[1];

                        for (int i = 2; i < hexDataPTU.Length; i++)
                        {
                            hexDataPTU[i] = (byte)ComPort.ReadByte();
                        }
                        bSondeDataPTU = hexDataPTU;
                        PTUType = "PTUX";
                        PTUPacket = true;
                    }

                    if (PTUPacket == true && GPSPacket == true)
                    {
                        bothPacket = true;
                    }

                }
            }

            if (bothPacket == false)
            {
                return "Error";
            }
            

            //Converting byte array into a string and sending out for processing.
            sSondeDataGPS = BitConverter.ToString(hexDataGPS);
            sSondeDataPTU = BitConverter.ToString(hexDataPTU);

            return sSondeDataGPS + ", " + sSondeDataPTU;
        }

        #region PTUDecode //This section handles decodeing PTU for the bell202.

        //name: getPacketCount
        //input: none
        //output: string
        //description: reads the last hex GPS data packet transmitted by a radiosonde using the Bell202 tx
        //and displays it as a string. Returns "Packet not ready" if there is no data in the buffer.
        public string getPacketCount()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            UInt32 intpacketData = 0;

            for (int i = 3; i > 1; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            intpacketData = Convert.ToUInt32(packetData, 16);

            return intpacketData.ToString();
        }

        //name: getPressure
        //input: none
        //output: string
        //description: Reads from sSondeDataPTU and returns the currect pressure.
        public string getPressure()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            double doublepacketData = 0;

            for (int i = 6; i > 3; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            doublepacketData = int.Parse(packetData, System.Globalization.NumberStyles.HexNumber);
            doublepacketData = doublepacketData / 100;
            return doublepacketData.ToString();
        }

        //name: getAirTemp
        //input: none
        //output: string
        //description: Reads from sSondeDataPTU and returns the currect Air Temp.
        public string getAirTemp()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            double doublepacketData = 0;

            for (int i = 8; i > 6; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            doublepacketData = Convert.ToInt16(packetData, 16);
            doublepacketData = doublepacketData / 100;
            return doublepacketData.ToString();
        }

        //name: getHumidity
        //input: none
        //output: string
        //description: Reads from sSondeDataPTU and returns the currect Humidity.
        public string getHumidity()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            double doublepacketData = 0;

            for (int i = 10; i > 8; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            doublepacketData = Convert.ToInt16(packetData, 16);
            doublepacketData = doublepacketData / 100;
            return doublepacketData.ToString();
        }

        //name: getVBat
        //input: none
        //output: string
        //description: Reads from sSondeDataPTU and returns the currect Humidity.
        public string getVBat()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            double doublepacketData = 0;

            for (int i = 11; i > 10; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            doublepacketData = Convert.ToDouble(int.Parse(packetData, System.Globalization.NumberStyles.HexNumber));
            doublepacketData = doublepacketData / 10;
            return doublepacketData.ToString();
        }

        //name: getInternalTemp
        //input: none
        //output: string
        //description: Reads from sSondeDataPTU and returns the currect Internal Temp.
        public string getInternalTemp()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            double doublepacketData = 0;
            int intPacketData = 0;

            for (int i = 13; i > 11; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            intPacketData = Convert.ToInt16(packetData,16);
            doublepacketData = Convert.ToDouble(intPacketData) / 100;
            return doublepacketData.ToString();
        }

        //name: getPressureTemp
        //input: none
        //output: string
        //description: Reads from sSondeDataPTU and returns the currect Pressure Temp.
        public string getPressureTemp()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            double doublepacketData = 0;
            int intPacketData = 0;

            for (int i = 15; i > 13; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            intPacketData = Convert.ToInt16(packetData, 16);
            doublepacketData = Convert.ToDouble(intPacketData) / 100;
            return doublepacketData.ToString();
        }

        //name: getHumidityTemp
        //input: none
        //output: string
        //description: Reads from sSondeDataPTU and returns the currect Humidity Temp.
        public string getHumidityTemp()
        {
            string[] splitSondeData = sSondeDataPTU.Split('-');
            string packetData = "";
            double doublepacketData = 0;
            int intPacketData = 0;

            for (int i = 17; i > 15; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            intPacketData = Convert.ToInt16(packetData, 16);
            doublepacketData = Convert.ToDouble(intPacketData) / 100;
            return doublepacketData.ToString();
        }
        #endregion

        #region GPSDecode //This are is for decodeing the GPS of a bell202 tx.

        //name: getGPSLatitude
        //input: none
        //output: string
        //description: Reads from sSondeDataGPS and returns the currect Latitude.
        public string getGPSLatitude()
        {
            string[] splitSondeData = sSondeDataGPS.Split('-');
            string packetData = "";
            double doublepacketData = 0;

            byte[] floatVals = null;
            uint num = 0;

            for (int i = 5; i > 1; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            num = uint.Parse(packetData, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            doublepacketData = BitConverter.ToSingle(floatVals, 0);

            return doublepacketData.ToString();
        }

        //name: getGPSLongitude
        //input: none
        //output: string
        //description: Reads from sSondeDataGPS and returns the currect Longitude.
        public string getGPSLongitude()
        {
            string[] splitSondeData = sSondeDataGPS.Split('-');
            string packetData = "";
            double doublepacketData = 0;

            byte[] floatVals = null;
            uint num = 0;

            for (int i = 9; i > 5; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            num = uint.Parse(packetData, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            doublepacketData = BitConverter.ToSingle(floatVals, 0);

            return doublepacketData.ToString();
        }

        //name: getGPSAlt
        //input: none
        //output: string
        //description: Reads from sSondeDataGPS and returns the currect Alt.
        public string getGPSAlt()
        {
            string[] splitSondeData = sSondeDataGPS.Split('-');
            string packetData = "";
            double doublepacketData = 0;
            int intpacketData = 0;

            byte[] floatVals = null;
            uint num = 0;

            for (int i =11; i > 9; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            num = uint.Parse(packetData, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            doublepacketData = BitConverter.ToSingle(floatVals, 0);

            intpacketData = int.Parse(packetData, System.Globalization.NumberStyles.HexNumber);
            doublepacketData = Convert.ToDouble(intpacketData) - 5000;

            return doublepacketData.ToString();
        }

        //name: getGPSSatCount
        //input: none
        //output: string
        //description: Reads from sSondeDataGPS and returns the currect Sat Count.
        public string getGPSSatCount()
        {
            string[] splitSondeData = sSondeDataGPS.Split('-');
            string packetData = "";
            double doublepacketData = 0;
            int intpacketData = 0;

            byte[] floatVals = null;
            uint num = 0;

            for (int i = 12; i > 11; i--)
            {
                packetData = packetData + splitSondeData[i];
            }

            num = uint.Parse(packetData, System.Globalization.NumberStyles.AllowHexSpecifier);
            floatVals = BitConverter.GetBytes(num);
            doublepacketData = BitConverter.ToSingle(floatVals, 0);

            intpacketData = int.Parse(packetData, System.Globalization.NumberStyles.HexNumber);
            return intpacketData.ToString();
        }
        
        #endregion

        #region //This section is for collecting and processing data from the Bell202 maintance port

        /// <summary>
        /// Collects data from the bell202 radiosonde maintance port.
        /// </summary>
        /// <param name="dataGPS"></param>
        /// <param name="dataPTU"></param>
        public void getMaintanceData(ref string dataGPS, ref string dataPTU)
        {
            bool collectedAllData = false;
            while (!collectedAllData)
            {
                string serialData = "";

                try
                {
                    serialData = ComPort.ReadLine();
                }
                catch (Exception readError)
                {
                    ComPort.DiscardInBuffer();
                    dataGPS = readError.Message;
                    dataPTU = readError.Message;
                    return;
                }

                if (serialData.Length < 4) { break; }

                switch (serialData.Substring(0, 4))
                {
                    case "GPS:":
                        dataGPS = serialData;
                        currentSondeMaintanceGPS = serialData;
                        break;

                    case "PTU:":
                        dataPTU = serialData;
                        currentSondeMaintancePTU = serialData;
                        break;

                    case "GPSX":
                        dataGPS = serialData;
                        currentSondeMaintanceGPS = serialData;
                        break;

                    case "PTUX":
                        dataPTU = serialData;
                        currentSondeMaintancePTU = serialData;
                        break;

                    case "boot":
                        dataPTU = "Booting";
                        dataGPS = "Booting";
                        break;

                    case "init":
                        dataPTU = "init gps";
                        dataGPS = "init gps";
                        break;

                    case "read":
                        ComPort.Write("ptu=on\r");
                        ComPort.Write("gps=on\r");
                        break;
                }

                if (ComPort.BytesToRead == 0)
                {
                    collectedAllData = true;
                }

                Thread.Sleep(50); //A little break to make sure all the folling packet can enter the buffer.
            }
        }
        #region PTU Processing area.... The get area.
        /// <summary>
        /// Get the current pressure stored in memory. This function only works if the getMaintanceData has been run first.
        /// It returns a double.
        /// </summary>
        /// <returns></returns>
        public double getMaintancePressure()
        {
            if (currentSondeMaintancePTU != null)// A little protection.
            {
                string[] elementsPTU = currentSondeMaintancePTU.Split(':', ',');
                return Convert.ToDouble(elementsPTU[1]);
            }

            return 9999.99;
        }

        /// <summary>
        /// Get the current Air temp stored in memory. This function only works if the getMaintanceData has been run first.
        /// It returns a double.
        /// </summary>
        /// <returns></returns>
        public double getMaintanceAirTemp()
        {
            if (currentSondeMaintancePTU != null)// A little protection.
            {
                string[] elementsPTU = currentSondeMaintancePTU.Split(',');
                return Convert.ToDouble(elementsPTU[1]);
            }

            return 9999.99;
        }

        /// <summary>
        /// Get the current humidity stored in memory. This function only works if the getMaintanceData has been run first.
        /// It returns a double.
        /// </summary>
        /// <returns></returns>
        public double getMaintanceHumidity()
        {
            if (currentSondeMaintancePTU != null)// A little protection.
            {
                string[] elementsPTU = currentSondeMaintancePTU.Split(',');
                return Convert.ToDouble(elementsPTU[2]);
            }

            return 9999.99;
        }

        /// <summary>
        /// Get the current humidity temp stored in memory. This function only works if the getMaintanceData has been run first.
        /// It returns a double.
        /// </summary>
        /// <returns></returns>
        public double getMaintanceHumidityTemp()
        {
            if (currentSondeMaintancePTU != null)// A little protection.
            {
                string[] elementsPTU = currentSondeMaintancePTU.Split(',');
                return Convert.ToDouble(elementsPTU[3]);
            }

            return 9999.99;
        }
        #endregion

        #region GPS Processing area.... The get area.

        /// <summary>
        /// Get the current GPS data stored in memory. This function only works if the getMaintanceData has been run first.
        /// It returns a double.
        /// </summary>
        /// <returns></returns>
        public double getMaintanceLat()
        {
            if (currentSondeMaintanceGPS != null)// A little protection.
            {
                string[] elementsGPS = currentSondeMaintanceGPS.Split(':', ',');
                return Convert.ToDouble(elementsGPS[1]);
            }

            return 9999.99;
        }

        /// <summary>
        /// Get the current GPS long data stored in memory. This function only works if the getMaintanceData has been run first.
        /// It returns a double.
        /// </summary>
        /// <returns></returns>
        public double getMaintanceLong()
        {
            if (currentSondeMaintanceGPS != null)// A little protection.
            {
                string[] elementsGPS = currentSondeMaintanceGPS.Split(',');
                return Convert.ToDouble(elementsGPS[1]);
            }

            return 9999.99;
        }

        public double getMaintanceAlt()
        {
            if (currentSondeMaintanceGPS != null)// A little protection.
            {
                string[] elementsGPS = currentSondeMaintanceGPS.Split(',');
                return Convert.ToDouble(elementsGPS[2]);
            }

            return 9999.99;
        }

        #endregion

        #endregion

    }
    #endregion
}
