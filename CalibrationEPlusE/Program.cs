﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using Alerts;

namespace CalibrationEPlusE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //try
            //{
                Application.Run(new frmCalibrationMain());
            //}
            /*
            catch (Exception runError)
            {                
                //Starting audio alert.
                alertsAudio audioError = new alertsAudio();
                audioError.alertAudio(@"E:\InterMet Systems Projects\CailbrationEPlusE\CalibrationEPlusE\Audio\Alarm-08.wav", 1000);
                //audioError.startAlertAudio();

                //On screen display
                MessageBox.Show(runError.InnerException.StackTrace.ToString(), "Run Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
             */ 


        }

    }
}
