﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CalibrationEPlusE
{
    public partial class frmCalibrationUsers : Form
    {

        string[,] userInfo; //All the user data.

        public frmCalibrationUsers()
        {
            InitializeComponent();
        }

        private void frmCalibrationUsers_Load(object sender, EventArgs e)
        {
            //loading user information from the config file.
            loadUserSettingsFromFile();

            //updateing display with the data.
            updateUserDataDisplay();
        }

        public void loadUserSettingsFromFile()
        {
            int userConfigElements = 8;

            string[] configLine = File.ReadAllLines("userProfiles.cfg");
            userInfo = new string[configLine.Length - 3, userConfigElements];

            for (int i = 3; i < configLine.Length; i++)
            {
                string[] configElement = configLine[i].Split('\n', ','); //Breaking down the config line into it's elements.

                for (int j = 0; j < userConfigElements; j++)
                {
                    userInfo[i - 3, j] = configElement[j]; //All the user data.
                }
            }
        }

        public void updateUserDataDisplay()
        {
            int numberOfRows = (userInfo.Length)/8;
            dataGridViewUserProfiles.Rows.Add(numberOfRows - 1);

            for (int i = 0; i < userInfo.Length / 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    dataGridViewUserProfiles.Rows[i].Cells[j].Value = userInfo[i, j];
                }
            }

        }

        public void clearUserDataDisplay()
        {
            dataGridViewUserProfiles.Rows.Clear();
        }

    }
}
