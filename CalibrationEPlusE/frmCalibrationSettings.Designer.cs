﻿namespace CalibrationEPlusE
{
    partial class frmCalibrationSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBoxComSettings = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxComPortRelay = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxComPortThunder = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxComPortHMP234 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxComPortEPlusE = new System.Windows.Forms.ComboBox();
            this.groupBoxReferanceSensor = new System.Windows.Forms.GroupBox();
            this.radioButtonSensorEE31 = new System.Windows.Forms.RadioButton();
            this.radioButtonSensorHMP234 = new System.Windows.Forms.RadioButton();
            this.groupBoxChamberSettings = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxComPortChamber = new System.Windows.Forms.ComboBox();
            this.comboBoxChamberController = new System.Windows.Forms.ComboBox();
            this.groupBoxRadiosondes = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxComPortRadiosondeEnd = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxComPortRadiosondeStart = new System.Windows.Forms.ComboBox();
            this.buttonSaveSettings = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabControlSettings = new System.Windows.Forms.TabControl();
            this.tabPageGeneral = new System.Windows.Forms.TabPage();
            this.groupBoxCalcServer = new System.Windows.Forms.GroupBox();
            this.textBoxCalcServerAddress = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPageRadiosondes = new System.Windows.Forms.TabPage();
            this.groupBoxAcceptableTolerance = new System.Windows.Forms.GroupBox();
            this.textBoxRadiosondeUFreqPlusMinus = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxRadiosondeUFreqMean = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxRadiosondeAirTempDiff = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBoxRadiosondeColors = new System.Windows.Forms.GroupBox();
            this.panelColorInProcess = new System.Windows.Forms.Panel();
            this.panelColorRepeat = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.panelColorReady = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panelColorFail = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.panelColorPass = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPageLogging = new System.Windows.Forms.TabPage();
            this.groupBoxFileLocations = new System.Windows.Forms.GroupBox();
            this.textBoxCoefStorageLoc = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.buttonRAWFileStorageLocation = new System.Windows.Forms.Button();
            this.textBoxRAWStorageLocation = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPageCalibration = new System.Windows.Forms.TabPage();
            this.buttonAddPoint = new System.Windows.Forms.Button();
            this.dataGridViewCalibrationPoints = new System.Windows.Forms.DataGridView();
            this.ColumnOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTempSetPoint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEnclosureATR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEnclosureTSR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnChamberTempSP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnChamberATR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTempStablity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTempStableTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnHumiditySetPoint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnHumidityStableRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEnclosureHumidityRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnHumidityStableTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnThunderSATURpsi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnThunderSATIRC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnThunderFlowRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnThunderMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnValveA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnValveB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnValveC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnValve1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnValve2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnValve3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageAlerts = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxEmailAddress = new System.Windows.Forms.TextBox();
            this.checkBoxSSL = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxEmailPassword = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxEmailUsername = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxServerPort = new System.Windows.Forms.TextBox();
            this.textBoxServerAddress = new System.Windows.Forms.TextBox();
            this.groupBoxAlertEmail = new System.Windows.Forms.GroupBox();
            this.textBoxAlertEmailAddress = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.checkBoxAlertEmailEnable = new System.Windows.Forms.CheckBox();
            this.groupBoxAlertTxetMessage = new System.Windows.Forms.GroupBox();
            this.textBoxAlertPhoneNumber = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxAlertTextMessageEnable = new System.Windows.Forms.CheckBox();
            this.groupBoxAlertAudio = new System.Windows.Forms.GroupBox();
            this.buttonBrowseAudioFile = new System.Windows.Forms.Button();
            this.textBoxAlertAudioSoundFile = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBoxAlertAudioEnable = new System.Windows.Forms.CheckBox();
            this.tabPageRelay = new System.Windows.Forms.TabPage();
            this.groupBoxRelays = new System.Windows.Forms.GroupBox();
            this.dataGridViewRelays = new System.Windows.Forms.DataGridView();
            this.ColumnRelayPos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFunction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDependencies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxValveControl = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBoxValveControl = new System.Windows.Forms.ComboBox();
            this.groupBoxHumiditySource = new System.Windows.Forms.GroupBox();
            this.radioButtonRHSourceThunder = new System.Windows.Forms.RadioButton();
            this.radioButtonRHSourceThermotron = new System.Windows.Forms.RadioButton();
            this.groupBoxComSettings.SuspendLayout();
            this.groupBoxReferanceSensor.SuspendLayout();
            this.groupBoxChamberSettings.SuspendLayout();
            this.groupBoxRadiosondes.SuspendLayout();
            this.tabControlSettings.SuspendLayout();
            this.tabPageGeneral.SuspendLayout();
            this.groupBoxCalcServer.SuspendLayout();
            this.tabPageRadiosondes.SuspendLayout();
            this.groupBoxAcceptableTolerance.SuspendLayout();
            this.groupBoxRadiosondeColors.SuspendLayout();
            this.tabPageLogging.SuspendLayout();
            this.groupBoxFileLocations.SuspendLayout();
            this.tabPageCalibration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCalibrationPoints)).BeginInit();
            this.tabPageAlerts.SuspendLayout();
            this.groupBoxAlertEmail.SuspendLayout();
            this.groupBoxAlertTxetMessage.SuspendLayout();
            this.groupBoxAlertAudio.SuspendLayout();
            this.tabPageRelay.SuspendLayout();
            this.groupBoxRelays.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRelays)).BeginInit();
            this.groupBoxValveControl.SuspendLayout();
            this.groupBoxHumiditySource.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxComSettings
            // 
            this.groupBoxComSettings.Controls.Add(this.label6);
            this.groupBoxComSettings.Controls.Add(this.comboBoxComPortRelay);
            this.groupBoxComSettings.Controls.Add(this.label5);
            this.groupBoxComSettings.Controls.Add(this.comboBoxComPortThunder);
            this.groupBoxComSettings.Controls.Add(this.label4);
            this.groupBoxComSettings.Controls.Add(this.comboBoxComPortHMP234);
            this.groupBoxComSettings.Controls.Add(this.label3);
            this.groupBoxComSettings.Controls.Add(this.comboBoxComPortEPlusE);
            this.groupBoxComSettings.Location = new System.Drawing.Point(6, 6);
            this.groupBoxComSettings.Name = "groupBoxComSettings";
            this.groupBoxComSettings.Size = new System.Drawing.Size(200, 144);
            this.groupBoxComSettings.TabIndex = 0;
            this.groupBoxComSettings.TabStop = false;
            this.groupBoxComSettings.Text = "Com Port Settings";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Relay Com Port:";
            // 
            // comboBoxComPortRelay
            // 
            this.comboBoxComPortRelay.FormattingEnabled = true;
            this.comboBoxComPortRelay.Location = new System.Drawing.Point(108, 100);
            this.comboBoxComPortRelay.Name = "comboBoxComPortRelay";
            this.comboBoxComPortRelay.Size = new System.Drawing.Size(85, 21);
            this.comboBoxComPortRelay.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Thunder Com Port:";
            // 
            // comboBoxComPortThunder
            // 
            this.comboBoxComPortThunder.FormattingEnabled = true;
            this.comboBoxComPortThunder.Location = new System.Drawing.Point(108, 73);
            this.comboBoxComPortThunder.Name = "comboBoxComPortThunder";
            this.comboBoxComPortThunder.Size = new System.Drawing.Size(85, 21);
            this.comboBoxComPortThunder.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "HMP234 Com Port:";
            // 
            // comboBoxComPortHMP234
            // 
            this.comboBoxComPortHMP234.FormattingEnabled = true;
            this.comboBoxComPortHMP234.Location = new System.Drawing.Point(108, 46);
            this.comboBoxComPortHMP234.Name = "comboBoxComPortHMP234";
            this.comboBoxComPortHMP234.Size = new System.Drawing.Size(85, 21);
            this.comboBoxComPortHMP234.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "E+E Com Port:";
            // 
            // comboBoxComPortEPlusE
            // 
            this.comboBoxComPortEPlusE.FormattingEnabled = true;
            this.comboBoxComPortEPlusE.Location = new System.Drawing.Point(108, 19);
            this.comboBoxComPortEPlusE.Name = "comboBoxComPortEPlusE";
            this.comboBoxComPortEPlusE.Size = new System.Drawing.Size(85, 21);
            this.comboBoxComPortEPlusE.TabIndex = 2;
            // 
            // groupBoxReferanceSensor
            // 
            this.groupBoxReferanceSensor.Controls.Add(this.radioButtonSensorEE31);
            this.groupBoxReferanceSensor.Controls.Add(this.radioButtonSensorHMP234);
            this.groupBoxReferanceSensor.Location = new System.Drawing.Point(212, 9);
            this.groupBoxReferanceSensor.Name = "groupBoxReferanceSensor";
            this.groupBoxReferanceSensor.Size = new System.Drawing.Size(200, 64);
            this.groupBoxReferanceSensor.TabIndex = 1;
            this.groupBoxReferanceSensor.TabStop = false;
            this.groupBoxReferanceSensor.Text = "Referance Sensor";
            // 
            // radioButtonSensorEE31
            // 
            this.radioButtonSensorEE31.AutoSize = true;
            this.radioButtonSensorEE31.Location = new System.Drawing.Point(7, 41);
            this.radioButtonSensorEE31.Name = "radioButtonSensorEE31";
            this.radioButtonSensorEE31.Size = new System.Drawing.Size(74, 17);
            this.radioButtonSensorEE31.TabIndex = 1;
            this.radioButtonSensorEE31.TabStop = true;
            this.radioButtonSensorEE31.Text = "E+E EE31";
            this.radioButtonSensorEE31.UseVisualStyleBackColor = true;
            // 
            // radioButtonSensorHMP234
            // 
            this.radioButtonSensorHMP234.AutoSize = true;
            this.radioButtonSensorHMP234.Location = new System.Drawing.Point(7, 20);
            this.radioButtonSensorHMP234.Name = "radioButtonSensorHMP234";
            this.radioButtonSensorHMP234.Size = new System.Drawing.Size(104, 17);
            this.radioButtonSensorHMP234.TabIndex = 0;
            this.radioButtonSensorHMP234.TabStop = true;
            this.radioButtonSensorHMP234.Text = "Vaisala HMP234";
            this.radioButtonSensorHMP234.UseVisualStyleBackColor = true;
            // 
            // groupBoxChamberSettings
            // 
            this.groupBoxChamberSettings.Controls.Add(this.label2);
            this.groupBoxChamberSettings.Controls.Add(this.label1);
            this.groupBoxChamberSettings.Controls.Add(this.comboBoxComPortChamber);
            this.groupBoxChamberSettings.Controls.Add(this.comboBoxChamberController);
            this.groupBoxChamberSettings.Location = new System.Drawing.Point(212, 76);
            this.groupBoxChamberSettings.Name = "groupBoxChamberSettings";
            this.groupBoxChamberSettings.Size = new System.Drawing.Size(200, 74);
            this.groupBoxChamberSettings.TabIndex = 2;
            this.groupBoxChamberSettings.TabStop = false;
            this.groupBoxChamberSettings.Text = "Chamber Controller Settings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Controller Com Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Controller";
            // 
            // comboBoxComPortChamber
            // 
            this.comboBoxComPortChamber.FormattingEnabled = true;
            this.comboBoxComPortChamber.Location = new System.Drawing.Point(109, 46);
            this.comboBoxComPortChamber.Name = "comboBoxComPortChamber";
            this.comboBoxComPortChamber.Size = new System.Drawing.Size(85, 21);
            this.comboBoxComPortChamber.TabIndex = 1;
            // 
            // comboBoxChamberController
            // 
            this.comboBoxChamberController.FormattingEnabled = true;
            this.comboBoxChamberController.Items.AddRange(new object[] {
            "Watlow",
            "Thermotron"});
            this.comboBoxChamberController.Location = new System.Drawing.Point(73, 19);
            this.comboBoxChamberController.Name = "comboBoxChamberController";
            this.comboBoxChamberController.Size = new System.Drawing.Size(121, 21);
            this.comboBoxChamberController.TabIndex = 0;
            // 
            // groupBoxRadiosondes
            // 
            this.groupBoxRadiosondes.Controls.Add(this.label8);
            this.groupBoxRadiosondes.Controls.Add(this.comboBoxComPortRadiosondeEnd);
            this.groupBoxRadiosondes.Controls.Add(this.label7);
            this.groupBoxRadiosondes.Controls.Add(this.comboBoxComPortRadiosondeStart);
            this.groupBoxRadiosondes.Location = new System.Drawing.Point(3, 3);
            this.groupBoxRadiosondes.Name = "groupBoxRadiosondes";
            this.groupBoxRadiosondes.Size = new System.Drawing.Size(236, 71);
            this.groupBoxRadiosondes.TabIndex = 3;
            this.groupBoxRadiosondes.TabStop = false;
            this.groupBoxRadiosondes.Text = "Radiosondes Settings";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Radiosonde Ending Port";
            // 
            // comboBoxComPortRadiosondeEnd
            // 
            this.comboBoxComPortRadiosondeEnd.FormattingEnabled = true;
            this.comboBoxComPortRadiosondeEnd.Location = new System.Drawing.Point(137, 40);
            this.comboBoxComPortRadiosondeEnd.Name = "comboBoxComPortRadiosondeEnd";
            this.comboBoxComPortRadiosondeEnd.Size = new System.Drawing.Size(85, 21);
            this.comboBoxComPortRadiosondeEnd.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Radiosonde Starting Port";
            // 
            // comboBoxComPortRadiosondeStart
            // 
            this.comboBoxComPortRadiosondeStart.FormattingEnabled = true;
            this.comboBoxComPortRadiosondeStart.Location = new System.Drawing.Point(137, 13);
            this.comboBoxComPortRadiosondeStart.Name = "comboBoxComPortRadiosondeStart";
            this.comboBoxComPortRadiosondeStart.Size = new System.Drawing.Size(85, 21);
            this.comboBoxComPortRadiosondeStart.TabIndex = 10;
            // 
            // buttonSaveSettings
            // 
            this.buttonSaveSettings.Location = new System.Drawing.Point(141, 333);
            this.buttonSaveSettings.Name = "buttonSaveSettings";
            this.buttonSaveSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveSettings.TabIndex = 4;
            this.buttonSaveSettings.Text = "Save";
            this.buttonSaveSettings.UseVisualStyleBackColor = true;
            this.buttonSaveSettings.Click += new System.EventHandler(this.buttonSaveSettings_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(222, 333);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabControlSettings
            // 
            this.tabControlSettings.Controls.Add(this.tabPageGeneral);
            this.tabControlSettings.Controls.Add(this.tabPageRadiosondes);
            this.tabControlSettings.Controls.Add(this.tabPageLogging);
            this.tabControlSettings.Controls.Add(this.tabPageCalibration);
            this.tabControlSettings.Controls.Add(this.tabPageAlerts);
            this.tabControlSettings.Controls.Add(this.tabPageRelay);
            this.tabControlSettings.Location = new System.Drawing.Point(6, 8);
            this.tabControlSettings.Name = "tabControlSettings";
            this.tabControlSettings.SelectedIndex = 0;
            this.tabControlSettings.Size = new System.Drawing.Size(425, 319);
            this.tabControlSettings.TabIndex = 6;
            // 
            // tabPageGeneral
            // 
            this.tabPageGeneral.Controls.Add(this.groupBoxHumiditySource);
            this.tabPageGeneral.Controls.Add(this.groupBoxCalcServer);
            this.tabPageGeneral.Controls.Add(this.groupBoxComSettings);
            this.tabPageGeneral.Controls.Add(this.groupBoxReferanceSensor);
            this.tabPageGeneral.Controls.Add(this.groupBoxChamberSettings);
            this.tabPageGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabPageGeneral.Name = "tabPageGeneral";
            this.tabPageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGeneral.Size = new System.Drawing.Size(417, 293);
            this.tabPageGeneral.TabIndex = 0;
            this.tabPageGeneral.Text = "General";
            this.tabPageGeneral.UseVisualStyleBackColor = true;
            // 
            // groupBoxCalcServer
            // 
            this.groupBoxCalcServer.Controls.Add(this.textBoxCalcServerAddress);
            this.groupBoxCalcServer.Controls.Add(this.label10);
            this.groupBoxCalcServer.Enabled = false;
            this.groupBoxCalcServer.Location = new System.Drawing.Point(6, 225);
            this.groupBoxCalcServer.Name = "groupBoxCalcServer";
            this.groupBoxCalcServer.Size = new System.Drawing.Size(405, 59);
            this.groupBoxCalcServer.TabIndex = 3;
            this.groupBoxCalcServer.TabStop = false;
            this.groupBoxCalcServer.Text = "Calc Server";
            // 
            // textBoxCalcServerAddress
            // 
            this.textBoxCalcServerAddress.Location = new System.Drawing.Point(7, 33);
            this.textBoxCalcServerAddress.Name = "textBoxCalcServerAddress";
            this.textBoxCalcServerAddress.Size = new System.Drawing.Size(392, 20);
            this.textBoxCalcServerAddress.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Server Address:";
            // 
            // tabPageRadiosondes
            // 
            this.tabPageRadiosondes.Controls.Add(this.groupBoxAcceptableTolerance);
            this.tabPageRadiosondes.Controls.Add(this.groupBoxRadiosondeColors);
            this.tabPageRadiosondes.Controls.Add(this.groupBoxRadiosondes);
            this.tabPageRadiosondes.Location = new System.Drawing.Point(4, 22);
            this.tabPageRadiosondes.Name = "tabPageRadiosondes";
            this.tabPageRadiosondes.Size = new System.Drawing.Size(417, 293);
            this.tabPageRadiosondes.TabIndex = 1;
            this.tabPageRadiosondes.Text = "Radiosondes";
            this.tabPageRadiosondes.UseVisualStyleBackColor = true;
            // 
            // groupBoxAcceptableTolerance
            // 
            this.groupBoxAcceptableTolerance.Controls.Add(this.textBoxRadiosondeUFreqPlusMinus);
            this.groupBoxAcceptableTolerance.Controls.Add(this.label16);
            this.groupBoxAcceptableTolerance.Controls.Add(this.textBoxRadiosondeUFreqMean);
            this.groupBoxAcceptableTolerance.Controls.Add(this.label15);
            this.groupBoxAcceptableTolerance.Controls.Add(this.textBoxRadiosondeAirTempDiff);
            this.groupBoxAcceptableTolerance.Controls.Add(this.label14);
            this.groupBoxAcceptableTolerance.Location = new System.Drawing.Point(3, 76);
            this.groupBoxAcceptableTolerance.Name = "groupBoxAcceptableTolerance";
            this.groupBoxAcceptableTolerance.Size = new System.Drawing.Size(236, 140);
            this.groupBoxAcceptableTolerance.TabIndex = 5;
            this.groupBoxAcceptableTolerance.TabStop = false;
            this.groupBoxAcceptableTolerance.Text = "Acceptable Tolerance";
            // 
            // textBoxRadiosondeUFreqPlusMinus
            // 
            this.textBoxRadiosondeUFreqPlusMinus.Location = new System.Drawing.Point(128, 71);
            this.textBoxRadiosondeUFreqPlusMinus.Name = "textBoxRadiosondeUFreqPlusMinus";
            this.textBoxRadiosondeUFreqPlusMinus.Size = new System.Drawing.Size(66, 20);
            this.textBoxRadiosondeUFreqPlusMinus.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(31, 74);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Humidity Freq +/-:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxRadiosondeUFreqMean
            // 
            this.textBoxRadiosondeUFreqMean.Location = new System.Drawing.Point(128, 45);
            this.textBoxRadiosondeUFreqMean.Name = "textBoxRadiosondeUFreqMean";
            this.textBoxRadiosondeUFreqMean.Size = new System.Drawing.Size(66, 20);
            this.textBoxRadiosondeUFreqMean.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(18, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Humidity Freq Mean:";
            // 
            // textBoxRadiosondeAirTempDiff
            // 
            this.textBoxRadiosondeAirTempDiff.Location = new System.Drawing.Point(128, 19);
            this.textBoxRadiosondeAirTempDiff.Name = "textBoxRadiosondeAirTempDiff";
            this.textBoxRadiosondeAirTempDiff.Size = new System.Drawing.Size(66, 20);
            this.textBoxRadiosondeAirTempDiff.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(51, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Air Temp Diff:";
            // 
            // groupBoxRadiosondeColors
            // 
            this.groupBoxRadiosondeColors.Controls.Add(this.panelColorInProcess);
            this.groupBoxRadiosondeColors.Controls.Add(this.panelColorRepeat);
            this.groupBoxRadiosondeColors.Controls.Add(this.label25);
            this.groupBoxRadiosondeColors.Controls.Add(this.panelColorReady);
            this.groupBoxRadiosondeColors.Controls.Add(this.label24);
            this.groupBoxRadiosondeColors.Controls.Add(this.panelColorFail);
            this.groupBoxRadiosondeColors.Controls.Add(this.label23);
            this.groupBoxRadiosondeColors.Controls.Add(this.panelColorPass);
            this.groupBoxRadiosondeColors.Controls.Add(this.label22);
            this.groupBoxRadiosondeColors.Controls.Add(this.label21);
            this.groupBoxRadiosondeColors.Location = new System.Drawing.Point(245, 3);
            this.groupBoxRadiosondeColors.Name = "groupBoxRadiosondeColors";
            this.groupBoxRadiosondeColors.Size = new System.Drawing.Size(169, 213);
            this.groupBoxRadiosondeColors.TabIndex = 4;
            this.groupBoxRadiosondeColors.TabStop = false;
            this.groupBoxRadiosondeColors.Text = "Radiosonde Colors";
            // 
            // panelColorInProcess
            // 
            this.panelColorInProcess.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorInProcess.Location = new System.Drawing.Point(115, 100);
            this.panelColorInProcess.Name = "panelColorInProcess";
            this.panelColorInProcess.Size = new System.Drawing.Size(21, 21);
            this.panelColorInProcess.TabIndex = 25;
            this.panelColorInProcess.DoubleClick += new System.EventHandler(this.panelColorInProcess_DoubleClick);
            // 
            // panelColorRepeat
            // 
            this.panelColorRepeat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorRepeat.Location = new System.Drawing.Point(115, 127);
            this.panelColorRepeat.Name = "panelColorRepeat";
            this.panelColorRepeat.Size = new System.Drawing.Size(21, 21);
            this.panelColorRepeat.TabIndex = 23;
            this.panelColorRepeat.DoubleClick += new System.EventHandler(this.panelColorRepeat_DoubleClick);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(44, 102);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "In Process:";
            // 
            // panelColorReady
            // 
            this.panelColorReady.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorReady.Location = new System.Drawing.Point(115, 73);
            this.panelColorReady.Name = "panelColorReady";
            this.panelColorReady.Size = new System.Drawing.Size(21, 21);
            this.panelColorReady.TabIndex = 21;
            this.panelColorReady.DoubleClick += new System.EventHandler(this.panelColorReady_DoubleClick);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(59, 129);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "Repeat:";
            // 
            // panelColorFail
            // 
            this.panelColorFail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorFail.Location = new System.Drawing.Point(115, 46);
            this.panelColorFail.Name = "panelColorFail";
            this.panelColorFail.Size = new System.Drawing.Size(21, 21);
            this.panelColorFail.TabIndex = 19;
            this.panelColorFail.DoubleClick += new System.EventHandler(this.panelColorFail_DoubleClick);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(63, 75);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 13);
            this.label23.TabIndex = 20;
            this.label23.Text = "Ready:";
            // 
            // panelColorPass
            // 
            this.panelColorPass.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorPass.Location = new System.Drawing.Point(115, 19);
            this.panelColorPass.Name = "panelColorPass";
            this.panelColorPass.Size = new System.Drawing.Size(21, 21);
            this.panelColorPass.TabIndex = 17;
            this.panelColorPass.DoubleClick += new System.EventHandler(this.panelColorPass_DoubleClick);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(79, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(26, 13);
            this.label22.TabIndex = 18;
            this.label22.Text = "Fail:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(72, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 16;
            this.label21.Text = "Pass:";
            // 
            // tabPageLogging
            // 
            this.tabPageLogging.Controls.Add(this.groupBoxFileLocations);
            this.tabPageLogging.Location = new System.Drawing.Point(4, 22);
            this.tabPageLogging.Name = "tabPageLogging";
            this.tabPageLogging.Size = new System.Drawing.Size(417, 293);
            this.tabPageLogging.TabIndex = 2;
            this.tabPageLogging.Text = "Logging";
            this.tabPageLogging.UseVisualStyleBackColor = true;
            // 
            // groupBoxFileLocations
            // 
            this.groupBoxFileLocations.Controls.Add(this.textBoxCoefStorageLoc);
            this.groupBoxFileLocations.Controls.Add(this.label27);
            this.groupBoxFileLocations.Controls.Add(this.buttonRAWFileStorageLocation);
            this.groupBoxFileLocations.Controls.Add(this.textBoxRAWStorageLocation);
            this.groupBoxFileLocations.Controls.Add(this.label9);
            this.groupBoxFileLocations.Location = new System.Drawing.Point(6, 3);
            this.groupBoxFileLocations.Name = "groupBoxFileLocations";
            this.groupBoxFileLocations.Size = new System.Drawing.Size(408, 123);
            this.groupBoxFileLocations.TabIndex = 0;
            this.groupBoxFileLocations.TabStop = false;
            this.groupBoxFileLocations.Text = "File Locations";
            // 
            // textBoxCoefStorageLoc
            // 
            this.textBoxCoefStorageLoc.Location = new System.Drawing.Point(9, 76);
            this.textBoxCoefStorageLoc.Name = "textBoxCoefStorageLoc";
            this.textBoxCoefStorageLoc.Size = new System.Drawing.Size(361, 20);
            this.textBoxCoefStorageLoc.TabIndex = 4;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 60);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(113, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Coef Storage Location";
            // 
            // buttonRAWFileStorageLocation
            // 
            this.buttonRAWFileStorageLocation.Location = new System.Drawing.Point(376, 31);
            this.buttonRAWFileStorageLocation.Name = "buttonRAWFileStorageLocation";
            this.buttonRAWFileStorageLocation.Size = new System.Drawing.Size(26, 23);
            this.buttonRAWFileStorageLocation.TabIndex = 2;
            this.buttonRAWFileStorageLocation.Text = "<-";
            this.buttonRAWFileStorageLocation.UseVisualStyleBackColor = true;
            this.buttonRAWFileStorageLocation.Click += new System.EventHandler(this.buttonRAWFileStorageLocation_Click);
            // 
            // textBoxRAWStorageLocation
            // 
            this.textBoxRAWStorageLocation.Location = new System.Drawing.Point(9, 33);
            this.textBoxRAWStorageLocation.Name = "textBoxRAWStorageLocation";
            this.textBoxRAWStorageLocation.Size = new System.Drawing.Size(361, 20);
            this.textBoxRAWStorageLocation.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "RAW File Storage Location";
            // 
            // tabPageCalibration
            // 
            this.tabPageCalibration.Controls.Add(this.buttonAddPoint);
            this.tabPageCalibration.Controls.Add(this.dataGridViewCalibrationPoints);
            this.tabPageCalibration.Location = new System.Drawing.Point(4, 22);
            this.tabPageCalibration.Name = "tabPageCalibration";
            this.tabPageCalibration.Size = new System.Drawing.Size(417, 293);
            this.tabPageCalibration.TabIndex = 3;
            this.tabPageCalibration.Text = "Calibration";
            this.tabPageCalibration.UseVisualStyleBackColor = true;
            // 
            // buttonAddPoint
            // 
            this.buttonAddPoint.Location = new System.Drawing.Point(3, 193);
            this.buttonAddPoint.Name = "buttonAddPoint";
            this.buttonAddPoint.Size = new System.Drawing.Size(75, 23);
            this.buttonAddPoint.TabIndex = 1;
            this.buttonAddPoint.Text = "Add Point";
            this.buttonAddPoint.UseVisualStyleBackColor = true;
            this.buttonAddPoint.Click += new System.EventHandler(this.buttonAddPoint_Click);
            // 
            // dataGridViewCalibrationPoints
            // 
            this.dataGridViewCalibrationPoints.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCalibrationPoints.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnOrder,
            this.ColumnTempSetPoint,
            this.ColumnEnclosureATR,
            this.ColumnEnclosureTSR,
            this.ColumnChamberTempSP,
            this.ColumnChamberATR,
            this.ColumnTempStablity,
            this.ColumnTempStableTime,
            this.ColumnHumiditySetPoint,
            this.ColumnHumidityStableRange,
            this.ColumnEnclosureHumidityRange,
            this.ColumnHumidityStableTime,
            this.ColumnThunderSATURpsi,
            this.ColumnThunderSATIRC,
            this.ColumnThunderFlowRate,
            this.ColumnThunderMode,
            this.ColumnValveA,
            this.ColumnValveB,
            this.ColumnValveC,
            this.ColumnValve1,
            this.ColumnValve2,
            this.ColumnValve3});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCalibrationPoints.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewCalibrationPoints.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewCalibrationPoints.Name = "dataGridViewCalibrationPoints";
            this.dataGridViewCalibrationPoints.RowHeadersVisible = false;
            this.dataGridViewCalibrationPoints.Size = new System.Drawing.Size(411, 184);
            this.dataGridViewCalibrationPoints.TabIndex = 0;
            // 
            // ColumnOrder
            // 
            this.ColumnOrder.HeaderText = "Order";
            this.ColumnOrder.Name = "ColumnOrder";
            this.ColumnOrder.ToolTipText = "Order of calibration step";
            this.ColumnOrder.Width = 37;
            // 
            // ColumnTempSetPoint
            // 
            this.ColumnTempSetPoint.HeaderText = "Enclosure TempSP";
            this.ColumnTempSetPoint.Name = "ColumnTempSetPoint";
            this.ColumnTempSetPoint.ToolTipText = "Enclosure Temp Set Point";
            this.ColumnTempSetPoint.Width = 60;
            // 
            // ColumnEnclosureATR
            // 
            this.ColumnEnclosureATR.HeaderText = "Enclosure ATR";
            this.ColumnEnclosureATR.Name = "ColumnEnclosureATR";
            this.ColumnEnclosureATR.ToolTipText = "Enclosure Air Temp Range";
            this.ColumnEnclosureATR.Width = 60;
            // 
            // ColumnEnclosureTSR
            // 
            this.ColumnEnclosureTSR.HeaderText = "Enclosure TSR";
            this.ColumnEnclosureTSR.Name = "ColumnEnclosureTSR";
            this.ColumnEnclosureTSR.ToolTipText = "Enclosure Temp Stability Rate";
            this.ColumnEnclosureTSR.Width = 60;
            // 
            // ColumnChamberTempSP
            // 
            this.ColumnChamberTempSP.HeaderText = "Chamber TempSP";
            this.ColumnChamberTempSP.Name = "ColumnChamberTempSP";
            this.ColumnChamberTempSP.ToolTipText = "Chamber Temp Set Point";
            this.ColumnChamberTempSP.Width = 60;
            // 
            // ColumnChamberATR
            // 
            this.ColumnChamberATR.HeaderText = "Chamber ATR";
            this.ColumnChamberATR.Name = "ColumnChamberATR";
            this.ColumnChamberATR.ToolTipText = "Chamber Air Temp Range";
            this.ColumnChamberATR.Width = 55;
            // 
            // ColumnTempStablity
            // 
            this.ColumnTempStablity.HeaderText = "Chamber TSR";
            this.ColumnTempStablity.Name = "ColumnTempStablity";
            this.ColumnTempStablity.ToolTipText = "Chamber Temp Stability Rate";
            this.ColumnTempStablity.Width = 55;
            // 
            // ColumnTempStableTime
            // 
            this.ColumnTempStableTime.HeaderText = "TST";
            this.ColumnTempStableTime.Name = "ColumnTempStableTime";
            this.ColumnTempStableTime.ToolTipText = "Test Stability Time";
            this.ColumnTempStableTime.Width = 35;
            // 
            // ColumnHumiditySetPoint
            // 
            this.ColumnHumiditySetPoint.HeaderText = "Enclosure HumiditySP";
            this.ColumnHumiditySetPoint.Name = "ColumnHumiditySetPoint";
            this.ColumnHumiditySetPoint.ToolTipText = "Enclosure Humidity Set Point";
            this.ColumnHumiditySetPoint.Width = 65;
            // 
            // ColumnHumidityStableRange
            // 
            this.ColumnHumidityStableRange.HeaderText = "Enclosure HSR";
            this.ColumnHumidityStableRange.Name = "ColumnHumidityStableRange";
            this.ColumnHumidityStableRange.ToolTipText = "Enclouser Humidity Set Point Range";
            this.ColumnHumidityStableRange.Width = 60;
            // 
            // ColumnEnclosureHumidityRange
            // 
            this.ColumnEnclosureHumidityRange.HeaderText = "Enclosure UR";
            this.ColumnEnclosureHumidityRange.Name = "ColumnEnclosureHumidityRange";
            this.ColumnEnclosureHumidityRange.ToolTipText = "Enclosure Humidity Stability Rate";
            this.ColumnEnclosureHumidityRange.Width = 60;
            // 
            // ColumnHumidityStableTime
            // 
            this.ColumnHumidityStableTime.HeaderText = "HST";
            this.ColumnHumidityStableTime.Name = "ColumnHumidityStableTime";
            this.ColumnHumidityStableTime.Width = 35;
            // 
            // ColumnThunderSATURpsi
            // 
            this.ColumnThunderSATURpsi.HeaderText = "Thunder SATUR psi";
            this.ColumnThunderSATURpsi.Name = "ColumnThunderSATURpsi";
            this.ColumnThunderSATURpsi.Width = 65;
            // 
            // ColumnThunderSATIRC
            // 
            this.ColumnThunderSATIRC.HeaderText = "Thunder SATIR °C";
            this.ColumnThunderSATIRC.Name = "ColumnThunderSATIRC";
            this.ColumnThunderSATIRC.Width = 65;
            // 
            // ColumnThunderFlowRate
            // 
            this.ColumnThunderFlowRate.HeaderText = "Thunder Flow Rate";
            this.ColumnThunderFlowRate.Name = "ColumnThunderFlowRate";
            this.ColumnThunderFlowRate.Width = 65;
            // 
            // ColumnThunderMode
            // 
            this.ColumnThunderMode.HeaderText = "Thunder Mode";
            this.ColumnThunderMode.Name = "ColumnThunderMode";
            this.ColumnThunderMode.Width = 65;
            // 
            // ColumnValveA
            // 
            this.ColumnValveA.HeaderText = "Valve A";
            this.ColumnValveA.Name = "ColumnValveA";
            this.ColumnValveA.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnValveA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnValveA.Width = 50;
            // 
            // ColumnValveB
            // 
            this.ColumnValveB.HeaderText = "Valve B";
            this.ColumnValveB.Name = "ColumnValveB";
            this.ColumnValveB.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnValveB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnValveB.Width = 50;
            // 
            // ColumnValveC
            // 
            this.ColumnValveC.HeaderText = "Valve C";
            this.ColumnValveC.Name = "ColumnValveC";
            this.ColumnValveC.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnValveC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnValveC.Width = 50;
            // 
            // ColumnValve1
            // 
            this.ColumnValve1.HeaderText = "V1";
            this.ColumnValve1.Name = "ColumnValve1";
            this.ColumnValve1.Width = 40;
            // 
            // ColumnValve2
            // 
            this.ColumnValve2.HeaderText = "V2";
            this.ColumnValve2.Name = "ColumnValve2";
            this.ColumnValve2.Width = 40;
            // 
            // ColumnValve3
            // 
            this.ColumnValve3.HeaderText = "V3";
            this.ColumnValve3.Name = "ColumnValve3";
            this.ColumnValve3.Width = 40;
            // 
            // tabPageAlerts
            // 
            this.tabPageAlerts.Controls.Add(this.label26);
            this.tabPageAlerts.Controls.Add(this.textBoxEmailAddress);
            this.tabPageAlerts.Controls.Add(this.checkBoxSSL);
            this.tabPageAlerts.Controls.Add(this.label20);
            this.tabPageAlerts.Controls.Add(this.textBoxEmailPassword);
            this.tabPageAlerts.Controls.Add(this.label19);
            this.tabPageAlerts.Controls.Add(this.textBoxEmailUsername);
            this.tabPageAlerts.Controls.Add(this.label18);
            this.tabPageAlerts.Controls.Add(this.label17);
            this.tabPageAlerts.Controls.Add(this.textBoxServerPort);
            this.tabPageAlerts.Controls.Add(this.textBoxServerAddress);
            this.tabPageAlerts.Controls.Add(this.groupBoxAlertEmail);
            this.tabPageAlerts.Controls.Add(this.groupBoxAlertTxetMessage);
            this.tabPageAlerts.Controls.Add(this.groupBoxAlertAudio);
            this.tabPageAlerts.Location = new System.Drawing.Point(4, 22);
            this.tabPageAlerts.Name = "tabPageAlerts";
            this.tabPageAlerts.Size = new System.Drawing.Size(417, 293);
            this.tabPageAlerts.TabIndex = 4;
            this.tabPageAlerts.Text = "Alerts";
            this.tabPageAlerts.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 169);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(79, 13);
            this.label26.TabIndex = 15;
            this.label26.Text = "Server Address";
            // 
            // textBoxEmailAddress
            // 
            this.textBoxEmailAddress.Location = new System.Drawing.Point(92, 137);
            this.textBoxEmailAddress.Name = "textBoxEmailAddress";
            this.textBoxEmailAddress.Size = new System.Drawing.Size(305, 20);
            this.textBoxEmailAddress.TabIndex = 14;
            // 
            // checkBoxSSL
            // 
            this.checkBoxSSL.AutoSize = true;
            this.checkBoxSSL.Location = new System.Drawing.Point(350, 168);
            this.checkBoxSSL.Name = "checkBoxSSL";
            this.checkBoxSSL.Size = new System.Drawing.Size(46, 17);
            this.checkBoxSSL.TabIndex = 13;
            this.checkBoxSSL.Text = "SSL";
            this.checkBoxSSL.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(230, 195);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Password";
            // 
            // textBoxEmailPassword
            // 
            this.textBoxEmailPassword.Location = new System.Drawing.Point(289, 192);
            this.textBoxEmailPassword.Name = "textBoxEmailPassword";
            this.textBoxEmailPassword.Size = new System.Drawing.Size(108, 20);
            this.textBoxEmailPassword.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(31, 195);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "Username";
            // 
            // textBoxEmailUsername
            // 
            this.textBoxEmailUsername.Location = new System.Drawing.Point(92, 192);
            this.textBoxEmailUsername.Name = "textBoxEmailUsername";
            this.textBoxEmailUsername.Size = new System.Drawing.Size(132, 20);
            this.textBoxEmailUsername.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(257, 169);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(26, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "Port";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 140);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 7;
            this.label17.Text = "Email Address";
            // 
            // textBoxServerPort
            // 
            this.textBoxServerPort.Location = new System.Drawing.Point(289, 166);
            this.textBoxServerPort.Name = "textBoxServerPort";
            this.textBoxServerPort.Size = new System.Drawing.Size(55, 20);
            this.textBoxServerPort.TabIndex = 6;
            // 
            // textBoxServerAddress
            // 
            this.textBoxServerAddress.Location = new System.Drawing.Point(92, 166);
            this.textBoxServerAddress.Name = "textBoxServerAddress";
            this.textBoxServerAddress.Size = new System.Drawing.Size(156, 20);
            this.textBoxServerAddress.TabIndex = 5;
            // 
            // groupBoxAlertEmail
            // 
            this.groupBoxAlertEmail.Controls.Add(this.textBoxAlertEmailAddress);
            this.groupBoxAlertEmail.Controls.Add(this.label13);
            this.groupBoxAlertEmail.Controls.Add(this.checkBoxAlertEmailEnable);
            this.groupBoxAlertEmail.Location = new System.Drawing.Point(3, 82);
            this.groupBoxAlertEmail.Name = "groupBoxAlertEmail";
            this.groupBoxAlertEmail.Size = new System.Drawing.Size(411, 47);
            this.groupBoxAlertEmail.TabIndex = 4;
            this.groupBoxAlertEmail.TabStop = false;
            this.groupBoxAlertEmail.Text = "Email";
            // 
            // textBoxAlertEmailAddress
            // 
            this.textBoxAlertEmailAddress.Location = new System.Drawing.Point(170, 18);
            this.textBoxAlertEmailAddress.Name = "textBoxAlertEmailAddress";
            this.textBoxAlertEmailAddress.Size = new System.Drawing.Size(235, 20);
            this.textBoxAlertEmailAddress.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(83, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Email Address:";
            // 
            // checkBoxAlertEmailEnable
            // 
            this.checkBoxAlertEmailEnable.AutoSize = true;
            this.checkBoxAlertEmailEnable.Location = new System.Drawing.Point(7, 20);
            this.checkBoxAlertEmailEnable.Name = "checkBoxAlertEmailEnable";
            this.checkBoxAlertEmailEnable.Size = new System.Drawing.Size(59, 17);
            this.checkBoxAlertEmailEnable.TabIndex = 0;
            this.checkBoxAlertEmailEnable.Text = "Enable";
            this.checkBoxAlertEmailEnable.UseVisualStyleBackColor = true;
            // 
            // groupBoxAlertTxetMessage
            // 
            this.groupBoxAlertTxetMessage.Controls.Add(this.textBoxAlertPhoneNumber);
            this.groupBoxAlertTxetMessage.Controls.Add(this.label12);
            this.groupBoxAlertTxetMessage.Controls.Add(this.checkBoxAlertTextMessageEnable);
            this.groupBoxAlertTxetMessage.Location = new System.Drawing.Point(3, 42);
            this.groupBoxAlertTxetMessage.Name = "groupBoxAlertTxetMessage";
            this.groupBoxAlertTxetMessage.Size = new System.Drawing.Size(411, 38);
            this.groupBoxAlertTxetMessage.TabIndex = 3;
            this.groupBoxAlertTxetMessage.TabStop = false;
            this.groupBoxAlertTxetMessage.Text = "Text Message";
            // 
            // textBoxAlertPhoneNumber
            // 
            this.textBoxAlertPhoneNumber.Location = new System.Drawing.Point(170, 14);
            this.textBoxAlertPhoneNumber.Name = "textBoxAlertPhoneNumber";
            this.textBoxAlertPhoneNumber.Size = new System.Drawing.Size(178, 20);
            this.textBoxAlertPhoneNumber.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(83, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Phone Number:";
            // 
            // checkBoxAlertTextMessageEnable
            // 
            this.checkBoxAlertTextMessageEnable.AutoSize = true;
            this.checkBoxAlertTextMessageEnable.Location = new System.Drawing.Point(7, 16);
            this.checkBoxAlertTextMessageEnable.Name = "checkBoxAlertTextMessageEnable";
            this.checkBoxAlertTextMessageEnable.Size = new System.Drawing.Size(59, 17);
            this.checkBoxAlertTextMessageEnable.TabIndex = 0;
            this.checkBoxAlertTextMessageEnable.Text = "Enable";
            this.checkBoxAlertTextMessageEnable.UseVisualStyleBackColor = true;
            // 
            // groupBoxAlertAudio
            // 
            this.groupBoxAlertAudio.Controls.Add(this.buttonBrowseAudioFile);
            this.groupBoxAlertAudio.Controls.Add(this.textBoxAlertAudioSoundFile);
            this.groupBoxAlertAudio.Controls.Add(this.label11);
            this.groupBoxAlertAudio.Controls.Add(this.checkBoxAlertAudioEnable);
            this.groupBoxAlertAudio.Location = new System.Drawing.Point(3, 3);
            this.groupBoxAlertAudio.Name = "groupBoxAlertAudio";
            this.groupBoxAlertAudio.Size = new System.Drawing.Size(411, 36);
            this.groupBoxAlertAudio.TabIndex = 0;
            this.groupBoxAlertAudio.TabStop = false;
            this.groupBoxAlertAudio.Text = "Audio";
            // 
            // buttonBrowseAudioFile
            // 
            this.buttonBrowseAudioFile.Location = new System.Drawing.Point(366, 11);
            this.buttonBrowseAudioFile.Name = "buttonBrowseAudioFile";
            this.buttonBrowseAudioFile.Size = new System.Drawing.Size(28, 23);
            this.buttonBrowseAudioFile.TabIndex = 1;
            this.buttonBrowseAudioFile.Text = "<-";
            this.buttonBrowseAudioFile.UseVisualStyleBackColor = true;
            this.buttonBrowseAudioFile.Click += new System.EventHandler(this.buttonBrowseAudioFile_Click);
            // 
            // textBoxAlertAudioSoundFile
            // 
            this.textBoxAlertAudioSoundFile.Location = new System.Drawing.Point(134, 13);
            this.textBoxAlertAudioSoundFile.Name = "textBoxAlertAudioSoundFile";
            this.textBoxAlertAudioSoundFile.Size = new System.Drawing.Size(226, 20);
            this.textBoxAlertAudioSoundFile.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(72, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Audio File:";
            // 
            // checkBoxAlertAudioEnable
            // 
            this.checkBoxAlertAudioEnable.AutoSize = true;
            this.checkBoxAlertAudioEnable.Location = new System.Drawing.Point(7, 15);
            this.checkBoxAlertAudioEnable.Name = "checkBoxAlertAudioEnable";
            this.checkBoxAlertAudioEnable.Size = new System.Drawing.Size(59, 17);
            this.checkBoxAlertAudioEnable.TabIndex = 0;
            this.checkBoxAlertAudioEnable.Text = "Enable";
            this.checkBoxAlertAudioEnable.UseVisualStyleBackColor = true;
            // 
            // tabPageRelay
            // 
            this.tabPageRelay.Controls.Add(this.groupBoxRelays);
            this.tabPageRelay.Controls.Add(this.groupBoxValveControl);
            this.tabPageRelay.Location = new System.Drawing.Point(4, 22);
            this.tabPageRelay.Name = "tabPageRelay";
            this.tabPageRelay.Size = new System.Drawing.Size(417, 293);
            this.tabPageRelay.TabIndex = 5;
            this.tabPageRelay.Text = "Relays";
            this.tabPageRelay.UseVisualStyleBackColor = true;
            // 
            // groupBoxRelays
            // 
            this.groupBoxRelays.Controls.Add(this.dataGridViewRelays);
            this.groupBoxRelays.Location = new System.Drawing.Point(3, 76);
            this.groupBoxRelays.Name = "groupBoxRelays";
            this.groupBoxRelays.Size = new System.Drawing.Size(411, 214);
            this.groupBoxRelays.TabIndex = 6;
            this.groupBoxRelays.TabStop = false;
            this.groupBoxRelays.Text = "Relays";
            // 
            // dataGridViewRelays
            // 
            this.dataGridViewRelays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRelays.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnRelayPos,
            this.ColumnFunction,
            this.ColumnDependencies});
            this.dataGridViewRelays.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewRelays.Name = "dataGridViewRelays";
            this.dataGridViewRelays.RowHeadersVisible = false;
            this.dataGridViewRelays.Size = new System.Drawing.Size(398, 189);
            this.dataGridViewRelays.TabIndex = 0;
            // 
            // ColumnRelayPos
            // 
            this.ColumnRelayPos.HeaderText = "POS";
            this.ColumnRelayPos.Name = "ColumnRelayPos";
            this.ColumnRelayPos.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnRelayPos.Width = 50;
            // 
            // ColumnFunction
            // 
            this.ColumnFunction.HeaderText = "Function";
            this.ColumnFunction.Name = "ColumnFunction";
            this.ColumnFunction.Width = 150;
            // 
            // ColumnDependencies
            // 
            this.ColumnDependencies.HeaderText = "Dependencies";
            this.ColumnDependencies.Name = "ColumnDependencies";
            // 
            // groupBoxValveControl
            // 
            this.groupBoxValveControl.Controls.Add(this.comboBox1);
            this.groupBoxValveControl.Enabled = false;
            this.groupBoxValveControl.Location = new System.Drawing.Point(3, 3);
            this.groupBoxValveControl.Name = "groupBoxValveControl";
            this.groupBoxValveControl.Size = new System.Drawing.Size(140, 67);
            this.groupBoxValveControl.TabIndex = 5;
            this.groupBoxValveControl.TabStop = false;
            this.groupBoxValveControl.Text = "Valve Control";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "manual",
            "auto"});
            this.comboBox1.Location = new System.Drawing.Point(9, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // comboBoxValveControl
            // 
            this.comboBoxValveControl.FormattingEnabled = true;
            this.comboBoxValveControl.Items.AddRange(new object[] {
            "manual",
            "auto"});
            this.comboBoxValveControl.Location = new System.Drawing.Point(9, 19);
            this.comboBoxValveControl.Name = "comboBoxValveControl";
            this.comboBoxValveControl.Size = new System.Drawing.Size(121, 21);
            this.comboBoxValveControl.TabIndex = 0;
            // 
            // groupBoxHumiditySource
            // 
            this.groupBoxHumiditySource.Controls.Add(this.radioButtonRHSourceThermotron);
            this.groupBoxHumiditySource.Controls.Add(this.radioButtonRHSourceThunder);
            this.groupBoxHumiditySource.Location = new System.Drawing.Point(6, 152);
            this.groupBoxHumiditySource.Name = "groupBoxHumiditySource";
            this.groupBoxHumiditySource.Size = new System.Drawing.Size(200, 67);
            this.groupBoxHumiditySource.TabIndex = 4;
            this.groupBoxHumiditySource.TabStop = false;
            this.groupBoxHumiditySource.Text = "Humidity Source";
            // 
            // radioButtonRHSourceThunder
            // 
            this.radioButtonRHSourceThunder.AutoSize = true;
            this.radioButtonRHSourceThunder.Location = new System.Drawing.Point(9, 20);
            this.radioButtonRHSourceThunder.Name = "radioButtonRHSourceThunder";
            this.radioButtonRHSourceThunder.Size = new System.Drawing.Size(92, 17);
            this.radioButtonRHSourceThunder.TabIndex = 0;
            this.radioButtonRHSourceThunder.Text = "Thunder 3900";
            this.radioButtonRHSourceThunder.UseVisualStyleBackColor = true;
            // 
            // radioButtonRHSourceThermotron
            // 
            this.radioButtonRHSourceThermotron.AutoSize = true;
            this.radioButtonRHSourceThermotron.Location = new System.Drawing.Point(9, 43);
            this.radioButtonRHSourceThermotron.Name = "radioButtonRHSourceThermotron";
            this.radioButtonRHSourceThermotron.Size = new System.Drawing.Size(79, 17);
            this.radioButtonRHSourceThermotron.TabIndex = 1;
            this.radioButtonRHSourceThermotron.TabStop = true;
            this.radioButtonRHSourceThermotron.Text = "Thermotron";
            this.radioButtonRHSourceThermotron.UseVisualStyleBackColor = true;
            // 
            // frmCalibrationSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 368);
            this.Controls.Add(this.tabControlSettings);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSaveSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCalibrationSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Calibration Settings";
            this.Load += new System.EventHandler(this.frmCalibrationSettings_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCalibrationSettings_FormClosing);
            this.groupBoxComSettings.ResumeLayout(false);
            this.groupBoxComSettings.PerformLayout();
            this.groupBoxReferanceSensor.ResumeLayout(false);
            this.groupBoxReferanceSensor.PerformLayout();
            this.groupBoxChamberSettings.ResumeLayout(false);
            this.groupBoxChamberSettings.PerformLayout();
            this.groupBoxRadiosondes.ResumeLayout(false);
            this.groupBoxRadiosondes.PerformLayout();
            this.tabControlSettings.ResumeLayout(false);
            this.tabPageGeneral.ResumeLayout(false);
            this.groupBoxCalcServer.ResumeLayout(false);
            this.groupBoxCalcServer.PerformLayout();
            this.tabPageRadiosondes.ResumeLayout(false);
            this.groupBoxAcceptableTolerance.ResumeLayout(false);
            this.groupBoxAcceptableTolerance.PerformLayout();
            this.groupBoxRadiosondeColors.ResumeLayout(false);
            this.groupBoxRadiosondeColors.PerformLayout();
            this.tabPageLogging.ResumeLayout(false);
            this.groupBoxFileLocations.ResumeLayout(false);
            this.groupBoxFileLocations.PerformLayout();
            this.tabPageCalibration.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCalibrationPoints)).EndInit();
            this.tabPageAlerts.ResumeLayout(false);
            this.tabPageAlerts.PerformLayout();
            this.groupBoxAlertEmail.ResumeLayout(false);
            this.groupBoxAlertEmail.PerformLayout();
            this.groupBoxAlertTxetMessage.ResumeLayout(false);
            this.groupBoxAlertTxetMessage.PerformLayout();
            this.groupBoxAlertAudio.ResumeLayout(false);
            this.groupBoxAlertAudio.PerformLayout();
            this.tabPageRelay.ResumeLayout(false);
            this.groupBoxRelays.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRelays)).EndInit();
            this.groupBoxValveControl.ResumeLayout(false);
            this.groupBoxHumiditySource.ResumeLayout(false);
            this.groupBoxHumiditySource.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxComSettings;
        private System.Windows.Forms.GroupBox groupBoxReferanceSensor;
        private System.Windows.Forms.RadioButton radioButtonSensorHMP234;
        private System.Windows.Forms.GroupBox groupBoxChamberSettings;
        private System.Windows.Forms.RadioButton radioButtonSensorEE31;
        private System.Windows.Forms.ComboBox comboBoxComPortEPlusE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxComPortChamber;
        private System.Windows.Forms.ComboBox comboBoxChamberController;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxComPortRelay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxComPortThunder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxComPortHMP234;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBoxRadiosondes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxComPortRadiosondeStart;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxComPortRadiosondeEnd;
        private System.Windows.Forms.Button buttonSaveSettings;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabControl tabControlSettings;
        private System.Windows.Forms.TabPage tabPageGeneral;
        private System.Windows.Forms.TabPage tabPageRadiosondes;
        private System.Windows.Forms.TabPage tabPageLogging;
        private System.Windows.Forms.TabPage tabPageCalibration;
        private System.Windows.Forms.DataGridView dataGridViewCalibrationPoints;
        private System.Windows.Forms.GroupBox groupBoxFileLocations;
        private System.Windows.Forms.Button buttonRAWFileStorageLocation;
        private System.Windows.Forms.TextBox textBoxRAWStorageLocation;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBoxCalcServer;
        private System.Windows.Forms.TextBox textBoxCalcServerAddress;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPageAlerts;
        private System.Windows.Forms.GroupBox groupBoxAlertAudio;
        private System.Windows.Forms.CheckBox checkBoxAlertAudioEnable;
        private System.Windows.Forms.GroupBox groupBoxAlertTxetMessage;
        private System.Windows.Forms.TextBox textBoxAlertPhoneNumber;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxAlertTextMessageEnable;
        private System.Windows.Forms.Button buttonBrowseAudioFile;
        private System.Windows.Forms.TextBox textBoxAlertAudioSoundFile;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBoxAlertEmail;
        private System.Windows.Forms.TextBox textBoxAlertEmailAddress;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkBoxAlertEmailEnable;
        private System.Windows.Forms.GroupBox groupBoxRadiosondeColors;
        private System.Windows.Forms.Panel panelColorInProcess;
        private System.Windows.Forms.Panel panelColorRepeat;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panelColorReady;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panelColorFail;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panelColorPass;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button buttonAddPoint;
        private System.Windows.Forms.GroupBox groupBoxAcceptableTolerance;
        private System.Windows.Forms.TextBox textBoxRadiosondeUFreqPlusMinus;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxRadiosondeUFreqMean;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxRadiosondeAirTempDiff;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxServerPort;
        private System.Windows.Forms.TextBox textBoxServerAddress;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxEmailUsername;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox checkBoxSSL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxEmailPassword;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxEmailAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTempSetPoint;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEnclosureATR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEnclosureTSR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnChamberTempSP;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnChamberATR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTempStablity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTempStableTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnHumiditySetPoint;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnHumidityStableRange;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEnclosureHumidityRange;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnHumidityStableTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnThunderSATURpsi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnThunderSATIRC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnThunderFlowRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnThunderMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValveA;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValveB;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValveC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValve1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValve2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValve3;
        private System.Windows.Forms.TabPage tabPageRelay;
        private System.Windows.Forms.ComboBox comboBoxValveControl;
        private System.Windows.Forms.GroupBox groupBoxValveControl;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBoxRelays;
        private System.Windows.Forms.DataGridView dataGridViewRelays;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRelayPos;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFunction;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDependencies;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxCoefStorageLoc;
        private System.Windows.Forms.GroupBox groupBoxHumiditySource;
        private System.Windows.Forms.RadioButton radioButtonRHSourceThunder;
        private System.Windows.Forms.RadioButton radioButtonRHSourceThermotron;
    }
}