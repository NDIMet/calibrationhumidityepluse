﻿namespace CalibrationEPlusE
{
    partial class frmCalibrationUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewUserProfiles = new System.Windows.Forms.DataGridView();
            this.ColumnUsername = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEnableAudio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAudioFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEnableTxt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTxtNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTxtCarrier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEnableEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEmailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonAddUser = new System.Windows.Forms.Button();
            this.buttonDeleteUser = new System.Windows.Forms.Button();
            this.buttonEditUser = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUserProfiles)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewUserProfiles
            // 
            this.dataGridViewUserProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUserProfiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnUsername,
            this.ColumnEnableAudio,
            this.ColumnAudioFile,
            this.ColumnEnableTxt,
            this.ColumnTxtNumber,
            this.ColumnTxtCarrier,
            this.ColumnEnableEmail,
            this.ColumnEmailAddress});
            this.dataGridViewUserProfiles.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewUserProfiles.Name = "dataGridViewUserProfiles";
            this.dataGridViewUserProfiles.RowHeadersVisible = false;
            this.dataGridViewUserProfiles.Size = new System.Drawing.Size(468, 196);
            this.dataGridViewUserProfiles.TabIndex = 0;
            // 
            // ColumnUsername
            // 
            this.ColumnUsername.HeaderText = "Username";
            this.ColumnUsername.Name = "ColumnUsername";
            // 
            // ColumnEnableAudio
            // 
            this.ColumnEnableAudio.HeaderText = "Enable Audio";
            this.ColumnEnableAudio.Name = "ColumnEnableAudio";
            // 
            // ColumnAudioFile
            // 
            this.ColumnAudioFile.HeaderText = "Audio File";
            this.ColumnAudioFile.Name = "ColumnAudioFile";
            // 
            // ColumnEnableTxt
            // 
            this.ColumnEnableTxt.HeaderText = "Enable Txt";
            this.ColumnEnableTxt.Name = "ColumnEnableTxt";
            // 
            // ColumnTxtNumber
            // 
            this.ColumnTxtNumber.HeaderText = "Txt Number";
            this.ColumnTxtNumber.Name = "ColumnTxtNumber";
            // 
            // ColumnTxtCarrier
            // 
            this.ColumnTxtCarrier.HeaderText = "Txt Carrier";
            this.ColumnTxtCarrier.Name = "ColumnTxtCarrier";
            // 
            // ColumnEnableEmail
            // 
            this.ColumnEnableEmail.HeaderText = "Enable Email";
            this.ColumnEnableEmail.Name = "ColumnEnableEmail";
            // 
            // ColumnEmailAddress
            // 
            this.ColumnEmailAddress.HeaderText = "Email Address";
            this.ColumnEmailAddress.Name = "ColumnEmailAddress";
            // 
            // buttonAddUser
            // 
            this.buttonAddUser.Location = new System.Drawing.Point(209, 227);
            this.buttonAddUser.Name = "buttonAddUser";
            this.buttonAddUser.Size = new System.Drawing.Size(75, 23);
            this.buttonAddUser.TabIndex = 1;
            this.buttonAddUser.Text = "Add";
            this.buttonAddUser.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteUser
            // 
            this.buttonDeleteUser.Location = new System.Drawing.Point(290, 227);
            this.buttonDeleteUser.Name = "buttonDeleteUser";
            this.buttonDeleteUser.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteUser.TabIndex = 2;
            this.buttonDeleteUser.Text = "Delete";
            this.buttonDeleteUser.UseVisualStyleBackColor = true;
            // 
            // buttonEditUser
            // 
            this.buttonEditUser.Location = new System.Drawing.Point(128, 227);
            this.buttonEditUser.Name = "buttonEditUser";
            this.buttonEditUser.Size = new System.Drawing.Size(75, 23);
            this.buttonEditUser.TabIndex = 3;
            this.buttonEditUser.Text = "Edit";
            this.buttonEditUser.UseVisualStyleBackColor = true;
            // 
            // frmCalibrationUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 262);
            this.Controls.Add(this.buttonEditUser);
            this.Controls.Add(this.buttonDeleteUser);
            this.Controls.Add(this.buttonAddUser);
            this.Controls.Add(this.dataGridViewUserProfiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCalibrationUsers";
            this.ShowInTaskbar = false;
            this.Text = "Calibration Users";
            this.Load += new System.EventHandler(this.frmCalibrationUsers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUserProfiles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewUserProfiles;
        private System.Windows.Forms.Button buttonAddUser;
        private System.Windows.Forms.Button buttonDeleteUser;
        private System.Windows.Forms.Button buttonEditUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUsername;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEnableAudio;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAudioFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEnableTxt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTxtNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTxtCarrier;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEnableEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEmailAddress;
    }
}