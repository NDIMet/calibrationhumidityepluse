﻿namespace CalibrationEPlusE
{
    partial class frmCalibrationMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalibrationMain));
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkRadiosondesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.thunderControlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chamberControlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relayControlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.calcCorrectChamberTempToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manuelResetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thunderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.envStableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startLoggingProformanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calulateCoefficentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.correctReferanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hMP234ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxCalibrationControls = new System.Windows.Forms.GroupBox();
            this.checkBoxPostCorrect = new System.Windows.Forms.CheckBox();
            this.checkBoxShutdownAfter = new System.Windows.Forms.CheckBox();
            this.comboBoxRadiosondeType = new System.Windows.Forms.ComboBox();
            this.buttonStartCalibration = new System.Windows.Forms.Button();
            this.groupBoxStatusRadiosondes = new System.Windows.Forms.GroupBox();
            this.comboBoxRadiosondeSelect = new System.Windows.Forms.ComboBox();
            this.dataGridViewRadiosondes = new System.Windows.Forms.DataGridView();
            this.ColumnLOC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTrackingNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPacketCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAirTempDiff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnHumidityCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFirmware = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxChamber = new System.Windows.Forms.GroupBox();
            this.labelChamerRelay = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelChamberHumiditySetPoint = new System.Windows.Forms.Label();
            this.labelChamberAirTempSetPoint = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelChamberHumidityRamp = new System.Windows.Forms.Label();
            this.labelChamberAirTempRamp = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelChamberHumidityAvg = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelChamberHumidity = new System.Windows.Forms.Label();
            this.labelChamberAirTempAvg = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelChamberAirTemp = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxReferance = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelReferanceAirTemp = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelReferanceHumidityAvg = new System.Windows.Forms.Label();
            this.labelReferanceAirTempAvg = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelReferanceHumidity = new System.Windows.Forms.Label();
            this.groupBoxThunderStatus = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.labelCurrentStatus = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.labelSetFlow = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.labelCurrentFlow = new System.Windows.Forms.Label();
            this.labelSetTestC = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.labelCurrentTestC = new System.Windows.Forms.Label();
            this.labelSetTestPSI = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelCurrentTestPSI = new System.Windows.Forms.Label();
            this.labelSetSaturC = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.labelCurrentSaturC = new System.Windows.Forms.Label();
            this.labelSetSaturPSI = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.labelCurrentSaturPSI = new System.Windows.Forms.Label();
            this.labelSetRH = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.labelCurrentRH = new System.Windows.Forms.Label();
            this.labelSetPPMw = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.labelCurrentPPMw = new System.Windows.Forms.Label();
            this.labelSetPPMv = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.labelCurrentPPMv = new System.Windows.Forms.Label();
            this.labelSetDewPoint = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.labelCurrentDewPoint = new System.Windows.Forms.Label();
            this.labelSetFrostPoint = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelCurrentFrostPoint = new System.Windows.Forms.Label();
            this.groupBoxCalibrationStatus = new System.Windows.Forms.GroupBox();
            this.labelCalibrationRadiosondeStatus = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.labelCalibraitonRunTime = new System.Windows.Forms.Label();
            this.labelCalibrationChamberStatus = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.labelCalibrationThunderStatus = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.labelCalibrationEnclouserStatus = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.labelCalibrationCurrentUSetPoint = new System.Windows.Forms.Label();
            this.labelCalibrationCurrentATSetPoint = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.labelCalibrationCurrentUDiff = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.labelCalibrationCurrentU = new System.Windows.Forms.Label();
            this.labelCalibrationCurrentATDiff = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.labelCalibrationCurrentAT = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.statusStripCalibration = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelCalibration = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarCurrentCalibration = new System.Windows.Forms.ToolStripProgressBar();
            this.statusStripMain.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBoxCalibrationControls.SuspendLayout();
            this.groupBoxStatusRadiosondes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRadiosondes)).BeginInit();
            this.groupBoxChamber.SuspendLayout();
            this.groupBoxReferance.SuspendLayout();
            this.groupBoxThunderStatus.SuspendLayout();
            this.groupBoxCalibrationStatus.SuspendLayout();
            this.statusStripCalibration.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelMain});
            this.statusStripMain.Location = new System.Drawing.Point(0, 544);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(625, 22);
            this.statusStripMain.TabIndex = 0;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // toolStripStatusLabelMain
            // 
            this.toolStripStatusLabelMain.Name = "toolStripStatusLabelMain";
            this.toolStripStatusLabelMain.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabelMain.Text = "Ready...";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.processToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(625, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.changeUsersToolStripMenuItem,
            this.userSettingsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // changeUsersToolStripMenuItem
            // 
            this.changeUsersToolStripMenuItem.Name = "changeUsersToolStripMenuItem";
            this.changeUsersToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.changeUsersToolStripMenuItem.Text = "Change Users";
            this.changeUsersToolStripMenuItem.Click += new System.EventHandler(this.changeUsersToolStripMenuItem_Click);
            // 
            // userSettingsToolStripMenuItem
            // 
            this.userSettingsToolStripMenuItem.Name = "userSettingsToolStripMenuItem";
            this.userSettingsToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.userSettingsToolStripMenuItem.Text = "Users Settings";
            this.userSettingsToolStripMenuItem.Click += new System.EventHandler(this.userSettingsToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkRadiosondesToolStripMenuItem,
            this.toolStripSeparator2,
            this.thunderControlsToolStripMenuItem,
            this.chamberControlsToolStripMenuItem,
            this.relayControlsToolStripMenuItem,
            this.toolStripSeparator1,
            this.calcCorrectChamberTempToolStripMenuItem,
            this.testToolStripMenuItem,
            this.manuelResetToolStripMenuItem,
            this.startLoggingProformanceToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            this.toolsToolStripMenuItem.Click += new System.EventHandler(this.toolsToolStripMenuItem_Click);
            // 
            // checkRadiosondesToolStripMenuItem
            // 
            this.checkRadiosondesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.stopToolStripMenuItem});
            this.checkRadiosondesToolStripMenuItem.Name = "checkRadiosondesToolStripMenuItem";
            this.checkRadiosondesToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.checkRadiosondesToolStripMenuItem.Text = "Check Radiosonde(s)";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.startToolStripMenuItem.Text = "Start";
            this.startToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Enabled = false;
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.stopToolStripMenuItem.Text = "Stop";
            this.stopToolStripMenuItem.Click += new System.EventHandler(this.stopToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(222, 6);
            // 
            // thunderControlsToolStripMenuItem
            // 
            this.thunderControlsToolStripMenuItem.Name = "thunderControlsToolStripMenuItem";
            this.thunderControlsToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.thunderControlsToolStripMenuItem.Text = "Thunder Controls";
            this.thunderControlsToolStripMenuItem.Click += new System.EventHandler(this.thunderControlsToolStripMenuItem_Click);
            // 
            // chamberControlsToolStripMenuItem
            // 
            this.chamberControlsToolStripMenuItem.Name = "chamberControlsToolStripMenuItem";
            this.chamberControlsToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.chamberControlsToolStripMenuItem.Text = "Chamber Controls";
            this.chamberControlsToolStripMenuItem.Click += new System.EventHandler(this.chamberControlsToolStripMenuItem_Click);
            // 
            // relayControlsToolStripMenuItem
            // 
            this.relayControlsToolStripMenuItem.Name = "relayControlsToolStripMenuItem";
            this.relayControlsToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.relayControlsToolStripMenuItem.Text = "Relay Controls";
            this.relayControlsToolStripMenuItem.Click += new System.EventHandler(this.relayControlsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(222, 6);
            // 
            // calcCorrectChamberTempToolStripMenuItem
            // 
            this.calcCorrectChamberTempToolStripMenuItem.Name = "calcCorrectChamberTempToolStripMenuItem";
            this.calcCorrectChamberTempToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.calcCorrectChamberTempToolStripMenuItem.Text = "Calc Correct Chamber Temp";
            this.calcCorrectChamberTempToolStripMenuItem.Click += new System.EventHandler(this.calcCorrectChamberTempToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.testToolStripMenuItem.Text = "Test";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // manuelResetToolStripMenuItem
            // 
            this.manuelResetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thunderToolStripMenuItem,
            this.envStableToolStripMenuItem});
            this.manuelResetToolStripMenuItem.Name = "manuelResetToolStripMenuItem";
            this.manuelResetToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.manuelResetToolStripMenuItem.Text = "Manual Reset";
            // 
            // thunderToolStripMenuItem
            // 
            this.thunderToolStripMenuItem.Name = "thunderToolStripMenuItem";
            this.thunderToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.thunderToolStripMenuItem.Text = "Thunder";
            this.thunderToolStripMenuItem.Click += new System.EventHandler(this.thunderToolStripMenuItem_Click);
            // 
            // envStableToolStripMenuItem
            // 
            this.envStableToolStripMenuItem.Name = "envStableToolStripMenuItem";
            this.envStableToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.envStableToolStripMenuItem.Text = "Env Stable";
            this.envStableToolStripMenuItem.Click += new System.EventHandler(this.envStableToolStripMenuItem_Click);
            // 
            // startLoggingProformanceToolStripMenuItem
            // 
            this.startLoggingProformanceToolStripMenuItem.Name = "startLoggingProformanceToolStripMenuItem";
            this.startLoggingProformanceToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.startLoggingProformanceToolStripMenuItem.Text = "Start Logging Proformance";
            this.startLoggingProformanceToolStripMenuItem.Click += new System.EventHandler(this.startLoggingProformanceToolStripMenuItem_Click);
            // 
            // processToolStripMenuItem
            // 
            this.processToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calulateCoefficentsToolStripMenuItem,
            this.correctReferanceToolStripMenuItem,
            this.checkToolStripMenuItem});
            this.processToolStripMenuItem.Name = "processToolStripMenuItem";
            this.processToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.processToolStripMenuItem.Text = "Process";
            // 
            // calulateCoefficentsToolStripMenuItem
            // 
            this.calulateCoefficentsToolStripMenuItem.Name = "calulateCoefficentsToolStripMenuItem";
            this.calulateCoefficentsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.calulateCoefficentsToolStripMenuItem.Text = "Calculate Coefficents";
            this.calulateCoefficentsToolStripMenuItem.Click += new System.EventHandler(this.calulateCoefficentsToolStripMenuItem_Click);
            // 
            // correctReferanceToolStripMenuItem
            // 
            this.correctReferanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hMP234ToolStripMenuItem});
            this.correctReferanceToolStripMenuItem.Name = "correctReferanceToolStripMenuItem";
            this.correctReferanceToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.correctReferanceToolStripMenuItem.Text = "Correct Reference";
            this.correctReferanceToolStripMenuItem.Click += new System.EventHandler(this.correctReferanceToolStripMenuItem_Click);
            // 
            // hMP234ToolStripMenuItem
            // 
            this.hMP234ToolStripMenuItem.Name = "hMP234ToolStripMenuItem";
            this.hMP234ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.hMP234ToolStripMenuItem.Text = "HMP234";
            this.hMP234ToolStripMenuItem.Click += new System.EventHandler(this.hMP234ToolStripMenuItem_Click);
            // 
            // checkToolStripMenuItem
            // 
            this.checkToolStripMenuItem.Name = "checkToolStripMenuItem";
            this.checkToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.checkToolStripMenuItem.Text = "Check Raw Data";
            this.checkToolStripMenuItem.Click += new System.EventHandler(this.checkToolStripMenuItem_Click);
            // 
            // groupBoxCalibrationControls
            // 
            this.groupBoxCalibrationControls.Controls.Add(this.checkBoxPostCorrect);
            this.groupBoxCalibrationControls.Controls.Add(this.checkBoxShutdownAfter);
            this.groupBoxCalibrationControls.Controls.Add(this.comboBoxRadiosondeType);
            this.groupBoxCalibrationControls.Controls.Add(this.buttonStartCalibration);
            this.groupBoxCalibrationControls.Location = new System.Drawing.Point(391, 459);
            this.groupBoxCalibrationControls.Name = "groupBoxCalibrationControls";
            this.groupBoxCalibrationControls.Size = new System.Drawing.Size(226, 71);
            this.groupBoxCalibrationControls.TabIndex = 2;
            this.groupBoxCalibrationControls.TabStop = false;
            this.groupBoxCalibrationControls.Text = "Calibration Controls";
            // 
            // checkBoxPostCorrect
            // 
            this.checkBoxPostCorrect.AutoSize = true;
            this.checkBoxPostCorrect.Checked = true;
            this.checkBoxPostCorrect.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPostCorrect.Location = new System.Drawing.Point(139, 48);
            this.checkBoxPostCorrect.Name = "checkBoxPostCorrect";
            this.checkBoxPostCorrect.Size = new System.Drawing.Size(66, 17);
            this.checkBoxPostCorrect.TabIndex = 3;
            this.checkBoxPostCorrect.Text = "Post Cor";
            this.checkBoxPostCorrect.UseVisualStyleBackColor = true;
            // 
            // checkBoxShutdownAfter
            // 
            this.checkBoxShutdownAfter.AutoSize = true;
            this.checkBoxShutdownAfter.Location = new System.Drawing.Point(12, 48);
            this.checkBoxShutdownAfter.Name = "checkBoxShutdownAfter";
            this.checkBoxShutdownAfter.Size = new System.Drawing.Size(126, 17);
            this.checkBoxShutdownAfter.TabIndex = 2;
            this.checkBoxShutdownAfter.Text = "Warm and Shutdown";
            this.checkBoxShutdownAfter.UseVisualStyleBackColor = true;
            // 
            // comboBoxRadiosondeType
            // 
            this.comboBoxRadiosondeType.FormattingEnabled = true;
            this.comboBoxRadiosondeType.Items.AddRange(new object[] {
            "iMet-1",
            "Bell202"});
            this.comboBoxRadiosondeType.Location = new System.Drawing.Point(12, 21);
            this.comboBoxRadiosondeType.Name = "comboBoxRadiosondeType";
            this.comboBoxRadiosondeType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRadiosondeType.TabIndex = 1;
            this.comboBoxRadiosondeType.Text = "Radiosonde Type";
            this.comboBoxRadiosondeType.SelectedIndexChanged += new System.EventHandler(this.comboBoxRadiosondeType_SelectedIndexChanged);
            // 
            // buttonStartCalibration
            // 
            this.buttonStartCalibration.Location = new System.Drawing.Point(139, 19);
            this.buttonStartCalibration.Name = "buttonStartCalibration";
            this.buttonStartCalibration.Size = new System.Drawing.Size(75, 23);
            this.buttonStartCalibration.TabIndex = 0;
            this.buttonStartCalibration.Text = "Start";
            this.buttonStartCalibration.UseVisualStyleBackColor = true;
            this.buttonStartCalibration.Click += new System.EventHandler(this.buttonStartCalibration_Click);
            // 
            // groupBoxStatusRadiosondes
            // 
            this.groupBoxStatusRadiosondes.Controls.Add(this.comboBoxRadiosondeSelect);
            this.groupBoxStatusRadiosondes.Controls.Add(this.dataGridViewRadiosondes);
            this.groupBoxStatusRadiosondes.Location = new System.Drawing.Point(12, 27);
            this.groupBoxStatusRadiosondes.Name = "groupBoxStatusRadiosondes";
            this.groupBoxStatusRadiosondes.Size = new System.Drawing.Size(373, 362);
            this.groupBoxStatusRadiosondes.TabIndex = 3;
            this.groupBoxStatusRadiosondes.TabStop = false;
            this.groupBoxStatusRadiosondes.Text = "Radiosonde Status";
            // 
            // comboBoxRadiosondeSelect
            // 
            this.comboBoxRadiosondeSelect.FormattingEnabled = true;
            this.comboBoxRadiosondeSelect.Location = new System.Drawing.Point(76, -24);
            this.comboBoxRadiosondeSelect.Name = "comboBoxRadiosondeSelect";
            this.comboBoxRadiosondeSelect.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRadiosondeSelect.TabIndex = 1;
            this.comboBoxRadiosondeSelect.Text = "Radiosonde Type";
            // 
            // dataGridViewRadiosondes
            // 
            this.dataGridViewRadiosondes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewRadiosondes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRadiosondes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnLOC,
            this.ColumnTrackingNumber,
            this.ColumnPacketCount,
            this.ColumnAirTempDiff,
            this.ColumnHumidityCount,
            this.ColumnFirmware});
            this.dataGridViewRadiosondes.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewRadiosondes.Name = "dataGridViewRadiosondes";
            this.dataGridViewRadiosondes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridViewRadiosondes.RowHeadersVisible = false;
            this.dataGridViewRadiosondes.Size = new System.Drawing.Size(349, 334);
            this.dataGridViewRadiosondes.TabIndex = 1;
            this.dataGridViewRadiosondes.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRadiosondes_CellContentDoubleClick);
            this.dataGridViewRadiosondes.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewRadiosondes_CellMouseDown);
            // 
            // ColumnLOC
            // 
            this.ColumnLOC.HeaderText = "LOC";
            this.ColumnLOC.Name = "ColumnLOC";
            this.ColumnLOC.Width = 50;
            // 
            // ColumnTrackingNumber
            // 
            this.ColumnTrackingNumber.HeaderText = "SondeTN";
            this.ColumnTrackingNumber.Name = "ColumnTrackingNumber";
            this.ColumnTrackingNumber.Width = 60;
            // 
            // ColumnPacketCount
            // 
            this.ColumnPacketCount.HeaderText = "Count";
            this.ColumnPacketCount.Name = "ColumnPacketCount";
            this.ColumnPacketCount.Width = 40;
            // 
            // ColumnAirTempDiff
            // 
            this.ColumnAirTempDiff.HeaderText = "ATDiff";
            this.ColumnAirTempDiff.Name = "ColumnAirTempDiff";
            this.ColumnAirTempDiff.Width = 40;
            // 
            // ColumnHumidityCount
            // 
            this.ColumnHumidityCount.FillWeight = 10F;
            this.ColumnHumidityCount.HeaderText = "U Count";
            this.ColumnHumidityCount.Name = "ColumnHumidityCount";
            this.ColumnHumidityCount.Width = 75;
            // 
            // ColumnFirmware
            // 
            this.ColumnFirmware.HeaderText = "Firmware";
            this.ColumnFirmware.Name = "ColumnFirmware";
            this.ColumnFirmware.Width = 60;
            // 
            // groupBoxChamber
            // 
            this.groupBoxChamber.Controls.Add(this.labelChamerRelay);
            this.groupBoxChamber.Controls.Add(this.label8);
            this.groupBoxChamber.Controls.Add(this.label9);
            this.groupBoxChamber.Controls.Add(this.label7);
            this.groupBoxChamber.Controls.Add(this.label23);
            this.groupBoxChamber.Controls.Add(this.labelChamberHumiditySetPoint);
            this.groupBoxChamber.Controls.Add(this.labelChamberAirTempSetPoint);
            this.groupBoxChamber.Controls.Add(this.label29);
            this.groupBoxChamber.Controls.Add(this.label16);
            this.groupBoxChamber.Controls.Add(this.label18);
            this.groupBoxChamber.Controls.Add(this.labelChamberHumidityRamp);
            this.groupBoxChamber.Controls.Add(this.labelChamberAirTempRamp);
            this.groupBoxChamber.Controls.Add(this.label25);
            this.groupBoxChamber.Controls.Add(this.label5);
            this.groupBoxChamber.Controls.Add(this.label4);
            this.groupBoxChamber.Controls.Add(this.labelChamberHumidityAvg);
            this.groupBoxChamber.Controls.Add(this.label6);
            this.groupBoxChamber.Controls.Add(this.labelChamberHumidity);
            this.groupBoxChamber.Controls.Add(this.labelChamberAirTempAvg);
            this.groupBoxChamber.Controls.Add(this.label3);
            this.groupBoxChamber.Controls.Add(this.labelChamberAirTemp);
            this.groupBoxChamber.Controls.Add(this.label2);
            this.groupBoxChamber.Controls.Add(this.label1);
            this.groupBoxChamber.Location = new System.Drawing.Point(391, 27);
            this.groupBoxChamber.Name = "groupBoxChamber";
            this.groupBoxChamber.Size = new System.Drawing.Size(226, 138);
            this.groupBoxChamber.TabIndex = 4;
            this.groupBoxChamber.TabStop = false;
            this.groupBoxChamber.Text = "Chamber Status";
            // 
            // labelChamerRelay
            // 
            this.labelChamerRelay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelChamerRelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelChamerRelay.Location = new System.Drawing.Point(34, 104);
            this.labelChamerRelay.Name = "labelChamerRelay";
            this.labelChamerRelay.Size = new System.Drawing.Size(166, 22);
            this.labelChamerRelay.TabIndex = 24;
            this.labelChamerRelay.Text = "Ready";
            this.labelChamerRelay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelChamerRelay.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.labelChamerRelay_MouseDoubleClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "%";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(205, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "%";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(205, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "%";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(125, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(18, 13);
            this.label23.TabIndex = 20;
            this.label23.Text = "°C";
            // 
            // labelChamberHumiditySetPoint
            // 
            this.labelChamberHumiditySetPoint.BackColor = System.Drawing.Color.White;
            this.labelChamberHumiditySetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberHumiditySetPoint.Enabled = false;
            this.labelChamberHumiditySetPoint.Location = new System.Drawing.Point(154, 29);
            this.labelChamberHumiditySetPoint.Name = "labelChamberHumiditySetPoint";
            this.labelChamberHumiditySetPoint.Size = new System.Drawing.Size(50, 15);
            this.labelChamberHumiditySetPoint.TabIndex = 19;
            this.labelChamberHumiditySetPoint.Text = "000.00";
            this.labelChamberHumiditySetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelChamberAirTempSetPoint
            // 
            this.labelChamberAirTempSetPoint.BackColor = System.Drawing.Color.White;
            this.labelChamberAirTempSetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberAirTempSetPoint.Location = new System.Drawing.Point(75, 29);
            this.labelChamberAirTempSetPoint.Name = "labelChamberAirTempSetPoint";
            this.labelChamberAirTempSetPoint.Size = new System.Drawing.Size(50, 15);
            this.labelChamberAirTempSetPoint.TabIndex = 18;
            this.labelChamberAirTempSetPoint.Text = "000.00";
            this.labelChamberAirTempSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(19, 30);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 13);
            this.label29.TabIndex = 17;
            this.label29.Text = "Set Point:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(205, 81);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "%";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(127, 81);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 13);
            this.label18.TabIndex = 15;
            this.label18.Text = "%";
            // 
            // labelChamberHumidityRamp
            // 
            this.labelChamberHumidityRamp.BackColor = System.Drawing.Color.White;
            this.labelChamberHumidityRamp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberHumidityRamp.Enabled = false;
            this.labelChamberHumidityRamp.Location = new System.Drawing.Point(154, 80);
            this.labelChamberHumidityRamp.Name = "labelChamberHumidityRamp";
            this.labelChamberHumidityRamp.Size = new System.Drawing.Size(50, 15);
            this.labelChamberHumidityRamp.TabIndex = 14;
            this.labelChamberHumidityRamp.Text = "000.00";
            this.labelChamberHumidityRamp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelChamberAirTempRamp
            // 
            this.labelChamberAirTempRamp.BackColor = System.Drawing.Color.White;
            this.labelChamberAirTempRamp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberAirTempRamp.Location = new System.Drawing.Point(75, 80);
            this.labelChamberAirTempRamp.Name = "labelChamberAirTempRamp";
            this.labelChamberAirTempRamp.Size = new System.Drawing.Size(50, 15);
            this.labelChamberAirTempRamp.TabIndex = 13;
            this.labelChamberAirTempRamp.Text = "000.00";
            this.labelChamberAirTempRamp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(34, 81);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(38, 13);
            this.label25.TabIndex = 12;
            this.label25.Text = "Ramp:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(125, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "°C";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(125, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "°C";
            // 
            // labelChamberHumidityAvg
            // 
            this.labelChamberHumidityAvg.BackColor = System.Drawing.Color.White;
            this.labelChamberHumidityAvg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberHumidityAvg.Enabled = false;
            this.labelChamberHumidityAvg.Location = new System.Drawing.Point(154, 63);
            this.labelChamberHumidityAvg.Name = "labelChamberHumidityAvg";
            this.labelChamberHumidityAvg.Size = new System.Drawing.Size(50, 15);
            this.labelChamberHumidityAvg.TabIndex = 7;
            this.labelChamberHumidityAvg.Text = "000.00";
            this.labelChamberHumidityAvg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(157, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Humidity";
            // 
            // labelChamberHumidity
            // 
            this.labelChamberHumidity.BackColor = System.Drawing.Color.White;
            this.labelChamberHumidity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberHumidity.Enabled = false;
            this.labelChamberHumidity.Location = new System.Drawing.Point(154, 46);
            this.labelChamberHumidity.Name = "labelChamberHumidity";
            this.labelChamberHumidity.Size = new System.Drawing.Size(50, 15);
            this.labelChamberHumidity.TabIndex = 5;
            this.labelChamberHumidity.Text = "000.00";
            this.labelChamberHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelChamberAirTempAvg
            // 
            this.labelChamberAirTempAvg.BackColor = System.Drawing.Color.White;
            this.labelChamberAirTempAvg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberAirTempAvg.Location = new System.Drawing.Point(75, 63);
            this.labelChamberAirTempAvg.Name = "labelChamberAirTempAvg";
            this.labelChamberAirTempAvg.Size = new System.Drawing.Size(50, 15);
            this.labelChamberAirTempAvg.TabIndex = 4;
            this.labelChamberAirTempAvg.Text = "000.00";
            this.labelChamberAirTempAvg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Air Temp";
            // 
            // labelChamberAirTemp
            // 
            this.labelChamberAirTemp.BackColor = System.Drawing.Color.White;
            this.labelChamberAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberAirTemp.Location = new System.Drawing.Point(75, 46);
            this.labelChamberAirTemp.Name = "labelChamberAirTemp";
            this.labelChamberAirTemp.Size = new System.Drawing.Size(50, 15);
            this.labelChamberAirTemp.TabIndex = 2;
            this.labelChamberAirTemp.Text = "000.00";
            this.labelChamberAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Change/Min:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Current:";
            // 
            // groupBoxReferance
            // 
            this.groupBoxReferance.Controls.Add(this.label10);
            this.groupBoxReferance.Controls.Add(this.label22);
            this.groupBoxReferance.Controls.Add(this.label20);
            this.groupBoxReferance.Controls.Add(this.label19);
            this.groupBoxReferance.Controls.Add(this.label11);
            this.groupBoxReferance.Controls.Add(this.labelReferanceAirTemp);
            this.groupBoxReferance.Controls.Add(this.label12);
            this.groupBoxReferance.Controls.Add(this.label17);
            this.groupBoxReferance.Controls.Add(this.labelReferanceHumidityAvg);
            this.groupBoxReferance.Controls.Add(this.labelReferanceAirTempAvg);
            this.groupBoxReferance.Controls.Add(this.label14);
            this.groupBoxReferance.Controls.Add(this.labelReferanceHumidity);
            this.groupBoxReferance.Location = new System.Drawing.Point(391, 171);
            this.groupBoxReferance.Name = "groupBoxReferance";
            this.groupBoxReferance.Size = new System.Drawing.Size(226, 65);
            this.groupBoxReferance.TabIndex = 5;
            this.groupBoxReferance.TabStop = false;
            this.groupBoxReferance.Text = "Enclosure 1 Reference Status";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(205, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "%";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(205, 45);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 13);
            this.label22.TabIndex = 24;
            this.label22.Text = "%";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(29, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Current:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 13);
            this.label19.TabIndex = 13;
            this.label19.Text = "Change/Min:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(126, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "°C";
            // 
            // labelReferanceAirTemp
            // 
            this.labelReferanceAirTemp.BackColor = System.Drawing.Color.White;
            this.labelReferanceAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelReferanceAirTemp.Location = new System.Drawing.Point(76, 27);
            this.labelReferanceAirTemp.Name = "labelReferanceAirTemp";
            this.labelReferanceAirTemp.Size = new System.Drawing.Size(50, 15);
            this.labelReferanceAirTemp.TabIndex = 14;
            this.labelReferanceAirTemp.Text = "000.00";
            this.labelReferanceAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(126, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "°C";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(78, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "Air Temp";
            // 
            // labelReferanceHumidityAvg
            // 
            this.labelReferanceHumidityAvg.BackColor = System.Drawing.Color.White;
            this.labelReferanceHumidityAvg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelReferanceHumidityAvg.Location = new System.Drawing.Point(155, 44);
            this.labelReferanceHumidityAvg.Name = "labelReferanceHumidityAvg";
            this.labelReferanceHumidityAvg.Size = new System.Drawing.Size(50, 15);
            this.labelReferanceHumidityAvg.TabIndex = 19;
            this.labelReferanceHumidityAvg.Text = "000.00";
            this.labelReferanceHumidityAvg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelReferanceAirTempAvg
            // 
            this.labelReferanceAirTempAvg.BackColor = System.Drawing.Color.White;
            this.labelReferanceAirTempAvg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelReferanceAirTempAvg.Location = new System.Drawing.Point(76, 44);
            this.labelReferanceAirTempAvg.Name = "labelReferanceAirTempAvg";
            this.labelReferanceAirTempAvg.Size = new System.Drawing.Size(50, 15);
            this.labelReferanceAirTempAvg.TabIndex = 16;
            this.labelReferanceAirTempAvg.Text = "000.00";
            this.labelReferanceAirTempAvg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(157, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Humidity";
            // 
            // labelReferanceHumidity
            // 
            this.labelReferanceHumidity.BackColor = System.Drawing.Color.White;
            this.labelReferanceHumidity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelReferanceHumidity.Location = new System.Drawing.Point(155, 27);
            this.labelReferanceHumidity.Name = "labelReferanceHumidity";
            this.labelReferanceHumidity.Size = new System.Drawing.Size(50, 15);
            this.labelReferanceHumidity.TabIndex = 17;
            this.labelReferanceHumidity.Text = "000.00";
            this.labelReferanceHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxThunderStatus
            // 
            this.groupBoxThunderStatus.Controls.Add(this.label15);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentStatus);
            this.groupBoxThunderStatus.Controls.Add(this.label48);
            this.groupBoxThunderStatus.Controls.Add(this.label47);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetFlow);
            this.groupBoxThunderStatus.Controls.Add(this.label42);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentFlow);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetTestC);
            this.groupBoxThunderStatus.Controls.Add(this.label45);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentTestC);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetTestPSI);
            this.groupBoxThunderStatus.Controls.Add(this.label30);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentTestPSI);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetSaturC);
            this.groupBoxThunderStatus.Controls.Add(this.label33);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentSaturC);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetSaturPSI);
            this.groupBoxThunderStatus.Controls.Add(this.label36);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentSaturPSI);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetRH);
            this.groupBoxThunderStatus.Controls.Add(this.label39);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentRH);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetPPMw);
            this.groupBoxThunderStatus.Controls.Add(this.label24);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentPPMw);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetPPMv);
            this.groupBoxThunderStatus.Controls.Add(this.label27);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentPPMv);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetDewPoint);
            this.groupBoxThunderStatus.Controls.Add(this.label21);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentDewPoint);
            this.groupBoxThunderStatus.Controls.Add(this.labelSetFrostPoint);
            this.groupBoxThunderStatus.Controls.Add(this.label13);
            this.groupBoxThunderStatus.Controls.Add(this.labelCurrentFrostPoint);
            this.groupBoxThunderStatus.Location = new System.Drawing.Point(391, 242);
            this.groupBoxThunderStatus.Name = "groupBoxThunderStatus";
            this.groupBoxThunderStatus.Size = new System.Drawing.Size(226, 211);
            this.groupBoxThunderStatus.TabIndex = 6;
            this.groupBoxThunderStatus.TabStop = false;
            this.groupBoxThunderStatus.Text = "Thunder Status";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(56, 189);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 55;
            this.label15.Text = "Status";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentStatus
            // 
            this.labelCurrentStatus.BackColor = System.Drawing.Color.White;
            this.labelCurrentStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentStatus.Location = new System.Drawing.Point(113, 188);
            this.labelCurrentStatus.Name = "labelCurrentStatus";
            this.labelCurrentStatus.Size = new System.Drawing.Size(76, 15);
            this.labelCurrentStatus.TabIndex = 56;
            this.labelCurrentStatus.Text = "XXXXXXXXX";
            this.labelCurrentStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(103, 12);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(42, 13);
            this.label48.TabIndex = 54;
            this.label48.Text = "Set Pnt";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(159, 12);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(41, 13);
            this.label47.TabIndex = 24;
            this.label47.Text = "Current";
            // 
            // labelSetFlow
            // 
            this.labelSetFlow.BackColor = System.Drawing.Color.White;
            this.labelSetFlow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetFlow.Location = new System.Drawing.Point(99, 172);
            this.labelSetFlow.Name = "labelSetFlow";
            this.labelSetFlow.Size = new System.Drawing.Size(50, 15);
            this.labelSetFlow.TabIndex = 53;
            this.labelSetFlow.Text = "000.00";
            this.labelSetFlow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(46, 173);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(47, 13);
            this.label42.TabIndex = 51;
            this.label42.Text = "Flow l/m";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentFlow
            // 
            this.labelCurrentFlow.BackColor = System.Drawing.Color.White;
            this.labelCurrentFlow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentFlow.Location = new System.Drawing.Point(155, 172);
            this.labelCurrentFlow.Name = "labelCurrentFlow";
            this.labelCurrentFlow.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentFlow.TabIndex = 52;
            this.labelCurrentFlow.Text = "000.00";
            this.labelCurrentFlow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetTestC
            // 
            this.labelSetTestC.BackColor = System.Drawing.Color.White;
            this.labelSetTestC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetTestC.Location = new System.Drawing.Point(99, 156);
            this.labelSetTestC.Name = "labelSetTestC";
            this.labelSetTestC.Size = new System.Drawing.Size(50, 15);
            this.labelSetTestC.TabIndex = 50;
            this.labelSetTestC.Text = "000.00";
            this.labelSetTestC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(51, 157);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(42, 13);
            this.label45.TabIndex = 48;
            this.label45.Text = "Test °C";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentTestC
            // 
            this.labelCurrentTestC.BackColor = System.Drawing.Color.White;
            this.labelCurrentTestC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentTestC.Location = new System.Drawing.Point(155, 156);
            this.labelCurrentTestC.Name = "labelCurrentTestC";
            this.labelCurrentTestC.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentTestC.TabIndex = 49;
            this.labelCurrentTestC.Text = "000.00";
            this.labelCurrentTestC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetTestPSI
            // 
            this.labelSetTestPSI.BackColor = System.Drawing.Color.White;
            this.labelSetTestPSI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetTestPSI.Location = new System.Drawing.Point(99, 140);
            this.labelSetTestPSI.Name = "labelSetTestPSI";
            this.labelSetTestPSI.Size = new System.Drawing.Size(50, 15);
            this.labelSetTestPSI.TabIndex = 47;
            this.labelSetTestPSI.Text = "000.00";
            this.labelSetTestPSI.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(45, 141);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(48, 13);
            this.label30.TabIndex = 45;
            this.label30.Text = "Test PSI";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentTestPSI
            // 
            this.labelCurrentTestPSI.BackColor = System.Drawing.Color.White;
            this.labelCurrentTestPSI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentTestPSI.Location = new System.Drawing.Point(155, 140);
            this.labelCurrentTestPSI.Name = "labelCurrentTestPSI";
            this.labelCurrentTestPSI.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentTestPSI.TabIndex = 46;
            this.labelCurrentTestPSI.Text = "000.00";
            this.labelCurrentTestPSI.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetSaturC
            // 
            this.labelSetSaturC.BackColor = System.Drawing.Color.White;
            this.labelSetSaturC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetSaturC.Location = new System.Drawing.Point(99, 124);
            this.labelSetSaturC.Name = "labelSetSaturC";
            this.labelSetSaturC.Size = new System.Drawing.Size(50, 15);
            this.labelSetSaturC.TabIndex = 44;
            this.labelSetSaturC.Text = "000.00";
            this.labelSetSaturC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(47, 125);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 13);
            this.label33.TabIndex = 42;
            this.label33.Text = "Satur °C";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentSaturC
            // 
            this.labelCurrentSaturC.BackColor = System.Drawing.Color.White;
            this.labelCurrentSaturC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentSaturC.Location = new System.Drawing.Point(155, 124);
            this.labelCurrentSaturC.Name = "labelCurrentSaturC";
            this.labelCurrentSaturC.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentSaturC.TabIndex = 43;
            this.labelCurrentSaturC.Text = "000.00";
            this.labelCurrentSaturC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetSaturPSI
            // 
            this.labelSetSaturPSI.BackColor = System.Drawing.Color.White;
            this.labelSetSaturPSI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetSaturPSI.Location = new System.Drawing.Point(99, 108);
            this.labelSetSaturPSI.Name = "labelSetSaturPSI";
            this.labelSetSaturPSI.Size = new System.Drawing.Size(50, 15);
            this.labelSetSaturPSI.TabIndex = 41;
            this.labelSetSaturPSI.Text = "000.00";
            this.labelSetSaturPSI.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(41, 109);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(52, 13);
            this.label36.TabIndex = 39;
            this.label36.Text = "Satur PSI";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentSaturPSI
            // 
            this.labelCurrentSaturPSI.BackColor = System.Drawing.Color.White;
            this.labelCurrentSaturPSI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentSaturPSI.Location = new System.Drawing.Point(155, 108);
            this.labelCurrentSaturPSI.Name = "labelCurrentSaturPSI";
            this.labelCurrentSaturPSI.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentSaturPSI.TabIndex = 40;
            this.labelCurrentSaturPSI.Text = "000.00";
            this.labelCurrentSaturPSI.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetRH
            // 
            this.labelSetRH.BackColor = System.Drawing.Color.White;
            this.labelSetRH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetRH.Location = new System.Drawing.Point(99, 92);
            this.labelSetRH.Name = "labelSetRH";
            this.labelSetRH.Size = new System.Drawing.Size(50, 15);
            this.labelSetRH.TabIndex = 38;
            this.labelSetRH.Text = "000.00";
            this.labelSetRH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(62, 93);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(31, 13);
            this.label39.TabIndex = 36;
            this.label39.Text = "%RH";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentRH
            // 
            this.labelCurrentRH.BackColor = System.Drawing.Color.White;
            this.labelCurrentRH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentRH.Location = new System.Drawing.Point(155, 92);
            this.labelCurrentRH.Name = "labelCurrentRH";
            this.labelCurrentRH.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentRH.TabIndex = 37;
            this.labelCurrentRH.Text = "000.00";
            this.labelCurrentRH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetPPMw
            // 
            this.labelSetPPMw.BackColor = System.Drawing.Color.White;
            this.labelSetPPMw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetPPMw.Location = new System.Drawing.Point(99, 76);
            this.labelSetPPMw.Name = "labelSetPPMw";
            this.labelSetPPMw.Size = new System.Drawing.Size(50, 15);
            this.labelSetPPMw.TabIndex = 35;
            this.labelSetPPMw.Text = "000.00";
            this.labelSetPPMw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(55, 77);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(38, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "PPMw";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentPPMw
            // 
            this.labelCurrentPPMw.BackColor = System.Drawing.Color.White;
            this.labelCurrentPPMw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentPPMw.Location = new System.Drawing.Point(155, 76);
            this.labelCurrentPPMw.Name = "labelCurrentPPMw";
            this.labelCurrentPPMw.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentPPMw.TabIndex = 34;
            this.labelCurrentPPMw.Text = "000.00";
            this.labelCurrentPPMw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetPPMv
            // 
            this.labelSetPPMv.BackColor = System.Drawing.Color.White;
            this.labelSetPPMv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetPPMv.Location = new System.Drawing.Point(99, 60);
            this.labelSetPPMv.Name = "labelSetPPMv";
            this.labelSetPPMv.Size = new System.Drawing.Size(50, 15);
            this.labelSetPPMv.TabIndex = 32;
            this.labelSetPPMv.Text = "000.00";
            this.labelSetPPMv.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(57, 61);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(36, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "PPMv";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentPPMv
            // 
            this.labelCurrentPPMv.BackColor = System.Drawing.Color.White;
            this.labelCurrentPPMv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentPPMv.Location = new System.Drawing.Point(155, 60);
            this.labelCurrentPPMv.Name = "labelCurrentPPMv";
            this.labelCurrentPPMv.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentPPMv.TabIndex = 31;
            this.labelCurrentPPMv.Text = "000.00";
            this.labelCurrentPPMv.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetDewPoint
            // 
            this.labelSetDewPoint.BackColor = System.Drawing.Color.White;
            this.labelSetDewPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetDewPoint.Location = new System.Drawing.Point(99, 44);
            this.labelSetDewPoint.Name = "labelSetDewPoint";
            this.labelSetDewPoint.Size = new System.Drawing.Size(50, 15);
            this.labelSetDewPoint.TabIndex = 29;
            this.labelSetDewPoint.Text = "000.00";
            this.labelSetDewPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(33, 45);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Dew PT °C";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentDewPoint
            // 
            this.labelCurrentDewPoint.BackColor = System.Drawing.Color.White;
            this.labelCurrentDewPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentDewPoint.Location = new System.Drawing.Point(155, 44);
            this.labelCurrentDewPoint.Name = "labelCurrentDewPoint";
            this.labelCurrentDewPoint.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentDewPoint.TabIndex = 28;
            this.labelCurrentDewPoint.Text = "000.00";
            this.labelCurrentDewPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSetFrostPoint
            // 
            this.labelSetFrostPoint.BackColor = System.Drawing.Color.White;
            this.labelSetFrostPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSetFrostPoint.Location = new System.Drawing.Point(99, 28);
            this.labelSetFrostPoint.Name = "labelSetFrostPoint";
            this.labelSetFrostPoint.Size = new System.Drawing.Size(50, 15);
            this.labelSetFrostPoint.TabIndex = 26;
            this.labelSetFrostPoint.Text = "000.00";
            this.labelSetFrostPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Frost PT °C";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCurrentFrostPoint
            // 
            this.labelCurrentFrostPoint.BackColor = System.Drawing.Color.White;
            this.labelCurrentFrostPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentFrostPoint.Location = new System.Drawing.Point(155, 28);
            this.labelCurrentFrostPoint.Name = "labelCurrentFrostPoint";
            this.labelCurrentFrostPoint.Size = new System.Drawing.Size(50, 15);
            this.labelCurrentFrostPoint.TabIndex = 25;
            this.labelCurrentFrostPoint.Text = "000.00";
            this.labelCurrentFrostPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxCalibrationStatus
            // 
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationRadiosondeStatus);
            this.groupBoxCalibrationStatus.Controls.Add(this.label38);
            this.groupBoxCalibrationStatus.Controls.Add(this.label34);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibraitonRunTime);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationChamberStatus);
            this.groupBoxCalibrationStatus.Controls.Add(this.label40);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationThunderStatus);
            this.groupBoxCalibrationStatus.Controls.Add(this.label43);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationEnclouserStatus);
            this.groupBoxCalibrationStatus.Controls.Add(this.label58);
            this.groupBoxCalibrationStatus.Controls.Add(this.label59);
            this.groupBoxCalibrationStatus.Controls.Add(this.label26);
            this.groupBoxCalibrationStatus.Controls.Add(this.label28);
            this.groupBoxCalibrationStatus.Controls.Add(this.label31);
            this.groupBoxCalibrationStatus.Controls.Add(this.label32);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationCurrentUSetPoint);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationCurrentATSetPoint);
            this.groupBoxCalibrationStatus.Controls.Add(this.label37);
            this.groupBoxCalibrationStatus.Controls.Add(this.label46);
            this.groupBoxCalibrationStatus.Controls.Add(this.label49);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationCurrentUDiff);
            this.groupBoxCalibrationStatus.Controls.Add(this.label51);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationCurrentU);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationCurrentATDiff);
            this.groupBoxCalibrationStatus.Controls.Add(this.label54);
            this.groupBoxCalibrationStatus.Controls.Add(this.labelCalibrationCurrentAT);
            this.groupBoxCalibrationStatus.Controls.Add(this.label56);
            this.groupBoxCalibrationStatus.Controls.Add(this.label57);
            this.groupBoxCalibrationStatus.Controls.Add(this.statusStripCalibration);
            this.groupBoxCalibrationStatus.Enabled = false;
            this.groupBoxCalibrationStatus.Location = new System.Drawing.Point(12, 395);
            this.groupBoxCalibrationStatus.Name = "groupBoxCalibrationStatus";
            this.groupBoxCalibrationStatus.Size = new System.Drawing.Size(373, 135);
            this.groupBoxCalibrationStatus.TabIndex = 7;
            this.groupBoxCalibrationStatus.TabStop = false;
            this.groupBoxCalibrationStatus.Text = "Enclosure 1 Calibration Status";
            // 
            // labelCalibrationRadiosondeStatus
            // 
            this.labelCalibrationRadiosondeStatus.BackColor = System.Drawing.Color.White;
            this.labelCalibrationRadiosondeStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationRadiosondeStatus.Location = new System.Drawing.Point(289, 84);
            this.labelCalibrationRadiosondeStatus.Name = "labelCalibrationRadiosondeStatus";
            this.labelCalibrationRadiosondeStatus.Size = new System.Drawing.Size(70, 15);
            this.labelCalibrationRadiosondeStatus.TabIndex = 56;
            this.labelCalibrationRadiosondeStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(237, 84);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(46, 13);
            this.label38.TabIndex = 55;
            this.label38.Text = "Sondes:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(49, 21);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(74, 13);
            this.label34.TabIndex = 54;
            this.label34.Text = "Elapsed Time:";
            // 
            // labelCalibraitonRunTime
            // 
            this.labelCalibraitonRunTime.BackColor = System.Drawing.Color.White;
            this.labelCalibraitonRunTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibraitonRunTime.Location = new System.Drawing.Point(129, 20);
            this.labelCalibraitonRunTime.Name = "labelCalibraitonRunTime";
            this.labelCalibraitonRunTime.Size = new System.Drawing.Size(90, 15);
            this.labelCalibraitonRunTime.TabIndex = 53;
            this.labelCalibraitonRunTime.Text = "XXXX";
            this.labelCalibraitonRunTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCalibrationChamberStatus
            // 
            this.labelCalibrationChamberStatus.BackColor = System.Drawing.Color.White;
            this.labelCalibrationChamberStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationChamberStatus.Location = new System.Drawing.Point(289, 33);
            this.labelCalibrationChamberStatus.Name = "labelCalibrationChamberStatus";
            this.labelCalibrationChamberStatus.Size = new System.Drawing.Size(70, 15);
            this.labelCalibrationChamberStatus.TabIndex = 52;
            this.labelCalibrationChamberStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(231, 33);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(52, 13);
            this.label40.TabIndex = 51;
            this.label40.Text = "Chamber:";
            // 
            // labelCalibrationThunderStatus
            // 
            this.labelCalibrationThunderStatus.BackColor = System.Drawing.Color.White;
            this.labelCalibrationThunderStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationThunderStatus.Location = new System.Drawing.Point(289, 67);
            this.labelCalibrationThunderStatus.Name = "labelCalibrationThunderStatus";
            this.labelCalibrationThunderStatus.Size = new System.Drawing.Size(70, 15);
            this.labelCalibrationThunderStatus.TabIndex = 50;
            this.labelCalibrationThunderStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(307, 16);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(38, 13);
            this.label43.TabIndex = 49;
            this.label43.Text = "Ready";
            // 
            // labelCalibrationEnclouserStatus
            // 
            this.labelCalibrationEnclouserStatus.BackColor = System.Drawing.Color.White;
            this.labelCalibrationEnclouserStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationEnclouserStatus.Location = new System.Drawing.Point(289, 50);
            this.labelCalibrationEnclouserStatus.Name = "labelCalibrationEnclouserStatus";
            this.labelCalibrationEnclouserStatus.Size = new System.Drawing.Size(70, 15);
            this.labelCalibrationEnclouserStatus.TabIndex = 48;
            this.labelCalibrationEnclouserStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(233, 67);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(50, 13);
            this.label58.TabIndex = 47;
            this.label58.Text = "Thunder:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(226, 50);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(57, 13);
            this.label59.TabIndex = 46;
            this.label59.Text = "Enclosure:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(206, 58);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(15, 13);
            this.label26.TabIndex = 45;
            this.label26.Text = "%";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(206, 75);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 13);
            this.label28.TabIndex = 44;
            this.label28.Text = "%";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(206, 92);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(15, 13);
            this.label31.TabIndex = 43;
            this.label31.Text = "%";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(126, 58);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(18, 13);
            this.label32.TabIndex = 42;
            this.label32.Text = "°C";
            // 
            // labelCalibrationCurrentUSetPoint
            // 
            this.labelCalibrationCurrentUSetPoint.BackColor = System.Drawing.Color.White;
            this.labelCalibrationCurrentUSetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationCurrentUSetPoint.Location = new System.Drawing.Point(155, 57);
            this.labelCalibrationCurrentUSetPoint.Name = "labelCalibrationCurrentUSetPoint";
            this.labelCalibrationCurrentUSetPoint.Size = new System.Drawing.Size(50, 15);
            this.labelCalibrationCurrentUSetPoint.TabIndex = 41;
            this.labelCalibrationCurrentUSetPoint.Text = "000.00";
            this.labelCalibrationCurrentUSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCalibrationCurrentATSetPoint
            // 
            this.labelCalibrationCurrentATSetPoint.BackColor = System.Drawing.Color.White;
            this.labelCalibrationCurrentATSetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationCurrentATSetPoint.Location = new System.Drawing.Point(76, 57);
            this.labelCalibrationCurrentATSetPoint.Name = "labelCalibrationCurrentATSetPoint";
            this.labelCalibrationCurrentATSetPoint.Size = new System.Drawing.Size(50, 15);
            this.labelCalibrationCurrentATSetPoint.TabIndex = 40;
            this.labelCalibrationCurrentATSetPoint.Text = "000.00";
            this.labelCalibrationCurrentATSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(20, 58);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 13);
            this.label37.TabIndex = 39;
            this.label37.Text = "Set Point:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(126, 92);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(18, 13);
            this.label46.TabIndex = 33;
            this.label46.Text = "°C";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(126, 75);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(18, 13);
            this.label49.TabIndex = 32;
            this.label49.Text = "°C";
            // 
            // labelCalibrationCurrentUDiff
            // 
            this.labelCalibrationCurrentUDiff.BackColor = System.Drawing.Color.White;
            this.labelCalibrationCurrentUDiff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationCurrentUDiff.Location = new System.Drawing.Point(155, 91);
            this.labelCalibrationCurrentUDiff.Name = "labelCalibrationCurrentUDiff";
            this.labelCalibrationCurrentUDiff.Size = new System.Drawing.Size(50, 15);
            this.labelCalibrationCurrentUDiff.TabIndex = 31;
            this.labelCalibrationCurrentUDiff.Text = "000.00";
            this.labelCalibrationCurrentUDiff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(158, 41);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(47, 13);
            this.label51.TabIndex = 30;
            this.label51.Text = "Humidity";
            // 
            // labelCalibrationCurrentU
            // 
            this.labelCalibrationCurrentU.BackColor = System.Drawing.Color.White;
            this.labelCalibrationCurrentU.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationCurrentU.Location = new System.Drawing.Point(155, 74);
            this.labelCalibrationCurrentU.Name = "labelCalibrationCurrentU";
            this.labelCalibrationCurrentU.Size = new System.Drawing.Size(50, 15);
            this.labelCalibrationCurrentU.TabIndex = 29;
            this.labelCalibrationCurrentU.Text = "000.00";
            this.labelCalibrationCurrentU.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCalibrationCurrentATDiff
            // 
            this.labelCalibrationCurrentATDiff.BackColor = System.Drawing.Color.White;
            this.labelCalibrationCurrentATDiff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationCurrentATDiff.Location = new System.Drawing.Point(76, 91);
            this.labelCalibrationCurrentATDiff.Name = "labelCalibrationCurrentATDiff";
            this.labelCalibrationCurrentATDiff.Size = new System.Drawing.Size(50, 15);
            this.labelCalibrationCurrentATDiff.TabIndex = 28;
            this.labelCalibrationCurrentATDiff.Text = "000.00";
            this.labelCalibrationCurrentATDiff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(79, 41);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(49, 13);
            this.label54.TabIndex = 27;
            this.label54.Text = "Air Temp";
            // 
            // labelCalibrationCurrentAT
            // 
            this.labelCalibrationCurrentAT.BackColor = System.Drawing.Color.White;
            this.labelCalibrationCurrentAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationCurrentAT.Location = new System.Drawing.Point(76, 74);
            this.labelCalibrationCurrentAT.Name = "labelCalibrationCurrentAT";
            this.labelCalibrationCurrentAT.Size = new System.Drawing.Size(50, 15);
            this.labelCalibrationCurrentAT.TabIndex = 26;
            this.labelCalibrationCurrentAT.Text = "000.00";
            this.labelCalibrationCurrentAT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(14, 92);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(59, 13);
            this.label56.TabIndex = 25;
            this.label56.Text = "Difference:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(29, 75);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(44, 13);
            this.label57.TabIndex = 24;
            this.label57.Text = "Current:";
            // 
            // statusStripCalibration
            // 
            this.statusStripCalibration.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelCalibration,
            this.toolStripProgressBarCurrentCalibration});
            this.statusStripCalibration.Location = new System.Drawing.Point(3, 110);
            this.statusStripCalibration.Name = "statusStripCalibration";
            this.statusStripCalibration.Size = new System.Drawing.Size(367, 22);
            this.statusStripCalibration.TabIndex = 1;
            this.statusStripCalibration.Text = "statusStrip1";
            // 
            // toolStripStatusLabelCalibration
            // 
            this.toolStripStatusLabelCalibration.AutoSize = false;
            this.toolStripStatusLabelCalibration.Name = "toolStripStatusLabelCalibration";
            this.toolStripStatusLabelCalibration.Size = new System.Drawing.Size(250, 17);
            this.toolStripStatusLabelCalibration.Text = "Current Calibration Process/Step";
            this.toolStripStatusLabelCalibration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBarCurrentCalibration
            // 
            this.toolStripProgressBarCurrentCalibration.Name = "toolStripProgressBarCurrentCalibration";
            this.toolStripProgressBarCurrentCalibration.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBarCurrentCalibration.Visible = false;
            // 
            // frmCalibrationMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 566);
            this.Controls.Add(this.groupBoxCalibrationStatus);
            this.Controls.Add(this.groupBoxThunderStatus);
            this.Controls.Add(this.groupBoxReferance);
            this.Controls.Add(this.groupBoxChamber);
            this.Controls.Add(this.groupBoxStatusRadiosondes);
            this.Controls.Add(this.groupBoxCalibrationControls);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmCalibrationMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "600,087 E+E Calibration 1.0.1.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCalibrationMain_FormClosing);
            this.Load += new System.EventHandler(this.frmCalibrationMain_Load);
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxCalibrationControls.ResumeLayout(false);
            this.groupBoxCalibrationControls.PerformLayout();
            this.groupBoxStatusRadiosondes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRadiosondes)).EndInit();
            this.groupBoxChamber.ResumeLayout(false);
            this.groupBoxChamber.PerformLayout();
            this.groupBoxReferance.ResumeLayout(false);
            this.groupBoxReferance.PerformLayout();
            this.groupBoxThunderStatus.ResumeLayout(false);
            this.groupBoxThunderStatus.PerformLayout();
            this.groupBoxCalibrationStatus.ResumeLayout(false);
            this.groupBoxCalibrationStatus.PerformLayout();
            this.statusStripCalibration.ResumeLayout(false);
            this.statusStripCalibration.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMain;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxCalibrationControls;
        private System.Windows.Forms.GroupBox groupBoxStatusRadiosondes;
        private System.Windows.Forms.GroupBox groupBoxChamber;
        private System.Windows.Forms.GroupBox groupBoxReferance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelChamberAirTemp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelChamberHumidityAvg;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelChamberHumidity;
        private System.Windows.Forms.Label labelChamberAirTempAvg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelReferanceAirTemp;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelReferanceHumidityAvg;
        private System.Windows.Forms.Label labelReferanceAirTempAvg;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelReferanceHumidity;
        private System.Windows.Forms.Button buttonStartCalibration;
        private System.Windows.Forms.GroupBox groupBoxThunderStatus;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelCurrentFrostPoint;
        private System.Windows.Forms.Label labelSetTestPSI;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label labelCurrentTestPSI;
        private System.Windows.Forms.Label labelSetSaturC;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label labelCurrentSaturC;
        private System.Windows.Forms.Label labelSetSaturPSI;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label labelCurrentSaturPSI;
        private System.Windows.Forms.Label labelSetRH;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label labelCurrentRH;
        private System.Windows.Forms.Label labelSetPPMw;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelCurrentPPMw;
        private System.Windows.Forms.Label labelSetPPMv;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label labelCurrentPPMv;
        private System.Windows.Forms.Label labelSetDewPoint;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelCurrentDewPoint;
        private System.Windows.Forms.Label labelSetFrostPoint;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label labelSetFlow;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label labelCurrentFlow;
        private System.Windows.Forms.Label labelSetTestC;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label labelCurrentTestC;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelCurrentStatus;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelChamberHumidityRamp;
        private System.Windows.Forms.Label labelChamberAirTempRamp;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridView dataGridViewRadiosondes;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thunderControlsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chamberControlsToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxCalibrationStatus;
        private System.Windows.Forms.StatusStrip statusStripCalibration;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelCalibration;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarCurrentCalibration;
        private System.Windows.Forms.ToolStripMenuItem relayControlsToolStripMenuItem;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelChamberHumiditySetPoint;
        private System.Windows.Forms.Label labelChamberAirTempSetPoint;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labelCalibrationChamberStatus;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label labelCalibrationThunderStatus;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label labelCalibrationEnclouserStatus;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labelCalibrationCurrentUSetPoint;
        private System.Windows.Forms.Label labelCalibrationCurrentATSetPoint;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label labelCalibrationCurrentUDiff;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label labelCalibrationCurrentU;
        private System.Windows.Forms.Label labelCalibrationCurrentATDiff;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label labelCalibrationCurrentAT;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label labelCalibraitonRunTime;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ToolStripMenuItem changeUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manuelResetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thunderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startLoggingProformanceToolStripMenuItem;
        private System.Windows.Forms.Label labelCalibrationRadiosondeStatus;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem userSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem processToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calulateCoefficentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkRadiosondesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLOC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTrackingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPacketCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAirTempDiff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnHumidityCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFirmware;
        private System.Windows.Forms.ToolStripMenuItem calcCorrectChamberTempToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem envStableToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxRadiosondeSelect;
        private System.Windows.Forms.ComboBox comboBoxRadiosondeType;
        private System.Windows.Forms.ToolStripMenuItem correctReferanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hMP234ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxShutdownAfter;
        private System.Windows.Forms.Label labelChamerRelay;
        private System.Windows.Forms.CheckBox checkBoxPostCorrect;
    }
}

