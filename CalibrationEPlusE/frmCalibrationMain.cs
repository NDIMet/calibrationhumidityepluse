﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Bill;
using TestEquipment;
using Radiosonde;
using Alerts;
using RelayController;
using E_E_Calculation_Project;
using sensorCorrection;
using EnviromentStability;
using Universal_Radiosonde;
using thunderComm2;

namespace CalibrationEPlusE
{
    #region Delegates for invoking back into the main thread

    //Data to be displayed from sensor thread to main thread
    delegate void updateRefSensor(double currentTemp, double currentHumidity);

    //Data to update the main tool strip text.
    delegate void updateMainToolStrip(string message);

    //Data to update the humidity display
    delegate void updateThunder();

    //Data to update the chamber display
    delegate void updateChamber();

    //Error Message Boxes
    delegate void reportErrors(string message, string title);

    //Delegate used to setup data grid.
    delegate void updateDataGrid(int counterNumberOfRadiosondes);

    //Delegate used to update data from radiosondes to screen.
    delegate void sendDataToScreen();

    //Delegate used for enabling threads that need bool checks.
    delegate bool restartThread();

    //Delegate used for updating Referance and Chamber TU differance to screen.
    delegate void updateTUDiff(bool goodData);

    //Delegate for updateing the calibration status section
    delegate void updateCalibrationStatusDel(double currentATSetPoint, double currentUSetPoint, double currentAT, double currentU,
            double currentATDiff, double currentUDiff,
            bool currentChamberReady, double currentChamberReadyCount,
            bool currentEnclosureReady, double currentEnclosureReadyCount,
            bool currrentThunderReady, double currentThunderReadyCount);

    delegate void testText(object[] message);

    #endregion


    public partial class frmCalibrationMain : Form
    {
        public frmCalibrationMain()
        {
            InitializeComponent();
        }

        #region Public objects.
        //Radiosonde objects
        public iMet1[] ValidSondeArryiMet1;
        public iMet1U[] ValidSondeArrayiMet1U;

        //Referance Sensors
        public HMP234 referanceHMP234 = null;
        public epluseEE31 referanceEE31 = null;

        //Calibration Chamber
        public Thermotron chamberThermotron = null;
        public Watlow chamberWatlow = null;

        //Humidity Gen.
        //public TS3900 referanceThunder = null;
        public equipmentThunder genThunder = null;

        public IPRelay.webRealy tennyChamberRelay = null;   //Tenny Chamber Relay Conroller.

        //Alert object
        public alertsTextMessage textMessage = new alertsTextMessage();
        public alertsAudio audioAlertMessage;

        //Relay controller
        public relayControllerSystem relayController = new relayControllerSystem();

        //Object for calculating the final coefficents.
        private EEHumidityCalculation calculatorHumidity = new EEHumidityCalculation();

        //Setting up eviromental stability checking object
        private stabilityEnviroment calibrationEnviroment = new stabilityEnviroment();

        #endregion

        #region Background Workers for collecting data form radiosondes, sensors, and test equipment.
        
        //Backgound worker for collectin all radiosonde data.
        public BackgroundWorker backgroundWorkerCollectRadiosondeData;

        //Background worker for the calibration process.
        public BackgroundWorker backgroundWorkerCalibrationProcess;

        //Background worker for enviroment statblility checking
        public BackgroundWorker backgroundWorkerEnviromentStability;

        //Collect data form the referance sensor selected for calibration.
        public BackgroundWorker backgroundWorkerReferanceSensor;

        //Collects current conditions as reported by selected calibration chamber
        public BackgroundWorker backgroundWorkerCalibrationChamber;

        //Collects curret conditons from the humidity generator.
        public BackgroundWorker backgroundWorkerHumidityGenerator;

        //Background worker for calculating the differance/min for Current Referance Sensor.
        public BackgroundWorker backgroundWorkerReferanceDifferance;

        //Background worker for calculating the differance/min for Current Chamber.
        public BackgroundWorker backgroundWorkerChamberReferanceDifferance;

        //Background worker for collection data from the elements to create a profomance log.
        public BackgroundWorker backgroundWorkerProformanceLog;

        //Background worker for displaying and handling Thunder Errors.
        public BackgroundWorker backgroundWorkerThunderError;

        #endregion

        public static class ProgramSettings
        {
            //Referance Sensors
            public static string calibrationReferanceSensor = null;
            public static string EPlusEReferanceComPort = null;
            public static string HMP234ReferanceComPort = null;

            //Raidosonde Com ports
            public static string RadiosondeStartingComPort = null;
            public static int NumberofRadiosondes = 0;

            //Chamber Settings
            public static string ChamberController = null;
            public static string ChamberControllerComPort = null;

            //Humidity Settings
            public static string humiditySource = null;
            //Thunder 3900 Low Humidity Generator
            public static string ThunderComPort = null;

            //Chamber relay controller
            public static string tennyChamberControllerIP = null;
            public static int tennyChamberControllerPort = 0;

            //Calc server
            public static string CalcServerAddress = null;

            //Raw File Folder Storage Location
            public static string RAWFileLOC = null;

            //Coef cal storage loacation
            public static string CoefLOC = null;

            //Alert settings
            public static string AudioFile = null;
            public static bool alertAudio = false;
            public static string TextMessageNumber = null;
            public static bool alertTextMessage = false;
            public static string EmailAddress = null;
            public static bool alertEmail = false;
            public static string OutgoingAddress = "";
            public static string OutgoingServer = "";
            public static string OutgoingPort = "";
            public static string OutgoingUsername = "";
            public static string OutgoingPassword = "";
            public static bool OutgoingSSL = false;

            //Colors for pass, fails, radiosonde types, and other things like that.
            public static Color colorPass = System.Drawing.Color.Green;
            public static Color colorFail = System.Drawing.Color.Red;
            public static Color colorReady = System.Drawing.Color.Red;
            public static Color colorInProcess = System.Drawing.Color.Red;
            public static Color colorRepeatElement = System.Drawing.Color.Red;

            //Calibration SetPoints
            public static string[,] calibrationSetPoint;

            //Radiosonde Acceptable Tolerances
            public static double tolAcceptableAirTempDiff = 0;
            public static double tolHumidityFreqMean = 0;
            public static double tolHumidityFrewPlusMinus = 0;

            //Valve control settings
            public static string modeValve = "";

            //Relay controller settings
            public static string relayComPort = "";
            public static string[,] relays = new string[16, 3];

            //Chamber Set Point Correction Coeffcients
            public static double[] coeffcientsChamber = new double[3];

            //Setting to control if the referance is corrected to the GE chilled mirror
            public static bool referanceCorrectionMode = false;

            //Radiosonde Type
            public static string radiosondeType = "";

        }

        public static class ProgramStatus
        {
            public static bool statusRadiosondeLogging = false;

            public static string currentEventLog = "";
        }

        public static class CalibrationStatus
        {
            //Contaner to check to see if a calibration is in process.
            public static bool statusCalibrationInProcess = false;

            //Mode that the current program is working in.
            /* Modes include:
             * idle = no calibration in effect. System is connected to sensors but no command to start has been issed.
             * error = error normal error could be anything is wrong.
             * sensorError = sensor error is set if the sensors are unable to be contacted.
             * ruuning = calibration is running.
             * postProcess = Generating coefficents and loading radio
             * abort =  the current calibration has been aborted.
            */ 
            public static string currentProgramMode = "idle";

            //Time when the calibration started.
            public static DateTime calibrationStartTime;

            //Current calibration point.
            public static int currentCalibrationPoint = 0;

            //Status of mixing fan(s).
            public static bool statusFans = false;

            //Status of Radiosonde Elements
            public static bool statusRadiosondePower = false;
            public static bool statusRadiosondeReady = false;

            //Status of chamber
            public static bool statusChamberStable = false;
            public static bool statusChamberAtSetPoint = false;

            //Status of enclosure
            public static bool statusEnclosureStable = false;
            public static bool statusEnclosureAtSetPoint = false;

            //Status of Thunder 3900
            public static bool statusThunderStable = false;
            public static bool statusThunderAtSetPoint = false;

            //Main go no-go for the calibration process. This is the only item the main will look at but to get this set true all the rest must be set true.
            //Recommend that it take many statble packets and loop before this is allowed to be set true.
            public static bool okToStartCollectingData = false;

            //Current overall logging file.
            public static string currentOverallLog = "";

            //Current Referance Differance.
            //Chamber Differance Data
            public static double currentRawDiffChamberTemp = 99.99;
            public static double currentRawDiffChamberHumidity = 99.99;

            public static double currentDiffChamberTemp = 99.99;
            public static double currentDiffChamberHumidity = 99.99;

            //Referance Differance Data
            public static double currentRawDiffReferanceTemp = 99.99;
            public static double currentRawDiffReferanceHumidity = 99.99;

            public static double currentDiffReferanceTemp = 99.99;
            public static double currentDiffReferanceHumidity = 99.99;

            //Time the calibration conditions must remain stable.
            //public static int calibrationStabilityWaitTime = 1;

            //Bool for preping the thunder 3900.
            public static bool prepThunder = false;

            //Bool for clearning status from the previous cal point.
            public static bool clearCalibrationStatus = false;
        }

        public static class CommunicationControl
        {
            //Controls the flow for communications with the chamber.
            public static bool commControlChamber = true;

            //Controls the flow of communications with the thunder.
            public static bool commControlHunmidityGen = true;

            //Controls the flow of communicatinos with the radiosondes.
            public static bool commControlRadiosonde = false;

        }

        public static class CurrentUserSettings
        {
            //Current User using the program.
            public static string CurrentUser = "";
            
            //Audio alert settings
            public static bool alertAudio = false;
            public static string alertAudioFile = null;

            //Txt Message alert settings
            public static bool alertTxtMessage = false;
            public static string alertTxtMessageNumber = null;
            public static string alertTxtMessageCarrier = null;

            //Email Message alert settings
            public static bool alertEmail = false;
            public static string alertEmailAddress = null;
        }

        #region AutoResetEvent objects
        
        //Auto Reset Events for telling if new referance data is in.
        public System.Threading.AutoResetEvent refChamberDataDone = new System.Threading.AutoResetEvent(false); //Current Running Chamber
        public System.Threading.AutoResetEvent refTUDataDone = new System.Threading.AutoResetEvent(false); //Current Running Referance Sensor

        //Events need for logging calibration data
        public System.Threading.AutoResetEvent dataFromRadiosondesReady = new System.Threading.AutoResetEvent(false);
        public System.Threading.AutoResetEvent dataFromReferanceReady = new System.Threading.AutoResetEvent(false);

        //Event indicating when then hunmidity generator status thread has shutdown.
        public System.Threading.AutoResetEvent humiditySystemShutdown = new System.Threading.AutoResetEvent(false);
        public System.Threading.AutoResetEvent humidttyGenComReady = new System.Threading.AutoResetEvent(false);

        //Purge humidity thread blocking.
        public System.Threading.AutoResetEvent purgeComplete = new System.Threading.AutoResetEvent(false);

        #endregion

        #region Loading and closing of the main form.
        private void frmCalibrationMain_Load(object sender, EventArgs e)
        {
            //Current User doing the work form.
            string value = "";
            while (value == "")
            {
                if (Bill.Interfaces.InputBox("User Name", "Your Name:", ref value) == DialogResult.OK)
                {
                    CurrentUserSettings.CurrentUser = value;
                    updateMainStatusLabel("Loaded... " + "Current User: " + value);

                    if (!loadUserSettingsFromFile(value))
                    {
                        MessageBox.Show("User Not Found. Please check settings and try again.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    value = "Close";
                    Application.Exit();
                }

            }

            //Loading settings in from the settings file.
            loadSettingsFromFile();

            //Starting Referance sensor
            startReferanceSensor();

            //Starting humidity gen
            startHumidityGen();

            //Starting Chamber
            startChamber();

            //Starting Referance differances
            startReferanceDifferance();

            //Starting Chamber Referance differances
            startChamberDifferance();

            //Start Relay Controller
            startRelayController();
        }

        private void frmCalibrationMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CurrentUserSettings.CurrentUser != "")
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2);

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }

            //Closeing Valves
            switch (ProgramSettings.modeValve)
            {
                case "manual":
                    MessageBox.Show("Set valves in the following order:\n\nWARNING THESE VALVES MUST BE SET IN THIS ORDER\n\n" +
                         "Valve C: True" + "\n" +
                          "Valve B: False" + "\n" +
                          "Valve A: False",
                           "Set Valves Alert", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case "auto":
                    //Closing both valve
                    resetValves();
                    break;
            }

            //Shutting down the chamber to help provent program crashes that lock the com port open.
            stopChamber();

            //Stopping calibration if running
            if (backgroundWorkerCalibrationProcess != null)
            {
                if (backgroundWorkerCalibrationProcess.IsBusy)
                {
                    stopCurrentCalibration();
                }
            }

            System.Threading.Thread.Sleep(300);
        }
        #endregion

        #region Methods for loading setting from a file.

        public bool loadUserSettingsFromFile(string userName)
        {
            //If the userName is blank then exit the method and return false
            if (userName == "")
            {
                return false;
            }

            string configLine = "";
            //Loading user settings.
            TextReader ProgramSettingsReader = new StreamReader("userProfiles.cfg");
            // read lines of text


            configLine = ProgramSettingsReader.ReadToEnd();
            configLine = configLine.Replace("\r", "");
            string[] configElement = configLine.Split('\n', ',');

            // close the stream
            ProgramSettingsReader.Close();

            for (int i = 0; i < configElement.Length; i++)
            {
                if (configElement[i] == userName)
                {
                    CurrentUserSettings.alertAudio = Convert.ToBoolean(configElement[i + 1]);
                    CurrentUserSettings.alertAudioFile = configElement[i + 2];

                    //Txt Message alert settings
                    CurrentUserSettings.alertTxtMessage = Convert.ToBoolean(configElement[i + 3]);
                    CurrentUserSettings.alertTxtMessageNumber = configElement[i + 4];
                    CurrentUserSettings.alertTxtMessageCarrier = configElement[i + 5];

                    //Email Message alert settings
                    CurrentUserSettings.alertEmail = Convert.ToBoolean(configElement[i + 6]);
                    CurrentUserSettings.alertEmailAddress = configElement[i + 7];

                    return true;
                }
            }
            return false;
        }

        public void loadSettingsFromFile()
        {
            int calibrationSets = 0;
            string configLine = "";
            TextReader ProgramSettingsReader = new StreamReader("EPlusESettings.cfg");
            // read lines of text


            configLine = ProgramSettingsReader.ReadToEnd();
            configLine = configLine.Replace("\r", "");
            string[] configElement = configLine.Split('\n', '=');

            // close the stream
            ProgramSettingsReader.Close();

            //Setting up the global settings.
            for (int i = 0; i<configElement.Length; i++)
            {
                switch (configElement[i])
                {
                    case "calibrationReferanceSensor":
                        ProgramSettings.calibrationReferanceSensor = configElement[i + 1];
                        break;

                    case "EPlusEReferanceComPort":
                        ProgramSettings.EPlusEReferanceComPort = configElement[i + 1];
                        break;
                        
                    case "HMP234ReferanceComPort":
                        ProgramSettings.HMP234ReferanceComPort = configElement[i + 1];
                        break;
                        
                    case "RadiosondeStartingComPort":
                        ProgramSettings.RadiosondeStartingComPort = configElement[i + 1];
                        break;

                    case "NumberofRadiosondes":
                        ProgramSettings.NumberofRadiosondes = Convert.ToInt16(configElement[i + 1]);
                        break;

                    case "ChamberController":
                        ProgramSettings.ChamberController = configElement[i + 1];
                        break;

                    case "ChamberControllerComPort":
                        ProgramSettings.ChamberControllerComPort = configElement[i + 1];
                        break;

                    case "ChamberRelayIP":
                        ProgramSettings.tennyChamberControllerIP = configElement[i + 1];
                        break;

                    case "ChamberRelayPort":
                        ProgramSettings.tennyChamberControllerPort = Convert.ToInt16(configElement[i+1]);
                        break;

                    case "ThunderComPort":
                        ProgramSettings.ThunderComPort = configElement[i + 1];
                        break;
                    
                    case "CalcServerAddress":
                        ProgramSettings.CalcServerAddress = configElement[i + 1];
                        break;

                    case "RAWFileLOC":
                        ProgramSettings.RAWFileLOC = configElement[i + 1];
                        break;

                    case "CoefLOC":
                        ProgramSettings.CoefLOC = configElement[i + 1];
                        break;

                    case "AudioFile":
                        ProgramSettings.AudioFile = configElement[i + 1];
                        break;

                    case "AlertAudio":
                        ProgramSettings.alertAudio = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case "TextMessageNumber":
                        ProgramSettings.TextMessageNumber = configElement[i + 1];
                        break;

                    case "AlertTextMessage":
                        ProgramSettings.alertTextMessage = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case "EmailAddress":
                        ProgramSettings.EmailAddress = configElement[i + 1];
                        break;

                    case "AlertEmail":
                        ProgramSettings.alertEmail = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case "Pass":
                        ProgramSettings.colorPass = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "Fail":
                        ProgramSettings.colorFail = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "Ready":
                        ProgramSettings.colorReady = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "InProcess":
                        ProgramSettings.colorInProcess = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "RepeatElement":
                        ProgramSettings.colorRepeatElement = Color.FromArgb(Convert.ToInt32(configElement[i + 1]));
                        break;

                    case "CalibrationStep":
                        calibrationSets++;
                        break;

                    case "AcceptableAirTempDiff":
                        ProgramSettings.tolAcceptableAirTempDiff = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case "HumidityFreqMean":
                        ProgramSettings.tolHumidityFreqMean = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case "HumidityFreqPlusMinus":
                        ProgramSettings.tolHumidityFrewPlusMinus = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case"OutgoingAddress":
                        ProgramSettings.OutgoingAddress = configElement[i + 1];
                        break;

                    case"OutgoingServer":
                        ProgramSettings.OutgoingServer = configElement[i + 1];
                        break;

                    case"OutgoingPort":
                        ProgramSettings.OutgoingPort = configElement[i + 1];
                        break;

                    case"OutgoingUsername":
                        ProgramSettings.OutgoingUsername = configElement[i + 1];
                        break;

                    case"OutgoingPassword":
                        ProgramSettings.OutgoingPassword = configElement[i + 1];
                        break;

                    case"OutgoingSSL":
                        ProgramSettings.OutgoingSSL = Convert.ToBoolean(configElement[i + 1]);
                        break;

                    case"ValveMode":
                        ProgramSettings.modeValve = configElement[i + 1];
                        break;

                    case"relayComPort":
                        ProgramSettings.relayComPort = configElement[i + 1];
                        break;


                        //loading coeffcients for correcting the chamber to the needed temp to achive the desire interal temp.
                    case"coeffcientChamber[0]":
                        ProgramSettings.coeffcientsChamber[0] = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case"coeffcientChamber[1]":
                        ProgramSettings.coeffcientsChamber[1] = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case"coeffcientChamber[2]":
                        ProgramSettings.coeffcientsChamber[2] = Convert.ToDouble(configElement[i + 1]);
                        break;

                    case "HumiditySource":
                        ProgramSettings.humiditySource = configElement[i + 1];
                        break;
                }

                
                //Loading relay settings
                if (configElement[i] == "relay")
                {
                    string[] relayElement = configElement[i+1].Split(',');
                    for (int loadRelay = 0; loadRelay < relayElement.Length; loadRelay++)
                    {
                        ProgramSettings.relays[Convert.ToInt16(relayElement[0]), loadRelay] = relayElement[loadRelay];
                    }

                }
            }

            

            //Loading calibration steps.
            ProgramSettings.calibrationSetPoint = new string[calibrationSets,22];
            int iParent = 0;
            for (int x = 0; x<configElement.Length; x++)
            {
                if (configElement[x] == "CalibrationStep")
                {
                    string[] calibrationSetElement = configElement[x + 1].Split(',');
                    for (int j = 0; j < calibrationSetElement.Length; j++)
                    {
                        ProgramSettings.calibrationSetPoint[iParent,j] = calibrationSetElement[j];
                    }
                    iParent++;
                }
            }

        }

        #endregion

        #region Settings Tool Strip Menu Items
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CalibrationStatus.statusCalibrationInProcess)
            {
                DialogResult areYouSure = MessageBox.Show("Changing settings while the calibration is in progress can cause the system to become unstable. Are you sure you want to make a change?", "Calibration in Progress", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2);
                if (areYouSure == DialogResult.No)
                {
                    return;
                }
            }

            //Stopping all data collection form referances during change to settings.
            setCommunicationControls(false);

            //Stop any calibtation, null all referances, chambers, radiosondes, and other equipment.
            stopReferanceSensor();

            //Stopping the humidity gen.
            stopHumidityGen();

            //Stopping the chamber.
            stopChamber();


            //Opening and displaying the settings form.
            Form frmSettings = new frmCalibrationSettings();
            frmSettings.Show(this);
        }

        private void userSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmUserSettings = new frmCalibrationUsers();
            frmUserSettings.Show();
        }

        #endregion

        #region Methods for accessing, starting, collecting data from the referance sensors.

        public bool startReferanceSensor()
        {
            try
            {
                backgroundWorkerReferanceSensor = new BackgroundWorker();
                backgroundWorkerReferanceSensor.WorkerSupportsCancellation = true;
                backgroundWorkerReferanceSensor.DoWork += new DoWorkEventHandler(backgroundWorkerReferanceSensor_DoWork);
                backgroundWorkerReferanceSensor.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerReferanceSensor_RunWorkerCompleted);
                backgroundWorkerReferanceSensor.RunWorkerAsync();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void stopReferanceSensor()
        {
            //Stopping the background worker. It will be disposed as it ends.
            if (backgroundWorkerReferanceSensor.IsBusy) { backgroundWorkerReferanceSensor.CancelAsync(); }

            //Closing the EE referance and nulling the object.
            if (referanceEE31 != null)
            {
                if (referanceEE31.ComPort.IsOpen) { referanceEE31.ComPort.Dispose(); }
                referanceEE31 = null;
            }

            //Closing the HMP234 referance and nulling the object.
            if (referanceHMP234 != null)
            {
                if (referanceHMP234.ComPort.IsOpen) { referanceHMP234.ComPort.Dispose(); }
                referanceHMP234 = null;
            }
        }

        void backgroundWorkerReferanceSensor_DoWork(object sender, DoWorkEventArgs e)
        {
            if (ProgramSettings.calibrationReferanceSensor.Contains("HMP"))
            {
                referanceHMP234 = new HMP234(ProgramSettings.HMP234ReferanceComPort);

                try
                {
                    //Opening the com port for the sensor.
                    if (!referanceHMP234.ComPort.IsOpen) { referanceHMP234.ComPort.Open(); }
                }
                catch (Exception openError)
                {
                    MessageBox.Show(openError.Message + "\nCheck settings and restart referance.", "Referance Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { openError.Message });
                    return;
                }
                //Starting sensor.
                referanceHMP234.startHMP234Sensor();

                //A little break to allow the sensor to wake up.
                System.Threading.Thread.Sleep(500);

                while (!backgroundWorkerReferanceSensor.CancellationPending)
                {
                    double[] currentTU = null;

                    try
                    {
                        //Getting data.
                        referanceHMP234.getHMP234Data();

                        //Getting current corrected data.
                        currentTU = getReferanceData(ProgramSettings.referanceCorrectionMode);
                    }
                    catch (Exception sensorError)
                    {
                        BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Referance: " + sensorError.Message });
                    }

                    //Sending data out to be displayed.
                    try
                    {
                        BeginInvoke(new updateRefSensor(updateRefSensorDisplay), new object[] { currentTU[0], currentTU[1] });
                    }
                    catch { }
                    //A little break so that the system doesn't just keep running.
                    refTUDataDone.Set();
                    dataFromReferanceReady.Set();
                    System.Threading.Thread.Sleep(500);
                }
            }

            if (ProgramSettings.calibrationReferanceSensor.Contains("EE31"))
            {
                referanceEE31 = new epluseEE31(ProgramSettings.EPlusEReferanceComPort);

                try
                {
                    //Opening the com port for the sensor.
                    if (!referanceEE31.ComPort.IsOpen) { referanceEE31.ComPort.Open(); }
                }
                catch (Exception openError)
                {
                    BeginInvoke(new reportErrors(reportErrorMessageBox), new object[] { openError.Message + "\nCheck settings and restart referance.", "Referance Error" });
                    BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Referance: " + openError.Message });
                    return;
                }

                while (!backgroundWorkerReferanceSensor.CancellationPending)
                {

                    double[] currentTU = null;

                    try
                    {
                        //Getting the data from sensor
                        referanceEE31.getEPlusEData();

                        //Collecting the data into currentTU
                        currentTU = getReferanceData(ProgramSettings.referanceCorrectionMode);

                        //Sending data out to be displayed.
                        BeginInvoke(new updateRefSensor(updateRefSensorDisplay), new object[] { currentTU[0], currentTU[1] });

                    }
                    catch (Exception sensorError)
                    {
                        BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Reference: " + sensorError.Message });
                    }



                    //A little break so that the system doesn't just keep running.
                    refTUDataDone.Set();
                    dataFromReferanceReady.Set();
                    System.Threading.Thread.Sleep(1000);

                }
            }
            /*
            switch (ProgramSettings.calibrationReferanceSensor)
            {
                case "HMP234":
                    referanceHMP234 = new HMP234(ProgramSettings.HMP234ReferanceComPort);

                    try
                    {
                        //Opening the com port for the sensor.
                        if (!referanceHMP234.ComPort.IsOpen) { referanceHMP234.ComPort.Open(); }
                    }
                    catch (Exception openError)
                    {
                        MessageBox.Show(openError.Message + "\nCheck settings and restart referance.","Referance Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                        BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { openError.Message });
                        return;
                    }
                    //Starting sensor.
                    referanceHMP234.startHMP234Sensor();

                    //A little break to allow the sensor to wake up.
                    System.Threading.Thread.Sleep(500);

                    while (!backgroundWorkerReferanceSensor.CancellationPending)
                    {
                        double[] currentTU = null;

                        try
                        {
                            //Getting data.
                            referanceHMP234.getHMP234Data();

                            //Getting current corrected data.
                            currentTU = getReferanceData(ProgramSettings.referanceCorrectionMode);
                        }
                        catch (Exception sensorError)
                        {
                            BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Referance: " + sensorError.Message });
                        }

                        //Sending data out to be displayed.
                        try
                        {
                            BeginInvoke(new updateRefSensor(updateRefSensorDisplay), new object[] {  currentTU[0], currentTU[1] });
                        }
                        catch { }
                        //A little break so that the system doesn't just keep running.
                        refTUDataDone.Set();
                        dataFromReferanceReady.Set();
                        System.Threading.Thread.Sleep(500);
                    }

                    break;

                case "EPlusE":
                    referanceEE31 = new epluseEE31(ProgramSettings.EPlusEReferanceComPort);
                    
                    try
                    {
                        //Opening the com port for the sensor.
                        if (!referanceEE31.ComPort.IsOpen) { referanceEE31.ComPort.Open(); }
                    }
                    catch (Exception openError)
                    {
                        BeginInvoke(new reportErrors(reportErrorMessageBox), new object[] { openError.Message + "\nCheck settings and restart referance.", "Referance Error" });
                        BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Referance: " + openError.Message });
                        return;
                    }

                    while (!backgroundWorkerReferanceSensor.CancellationPending)
                    {

                        double[] currentTU = null;

                        try
                        {
                            //Getting the data from sensor
                            referanceEE31.getEPlusEData();

                            //Collecting the data into currentTU
                            currentTU = getReferanceData(ProgramSettings.referanceCorrectionMode);

                            //Sending data out to be displayed.
                            BeginInvoke(new updateRefSensor(updateRefSensorDisplay), new object[] { currentTU[0], currentTU[1] });

                        }
                        catch (Exception sensorError)
                        {
                            BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Reference: " + sensorError.Message });
                        }



                        //A little break so that the system doesn't just keep running.
                        refTUDataDone.Set();
                        dataFromReferanceReady.Set();
                        System.Threading.Thread.Sleep(1000);

                    }
                    break;
            }
            */
        }

        void backgroundWorkerReferanceSensor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            updateMainStatusLabel("Reference sensor disconnected.");
        }

        /// <summary>
        /// This methods returns the current air temp and humidity in a double array.
        /// </summary>
        /// <returns></returns>
        public double[] getReferanceData(bool corrected)
        {
            //Data that will be returned after request.
            double[] referanceData = new double[2];

            if (ProgramSettings.calibrationReferanceSensor.Contains("HMP"))
            {
                //Setting up correction for HMP234
                sensorCorHMP234Blk corHMP234Blk = new sensorCorHMP234Blk();

                //Collecting information for output.
                referanceData[0] = referanceHMP234.getCurrentAirTemp();
                if (corrected)
                {
                    referanceData[1] = corHMP234Blk.getCorrectedHumidity(referanceHMP234.getCurrentHumidity());
                }
                else
                {
                    referanceData[1] = referanceHMP234.getCurrentHumidity();
                }
            }


            if (ProgramSettings.calibrationReferanceSensor.Contains("EE31"))
            {
                string report = "";
                sensorCorEE31 corEPlusE = new sensorCorEE31();

                //Complieing corrected data
                referanceData[0] = referanceEE31.getCurrentAirTemp();
                if (corrected)
                {
                    referanceData[1] = corEPlusE.getCorrectedHumidity(referanceEE31.getCurrentHumidity());
                }
                else
                {
                    referanceData[1] = referanceEE31.getCurrentHumidity();
                }
            }

            /*
            switch (ProgramSettings.calibrationReferanceSensor)
            {
                case "HMP234":
                    //Setting up correction for HMP234
                    sensorCorHMP234Blk corHMP234Blk = new sensorCorHMP234Blk();

                    //Collecting information for output.
                    referanceData[0] = referanceHMP234.getCurrentAirTemp();
                    if (corrected)
                    {
                        referanceData[1] = corHMP234Blk.getCorrectedHumidity(referanceHMP234.getCurrentHumidity());
                    }
                    else
                    {
                        referanceData[1] = referanceHMP234.getCurrentHumidity();
                    }
                    break;

                case "EPlusE":
                    string report = "";
                    sensorCorEE31 corEPlusE = new sensorCorEE31();

                    //Complieing corrected data
                    referanceData[0] = referanceEE31.getCurrentAirTemp();
                    if (corrected)
                    {
                        referanceData[1] = corEPlusE.getCorrectedHumidity(referanceEE31.getCurrentHumidity());
                    }
                    else
                    {
                        referanceData[1] = referanceEE31.getCurrentHumidity();
                    }
                    break;
            }
             */ 

            return referanceData;

        }

        #endregion

        #region Methods of accessing, starting, and collecting data from the Thunder Humidity Gen.
 
        public bool startHumidityGen()
        {
            
            //Starting background worker.
            humidttyGenComReady.Set();
            backgroundWorkerHumidityGenerator = new BackgroundWorker();
            backgroundWorkerHumidityGenerator.WorkerSupportsCancellation = true;
            backgroundWorkerHumidityGenerator.DoWork += new DoWorkEventHandler(backgroundWorkerHumidityGenerator_DoWork);
            backgroundWorkerHumidityGenerator.RunWorkerAsync();

            return true;

        }

        void backgroundWorkerHumidityGenerator_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (ProgramSettings.humiditySource)
            {
                case "Thunder":
                    if (genThunder == null)
                    {
                        //Setting up the thunder for serial comm.
                        try
                        {
                            genThunder = new equipmentThunder(ProgramSettings.ThunderComPort);
                        }
                        catch (Exception errorThunder)
                        {
                            logProgramEvent(errorThunder.Message);
                            return;
                        }

                        //Connecting to events in thunder object
                        genThunder.internetalUpdateComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(internetalUpdateComplete_PropertyChange);
                        //genThunder.dataProcessingComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(dataProcessingComplete_PropertyChange);
                        //genThunder.errorAlert.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(errorAlert_PropertyChange);
                        genThunder.startInternalUpdate();
                    }
                    break;
                case "Thermotron":
                    //Configureing the chamber for humidity mode and power save mode.
                    System.Threading.Thread.Sleep(5000);
                    chamberThermotron.setOptions(70);
                    chamberThermotron.processOptinosReturn(chamberThermotron.getOptions());

                    break;
            }
        }

        void dataProcessingComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new testText(testText), new object[] { data.NewValue });
        }

        void errorAlert_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            object incomingError = data;
            logProgramEvent((string)data.NewValue);
     //       BeginInvoke(new testText(testText), new object[] { data.NewValue });
            BeginInvoke(new updateMainToolStrip(testTextString), new object[] { data.NewValue });
            
        }

        public void testText(object[] message)
        {
            //textBoxTest.AppendText(message[0] + "," + message[1] + "\n");
        }

        public void testTextString(string message)
        {
            //textBoxTest.AppendText(message + "\n");
        }


        /// <summary>
        /// This should que the data update for the thunder.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void internetalUpdateComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                BeginInvoke(new updateThunder(updateThunderDisplay));
            }
            catch (Exception thunderDisplayError)
            {
                logProgramEvent(thunderDisplayError.Message);
            }
        }

        public void stopHumidityGen()
        {
            //If the system is not setup.
            if (genThunder != null)
            {
                //Shutting down internal process.
                genThunder.stopInternalUpdate();

                //Closing port and releaseing internal resorces.
                genThunder.dispose();

                //Closing the Thunder referance and nulling the object.
                if (genThunder != null)
                {
                    genThunder.dispose();
                    genThunder = null;
                }
            }
        }


        /// <summary>
        /// Method can only be used by the a thread other then the main thread. If the main tread uses this method it will lock up th
        /// main program.
        /// </summary>
        /// <param name="desiredSaturationPressure"></param>
        /// <param name="desiredFlowRate"></param>
        /// <param name="desiredMode"></param>
        public void commandHumidityGenerator(double desiredSaturationPressure, double desiredSatuationTemp,  double desiredFlowRate, string desiredMode)
        {
            switch (ProgramSettings.humiditySource)
            {
                case "Thunder":

                    //resendSettings: //Just in case there is a error.

                    //referanceThunder.setSaturationTemp(desiredSatuationTemp, true);
                    genThunder.setSaturationTemp(desiredSatuationTemp);
                    //referanceThunder.setSaturationPressure(desiredSaturationPressure, true);
                    genThunder.setSaturationPressure(desiredSaturationPressure);
                    //referanceThunder.setFlowRate(desiredFlowRate, true);
                    genThunder.setFlowRate(desiredFlowRate);

                    //purg, start, or stop the system.
                    string currentMode = "";

                    //decodeing what mode the thunder is currently in.
                    switch (genThunder.statsData.currentStaus)
                    {
                        case 0:
                            currentMode = "STP";
                            break;

                        case 1:
                            currentMode = "GEN";
                            break;

                        case -1:
                            currentMode = "PRG";
                            break;
                    }

                    //If the desire mode is differnet then current then make the mode change.
                    //if (desiredMode != currentMode)
                    //{
                    switch (desiredMode)
                    {
                        case "PRG":
                            genThunder.systemPurge();
                            break;
                        case "GEN":
                            genThunder.systemGenerate();
                            break;
                        case "STP":
                            genThunder.systemStop();
                            break;
                    }
                    //}

                    /*
                    System.Threading.Thread.Sleep(250);

                    //Checking to make sure all the command where accepted and the setting are corrent.
                    //Gathering Data
                    object[] checkingSettings = referanceThunder.getSetPoints();
                    string[] checkingSettingsElements = checkingSettings[1].ToString().Split(',');

                    if (Convert.ToDouble(checkingSettingsElements[5]) != desiredSaturationPressure ||
                        Convert.ToDouble(checkingSettingsElements[6]) != desiredSatuationTemp ||
                        Convert.ToDouble(checkingSettingsElements[9]) != desiredFlowRate)
                    {
                        goto resendSettings;
                    }

                    System.Threading.Thread.Sleep(250);
                     * */
                    humidttyGenComReady.Set();
                    break;

                case "Thermotron":

                    if (chamberThermotron.CurrentChamberSPC > 6)
                    {
                        CommunicationControl.commControlChamber = false;
                        System.Threading.Thread.Sleep(1000);

                        //Setting the chamber to generate humidity
                        //chamberThermotron.setOptions(70);
                        //chamberThermotron.processOptinosReturn(chamberThermotron.getOptions());
                        //Setting the humidity to be generated.
                        //chamberThermotron.setChamberTemp(Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 4]));
                        chamberThermotron.setChamberHumidity(Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 8]));
                        chamberThermotron.startChamberManualMode();

                        System.Threading.Thread.Sleep(500);
                        CommunicationControl.commControlChamber = true;
                    }
                    
                    break;
            }
        }

        /// <summary>
        /// Method is for reporting thunder errors. This includes writing to the log, displaying a message on screen,
        /// and sending to the users definded alert settings.
        /// </summary>
        /// <param name="errorData"></param>
        public void errorHumidityGenerator(object[] errorData)
        {
            backgroundWorkerThunderError = new BackgroundWorker();
            backgroundWorkerThunderError.WorkerSupportsCancellation = true;
            backgroundWorkerThunderError.DoWork += new DoWorkEventHandler(backgroundWorkerThunderError_DoWork);
            backgroundWorkerThunderError.RunWorkerAsync(Convert.ToString(errorData[2]));

        }

        void backgroundWorkerThunderError_DoWork(object sender, DoWorkEventArgs e)
        {
            string errorData = Convert.ToString(e.Argument);

            //Writing the data to a log if calibration is in progress.
            if (CalibrationStatus.statusCalibrationInProcess)
            {
                logProgramEvent("Thunder Error: " + Convert.ToString(errorData));
            }

            //Displaying the error on screen.
            updateMainStatusLabel("Thunder Error: " + errorData);
            //Bill.informationDisplay information = new informationDisplay("Thunder Error", Convert.ToString(errorData));

            //Alert user exteral.
            alertUser("Thunder Error", "Thunder Error: " + Convert.ToString(errorData), 0);
        }

        #endregion

        #region Methods for accessing, starting, and collecting data from chambers.

        public void startChamber()
        {
            if (backgroundWorkerCalibrationChamber == null)
            {
                backgroundWorkerCalibrationChamber = new BackgroundWorker();
                backgroundWorkerCalibrationChamber.WorkerSupportsCancellation = true;
                backgroundWorkerCalibrationChamber.DoWork += new DoWorkEventHandler(backgroundWorkerCalibrationChamber_DoWork);
                backgroundWorkerCalibrationChamber.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerCalibrationChamber_RunWorkerCompleted);
            }
            if (!backgroundWorkerCalibrationChamber.IsBusy)
            {
                backgroundWorkerCalibrationChamber.RunWorkerAsync();
            }

        }

        public void stopChamber()
        {
            switch(ProgramSettings.ChamberController)
            {
                case "Watlow":
                    //Closeing the Watlow chamber com port.
                    if (chamberWatlow.chamber.sp.IsOpen) { chamberWatlow.chamber.Close(); }
                    break;

                case "Thermotron":

                    //Stopping background worker.
                    if (backgroundWorkerCalibrationChamber.IsBusy) { backgroundWorkerCalibrationChamber.CancelAsync(); }

                    if (ProgramSettings.ChamberControllerComPort.ToLower().Contains("com"))
                    {
                        //Closing the Thermotron object
                        if (chamberThermotron != null)
                        {
                            if (chamberThermotron.ComPort.IsOpen) { chamberThermotron.ComPort.Dispose(); }
                            chamberThermotron = null;
                        }
                    }
                    break;
            }
        }

        void backgroundWorkerCalibrationChamber_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            switch (ProgramSettings.ChamberController)
            {
                case "Watlow":
                    if (chamberWatlow == null) //Creating object if it is null
                    {
                        chamberWatlow = new Watlow(ProgramSettings.ChamberControllerComPort);
                    }
                    else //updating it if it does exsist.
                    {
                        chamberWatlow.chamber.sp.PortName = ProgramSettings.ChamberControllerComPort;
                    }

                    //Trying to open the chamber com port.
                    try
                    {
                        if (!chamberWatlow.chamber.sp.IsOpen) { chamberWatlow.chamber.sp.Open(); }
                    }
                    catch (Exception openError)
                    {
                        BeginInvoke(new reportErrors(reportErrorMessageBox), new object[] { openError.Message + "\nCheck settings and restart referance.", "Chamber Controller Error" });
                        BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { openError.Message });
                        return;
                    }


                    //Setting up relay controller for tenny.
                    if (ProgramSettings.tennyChamberControllerIP != null)
                    {
                        tennyChamberRelay = new IPRelay.webRealy(ProgramSettings.tennyChamberControllerIP);
                        System.Threading.Thread collectRelayState = new System.Threading.Thread(this.collectTennyRelayData);
                        collectRelayState.Name = "collectRelayState";
                        collectRelayState.Start();
                    }
                    
                    while (backgroundWorkerCalibrationChamber.CancellationPending == false)
                    {
                        if (chamberWatlow.chamber.sp.IsOpen)
                        {
                            if (CommunicationControl.commControlChamber)
                            {
                                chamberWatlow.getAirTemp();
                                if (!chamberWatlow.chamber.sp.IsOpen) { break; }
                                System.Threading.Thread.Sleep(15);
                                if (!chamberWatlow.chamber.sp.IsOpen) { break; }
                                chamberWatlow.getAirTempSetPoint();
                                if (!chamberWatlow.chamber.sp.IsOpen) { break; }
                                System.Threading.Thread.Sleep(15);
                                if (!chamberWatlow.chamber.sp.IsOpen) { break; }
                                chamberWatlow.getAirTempRamp();

                                //Sending data out to be displayed.
                                try
                                {
                                    Invoke(new updateChamber(updateChamberDisplay));
                                }
                                catch {}

                                //A little break so that the system doesn't just keep running.
                                System.Threading.Thread.Sleep(1000);
                            }
                            else
                            {
                                //Sleeping to stop the thread from looping out of contol
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        else
                        {
                            break;
                        }
                        //Triggering the differance to be processed.
                        refChamberDataDone.Set();
                    }

                    break;

                case "Thermotron":

                    if (ProgramSettings.ChamberControllerComPort.ToLower().Contains("com"))
                    {
                        chamberThermotron = new Thermotron(ProgramSettings.ChamberControllerComPort);
                        //trying to open up the com port
                        try
                        {
                            if (!chamberThermotron.ComPort.IsOpen) { chamberThermotron.ComPort.Open(); }
                        }
                        catch (Exception openError)
                        {
                            BeginInvoke(new reportErrors(reportErrorMessageBox), new object[] { openError.Message + "\nCheck settings and restart referance.", "Chamber Controller Error" });
                            BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { openError.Message });
                            return;
                        }
                    }
                    else
                    {
                        string[] connectData = ProgramSettings.ChamberControllerComPort.Split(',');
                        chamberThermotron = new Thermotron(connectData[0], Convert.ToInt16(connectData[1]));
                    }

                    chamberThermotron.setProductTempMode(true);


                    while (!backgroundWorkerCalibrationChamber.CancellationPending)
                    {
                        
                        chamberThermotron.getChamberStatus();

                        //Sending data out to be displayed.
                        BeginInvoke(new updateChamber(updateChamberDisplay));

                        //Triggering the differance to be processed.
                        refChamberDataDone.Set();

                        //A little break so that the system doesn't just keep running.
                        System.Threading.Thread.Sleep(1000);
                    }
                    break;
            }
        }

        void backgroundWorkerCalibrationChamber_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Chamber: Chamber disconnected." });
            }
            catch { }
        }

        private void collectTennyRelayData()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;

            while (true)
            {
                tennyChamberRelay.getRelayState();
                try
                {
                    BeginInvoke(new updateChamber(updateRelayState));
                }
                catch { }

                System.Threading.Thread.Sleep(1000);
            }
        }

        private void updateRelayState()
        {
            if (tennyChamberRelay != null)
            {
                if (tennyChamberRelay.systemState.relayState)
                {
                    labelChamerRelay.BackColor = Color.LightGreen;
                    labelChamerRelay.Text = "Running";
                }
                else
                {
                    labelChamerRelay.BackColor = Color.Red;
                    labelChamerRelay.Text = "Stopped";
                }
            }
        }

        public double[] getChamberData()
        {
            double[] currentChamberData = new double[2];

            switch (ProgramSettings.ChamberController)
            {
                case "Watlow":
                    currentChamberData[0] = (Convert.ToDouble(chamberWatlow.currentRawAirTemp) / 10);
                    currentChamberData[1] = 0;
                    break;

                case "Thermotron":
                    currentChamberData[0] = chamberThermotron.CurrentChamberPC;
                    currentChamberData[1] = chamberThermotron.CurrentChamberRH;
                    break;
            }

            return currentChamberData;
        }

        public double getChamberATSetPoint()
        {
            double currentATSetPoint = 99999;
            switch (ProgramSettings.ChamberController)
            {
                case "Watlow":
                    currentATSetPoint = (Convert.ToDouble(chamberWatlow.currentRawSPAirTemp)/10);
                    break;

                case "Thermotron":
                    currentATSetPoint = chamberThermotron.CurrentChamberSPPC;
                    break;
            }
            return currentATSetPoint;
        }

        public void setChamberATSetPoint(double desiredATSetPoint)
        {
            
            switch (ProgramSettings.ChamberController)
            {
                case "Watlow":
                    CommunicationControl.commControlChamber = false;
                    System.Threading.Thread.Sleep(1000);

                    chamberWatlow.setAirTemp(desiredATSetPoint);
                    chamberWatlow.chamber.sp.DiscardInBuffer();

                    System.Threading.Thread.Sleep(500);
                    CommunicationControl.commControlChamber = true;
                    break;

                case "Thermotron":
                    /*
                    if (desiredATSetPoint < 7)
                    {
                        chamberThermotron.setOptions(64);
                    }
                     */ 
                    chamberThermotron.setProductTemp(desiredATSetPoint);
                    chamberThermotron.startChamberManualMode();
                    break;
            }

        }


        #endregion

        #region Methods for accession, starting, and collecting data from radiosondes.

        public bool startRadiosondeCollection()
        {
            //Setting up the radiosonde background worker to collect data.
            try
            {
                backgroundWorkerCollectRadiosondeData = new BackgroundWorker();
                backgroundWorkerCollectRadiosondeData.WorkerSupportsCancellation = true;
                backgroundWorkerCollectRadiosondeData.DoWork += new DoWorkEventHandler(backgroundWorkerCollectRadiosondeData_DoWork);
                backgroundWorkerCollectRadiosondeData.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerCollectRadiosondeData_RunWorkerCompleted);
                backgroundWorkerCollectRadiosondeData.RunWorkerAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void stopRadiosondeCollection()
        {
            //Stopping background worker.
            if (backgroundWorkerCollectRadiosondeData.IsBusy) 
            {
                backgroundWorkerCollectRadiosondeData.CancelAsync();
            }

            //System.Threading.Thread.Sleep(2000);
            //ValidSondeArryiMet1 = null;
        }

        void backgroundWorkerCollectRadiosondeData_DoWork(object sender, DoWorkEventArgs e)
        {
            //time of last packet being logged.
            DateTime timeDataLastLogged = DateTime.Now;

            System.Threading.Thread.CurrentThread.IsBackground = true;

            switch (ProgramSettings.radiosondeType)
            {
                case "iMet-1":
                    #region Processing for the iMet-1 Radiosonde.
                    //Searching for valid radiosondes.
                    string[] possibleSondeNames = new string[ProgramSettings.NumberofRadiosondes];

                    for (int name = 0; name < possibleSondeNames.Length; name++)
                    {
                        possibleSondeNames[name] = "COM" + (Convert.ToInt16(ProgramSettings.RadiosondeStartingComPort.Substring(3, ProgramSettings.RadiosondeStartingComPort.Length - 3)) + name).ToString();
                    }

                    iMet1[] PossibleSondeArry = new iMet1[possibleSondeNames.Length];

                    for (int i = 0; i < possibleSondeNames.Length; i++)
                    {
                        PossibleSondeArry[i] = new iMet1(possibleSondeNames[i]);  //setting com ports to radiosondes.   
                        PossibleSondeArry[i].Initialize(); //opens com port and send data on command. Also get first data packet.

                        try
                        {
                            this.Invoke(new updateMainToolStrip(this.updateMainStatusLabel), new object[] { ("Searching " + possibleSondeNames[i]) });
                        }
                        catch
                        {
                            //writeToErrorLog("Unable to report searching for radiosonde on " + possibleSondeNames[i]);
                        }
                    }

                    try
                    {
                        this.Invoke(new updateMainToolStrip(this.updateMainStatusLabel), new object[] { ("Clearing buffers") });
                    }
                    catch
                    {
                        //writeToErrorLog("Unable to report Clearing buffers to screen.");
                    }

                    foreach (iMet1 sonde in PossibleSondeArry)
                    {
                        if (sonde.ComPort.IsOpen == true)
                        {
                            sonde.ComPort.ReadExisting();
                        }
                    }

                    System.Threading.Thread.Sleep(4000); //Giving the hardware time to catch up to the software.

                    int numberOfSondes = 0;
                    System.Collections.ArrayList badSondes = new System.Collections.ArrayList();
                    for (int i = 0; i < PossibleSondeArry.Length; i++)
                    {
                        bool isSonde = PossibleSondeArry[i].detectSonde();
                        bool isSondeGood = false;
                        if (isSonde)
                        {
                            isSondeGood = checkRadiosondeData(PossibleSondeArry[i]); //Ching to make sure sonde is good for calibration.
                            //Building a listArray
                            if (!isSondeGood)
                            {
                                string reportingSondes = PossibleSondeArry[i].getTrackingID();
                                badSondes.Add(PossibleSondeArry[i]);
                            }
                        }
                        if (isSonde)
                        {
                            numberOfSondes++;

                            try
                            {
                                this.Invoke(new updateMainToolStrip(this.updateMainStatusLabel), new object[] { ("Radiosonde found on: " + possibleSondeNames[i]) });
                            }
                            catch
                            {
                                //writeToErrorLog("Unable to report Radiosonde found on " + possibleSondeNames[i]);
                            }
                        }
                        else
                        {
                            PossibleSondeArry[i] = null; //Setting object to null,,, to stop looking at it.
                            try
                            {
                                this.Invoke(new updateMainToolStrip(this.updateMainStatusLabel), new object[] { ("Radiosonde found on: " + possibleSondeNames[i] + " but bad data.") });
                            }
                            catch
                            {
                                //writeToErrorLog("Unable to report Radiosonde found on " + possibleSondeNames[i]);
                            }
                        }


                    }

                    string totalReport = "";
                    foreach (iMet1 sonde in badSondes)
                    {
                        totalReport += sonde.getTrackingID() + "\n";
                    }

                    /*

                    DialogResult radiosondeContinue = MessageBox.Show(totalReport + "Do you want to continue?", "Sonde Report", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    if (radiosondeContinue == DialogResult.No)
                    {
                        return;
                    }
                    */

                    ValidSondeArryiMet1 = new iMet1[numberOfSondes]; //Moving all the valid sonde to the global sonde arry.

                    int j = 0;

                    for (int i = 0; i < PossibleSondeArry.Length; i++)
                    {
                        if (PossibleSondeArry[i] != null)
                        {
                            ValidSondeArryiMet1[j] = PossibleSondeArry[i];
                            j++;
                        }
                    }
                    //Setup the data grids for each racks.
                    try
                    {
                        this.Invoke(new updateDataGrid(this.updateDataGrid), new object[] { numberOfSondes });
                    }
                    catch
                    {
                        logProgramEvent("Unable to update one of the rack data grids.");
                    }

                    this.Invoke(new updateMainToolStrip(this.updateMainStatusLabel), new object[] { ValidSondeArryiMet1.Length.ToString() + " Radiosonde(s) found." });
                    logProgramEvent(ValidSondeArryiMet1.Length.ToString() + " Radiosonde(s) found.");

                    //In cas no sondes are found.
                    if (numberOfSondes == 0)
                    {
                        DialogResult search0results = MessageBox.Show("No Radiosonde detected. Is the power on?", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        CommunicationControl.commControlRadiosonde = false;
                        return;
                    }
                    else
                    {
                        CommunicationControl.commControlRadiosonde = true;
                    }

                    //Clear out any and all data from the radiosonde serial ports. This is needed do to the large timeout now allowed.
                    for (int ab = 0; ab < ValidSondeArryiMet1.Length; ab++)
                    {
                        ValidSondeArryiMet1[ab].ComPort.ReadExisting();
                    }

                    //Radiosonde discovery complete.
                    CalibrationStatus.statusRadiosondeReady = true;

                    //Loop for updating and collect multi-sonde data from valid radiosondes.
                    while (!backgroundWorkerCollectRadiosondeData.CancellationPending)
                    {
                        //Allows data collection to be paused. This allows the command to be sent.
                        if (CommunicationControl.commControlRadiosonde == true)
                        {

                            for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                            {
                                try
                                {
                                    ValidSondeArryiMet1[i].getLastHexData();
                                }
                                catch
                                {
                                    logProgramEvent("Unable to getLastHexData form " + ValidSondeArryiMet1[i].ComPort.PortName);
                                }

                            }
                            //Getting the time that data was collected.
                        }

                        System.Threading.Thread.Sleep(100);
                        
                        //Update data to data grids.
                        //Also maybe some checking on the status of the radiosonde should go here.

                        try
                        {
                            this.Invoke(new sendDataToScreen(this.updateDataGridData));
                        }
                        catch
                        {
                            if (!backgroundWorkerCollectRadiosondeData.CancellationPending)
                            {
                                logProgramEvent("Unable to start Radiosonde update to screen");
                            }
                        }


                        TimeSpan timeSpanSinceLastLogging = DateTime.Now - timeDataLastLogged;

                        //Logging radiosonde data like the multi-sonde
                        if (timeSpanSinceLastLogging.TotalSeconds >= 10)
                        {
                            logValidRadiosondeData();
                            timeDataLastLogged = DateTime.Now;

                        }

                        //Letting calibration know that the radiosonde data is ready.
                        dataFromRadiosondesReady.Set();

                        //Taking a little pause so that this thread just doesn't run off.
                        try
                        {
                            if (ValidSondeArryiMet1.Length > 8)
                            {
                                System.Threading.Thread.Sleep(500);
                            }
                            else
                            {
                                System.Threading.Thread.Sleep(800);
                            }
                        }
                        catch { }
                    }

                    #endregion

                    //End of iMet-1 case
                    break;

                case "Bell202":
                    #region Processing for the Bell202 Radiosondes.

                    //Searching for valid radiosondes.
                    string[] possibleiMet1UPort = new string[ProgramSettings.NumberofRadiosondes];
                    iMet1U[] possibleiMet1U = new iMet1U[ProgramSettings.NumberofRadiosondes];

                    //Number of valid radiosonde that responded to the check command.
                    int numberOfValidRadiosondes = 0;

                    for (int name = 0; name < possibleiMet1UPort.Length; name++)
                    {
                        possibleiMet1UPort[name] = "COM" + (Convert.ToInt16(ProgramSettings.RadiosondeStartingComPort.Substring(3, ProgramSettings.RadiosondeStartingComPort.Length - 3)) + name).ToString();
                    }

                    for (int i = 0; i < possibleiMet1U.Length; i++)
                    {
                        possibleiMet1U[i] = new iMet1U(possibleiMet1UPort[i]);

                        try
                        {
                            this.Invoke(new updateMainToolStrip(this.updateMainStatusLabel), new object[] { ("Searching " + possibleiMet1UPort[i]) });
                        }
                        catch
                        {
                            //writeToErrorLog("Unable to report searching for radiosonde on " + possibleSondeNames[i]);
                        }


                        if (possibleiMet1U[i].checkForRadiosonde())
                        {
                            numberOfValidRadiosondes++;
                        }
                        else
                        {
                            possibleiMet1U[i] = null;
                        }
                    

                    }

                    ValidSondeArrayiMet1U = new iMet1U[numberOfValidRadiosondes];



                    //Compiling the Valid radiosondes into the global.
                    for (int i = 0, x = 0; x < ValidSondeArrayiMet1U.Length && i < possibleiMet1U.Length; i++)
                    {

                        if (possibleiMet1U[i] != null)
                        {
                            ValidSondeArrayiMet1U[x] = possibleiMet1U[i];
                            ValidSondeArrayiMet1U[x].OpenPort();
                            ValidSondeArrayiMet1U[x].setPollResponseMode();
                            System.Threading.Thread.Sleep(100);
                            ValidSondeArrayiMet1U[x].setManufacturingMode(true);
                            x++;
                        }

                    }

                    //Setup the data grids for each racks.
                    try
                    {
                        this.Invoke(new updateDataGrid(this.updateDataGrid), new object[] { numberOfValidRadiosondes });
                    }
                    catch
                    {
                        logProgramEvent("Unable to update one of the rack data grids.");
                    }

                    //Reporting how many sondes found.
                    this.Invoke(new updateMainToolStrip(this.updateMainStatusLabel), new object[] { ValidSondeArrayiMet1U.Length.ToString() + " Radiosonde(s) found." });
                    logProgramEvent(ValidSondeArrayiMet1U.Length.ToString() + " Radiosonde(s) found.");

                    //In cas no sondes are found.
                    if (numberOfValidRadiosondes == 0)
                    {
                        DialogResult search0results = MessageBox.Show("No Radiosonde detected. Is the power on?", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        CommunicationControl.commControlRadiosonde = false;
                        return;
                    }
                    else
                    {
                        CommunicationControl.commControlRadiosonde = true;
                    }

                    //////////////////////////////////////////////////////////////////////////////////////////////////
                    //////Building a temp radiosondes count check to solve some stupid problem.
                    //////////////////////////////////////////////////////////////////////////////////////////////
                    if (MessageBox.Show("Did you get the number of expected Radiosondes?", "Radiosonde Count", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1) == DialogResult.No)
                    {
                        CommunicationControl.commControlRadiosonde = false;
                        return;
                    }


                    //Radiosonde discovery complete.
                    CalibrationStatus.statusRadiosondeReady = true;

                    //Clearing the possible
                    possibleiMet1U = null;

                    while (!backgroundWorkerCollectRadiosondeData.CancellationPending)
                    {
                        //Allows data collection to be paused. This allows the command to be sent.
                        if (CommunicationControl.commControlRadiosonde == true)
                        {
                            //Update data to data grids.
                            //Also maybe some checking on the status of the radiosonde should go here.

                            TimeSpan timeSpanSinceLastLogging = DateTime.Now - timeDataLastLogged;

                            
                                try
                                {
                                    this.Invoke(new sendDataToScreen(this.updateDataGridData));
                                }
                                catch
                                {
                                    if (!backgroundWorkerCollectRadiosondeData.CancellationPending)
                                    {
                                        logProgramEvent("Unable to start Radiosonde update to screen");
                                    }
                                }
                            


                            

                            //Logging radiosonde data like the multi-sonde
                            if (timeSpanSinceLastLogging.TotalSeconds >= 10)
                            {
                                logValidRadiosondeData();
                                timeDataLastLogged = DateTime.Now;

                            }

                            //Letting calibration know that the radiosonde data is ready.
                            dataFromRadiosondesReady.Set();

                            System.Threading.Thread.Sleep(500);
                        }
                    }

                    #endregion
                    break;

            }
        }

        void backgroundWorkerCollectRadiosondeData_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Radiosondes are no longer ready. Resetting the global indicating so.
            CalibrationStatus.statusRadiosondeReady = false;

            switch (ProgramSettings.radiosondeType)
            {
                case "iMet-1":
                    //Closing all radiosonde com ports.
                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        ValidSondeArryiMet1[i].ComPort.Dispose();
                    }

                    //A little pause to make sure all com ports are closed.
                    System.Threading.Thread.Sleep(1000);
                    //nulling all the radiosonde
                    ValidSondeArryiMet1 = null;
                    System.Threading.Thread.Sleep(1000);


                    break;

                case "Bell202":
                    for (int x = 0; x < ValidSondeArrayiMet1U.Length; x++)
                    {
                        ValidSondeArrayiMet1U[x].closePort();
                    }


                    //A little pause to make sure all com ports are closed.
                    System.Threading.Thread.Sleep(1000);
                    //nulling all the radiosonde
                    ValidSondeArrayiMet1U = null;
                    System.Threading.Thread.Sleep(1000);

                    break;
            }

            //Checking to see if a calibration is still in process and if so restarting radiosonde collection.

            //Stopping to make sure some other prcess have stopped before resetting the collection.
            System.Threading.Thread.Sleep(1000);
            if (CalibrationStatus.statusCalibrationInProcess)
            {
                logProgramEvent("Radiosonde collection stopped. Calibration still in progress. Restarting radiosonde collection.");

                startRadiosondeCollection();

            }

            //Updateing status on what has happen.
            try
            {
                BeginInvoke(new updateMainToolStrip(updateMainStatusLabel), new object[] { "Radiosonde collection stopped." });
            }
            catch
            {
            }

        }

        public bool checkRadiosondeData(iMet1 radiosondeToTest)
        {
            bool statusOfRadiosonde = false;
            bool tempStatus = false;
            bool humidityStatus = false;

            double[] currentReferanceData = getReferanceData(ProgramSettings.referanceCorrectionMode);

            radiosondeToTest.getLastHexData();

            //Deciding if the readiosonde air temp is acceptable.
            if (Convert.ToDouble(radiosondeToTest.getAirTemp()) <= (currentReferanceData[0] + ProgramSettings.tolAcceptableAirTempDiff) &&
                Convert.ToDouble(radiosondeToTest.getAirTemp()) >= (currentReferanceData[0] - ProgramSettings.tolAcceptableAirTempDiff))
            {
                tempStatus = true;
            }

            //Deciding if the humidity differance is acceptable.
            if (Convert.ToDouble(radiosondeToTest.getRHFreqEPlusE()) <= Convert.ToDouble(ProgramSettings.tolHumidityFreqMean) + Convert.ToDouble(ProgramSettings.tolHumidityFrewPlusMinus) &&
                Convert.ToDouble(radiosondeToTest.getRHFreqEPlusE()) >= Convert.ToDouble(ProgramSettings.tolHumidityFreqMean) - Convert.ToDouble(ProgramSettings.tolHumidityFrewPlusMinus))
            {
                humidityStatus = true;
            }

            if (tempStatus && humidityStatus)
            {
                statusOfRadiosonde = true;
            }

            return statusOfRadiosonde;
        }

        #endregion

        #region Methods for setting up and operating the Relay Controller

        public void startRelayController()
        {
            switch (ProgramSettings.humiditySource)
            {
                case "Thunder":
                    relayController.relayController(ProgramSettings.relayComPort);

                    for (int i = 0; i < 16; i++)
                    {
                        if (ProgramSettings.relays[i, 0] != null)
                        {
                            relayController.setRelayFunction(Convert.ToInt16(ProgramSettings.relays[i, 0]), ProgramSettings.relays[i, 1], ProgramSettings.relays[i, 2]);
                        }

                    }
                    break;
            }
        }

        #endregion

        #region Methods for doing updates to the display
        public void updateMainStatusLabel(string incomingMessage)
        {
            toolStripStatusLabelMain.Text = incomingMessage;
        }

        public void updateCalibrationStatusLabel(string incomingMessage)
        {
            toolStripStatusLabelCalibration.Text = incomingMessage;
        }

        private void updateRefSensorDisplay(double currentAirTemp, double currentHumidityTemp)
        {
            labelReferanceAirTemp.Text = currentAirTemp.ToString("0.00");
            labelReferanceHumidity.Text = currentHumidityTemp.ToString("0.00");
        }

        /// <summary>
        /// Method for updateing the humidity gen data to the screen.
        /// </summary>
        private void updateThunderDisplay()
        {
            //Current Status of the Thunder.

            try
            {
                //labelCurrentFrostPoint.Text = referanceThunder.currentFrostPoint.ToString("0.000");
                labelCurrentFrostPoint.Text = genThunder.statsData.currentFrostPoint.ToString("0.000");
                //labelCurrentDewPoint.Text = referanceThunder.currentDewPoint.ToString("0.000");
                labelCurrentDewPoint.Text = genThunder.statsData.currentDewPoint.ToString("0.000");
                //labelCurrentPPMv.Text = referanceThunder.currentPPMv.ToString("0.000");
                labelCurrentPPMv.Text = genThunder.statsData.currentPPMv.ToString("0.000");

                //labelCurrentPPMw.Text = referanceThunder.currentPPMw.ToString("0.000");
                labelCurrentPPMw.Text = genThunder.statsData.currentPPMw.ToString("0.000");
                //labelCurrentRH.Text = referanceThunder.currentRH.ToString("0.000");
                labelCurrentRH.Text = genThunder.statsData.currentRH.ToString("0.000");
                //labelCurrentSaturPSI.Text = referanceThunder.currentSaturPSI.ToString("0.000");
                labelCurrentSaturPSI.Text = genThunder.statsData.currentSaturPSI.ToString("0.000");
                //labelCurrentSaturC.Text = referanceThunder.currentSaturC.ToString("0.000");
                labelCurrentSaturC.Text = genThunder.statsData.currentSaturC.ToString("0.000");
                //labelCurrentTestPSI.Text = referanceThunder.currentTestPSI.ToString("0.000");
                labelCurrentTestPSI.Text = genThunder.statsData.currentTestPSI.ToString("0.000");
                //labelCurrentTestC.Text = referanceThunder.currentTestC.ToString("0.000");
                labelCurrentTestC.Text = genThunder.statsData.currentTestC.ToString("0.000");
                //labelCurrentFlow.Text = referanceThunder.currentFlowRate.ToString("0.000");
                labelCurrentFlow.Text = genThunder.statsData.currentFlowRate.ToString("0.000");

                //The current status
                //if (referanceThunder.currentStaus == 0) { labelCurrentStatus.Text = "Idle"; }
                if (genThunder.statsData.currentStaus == 0) { labelCurrentStatus.Text = "Idle"; }
                //if (referanceThunder.currentStaus == 1) { labelCurrentStatus.Text = "Running"; }
                if (genThunder.statsData.currentStaus == 1) { labelCurrentStatus.Text = "Running"; }
                //if (referanceThunder.currentStaus == -1) { labelCurrentStatus.Text = "Purging"; }
                if (genThunder.statsData.currentStaus == -1) { labelCurrentStatus.Text = "Purging"; }

                //Current Set Points of the Thunder.
                //labelSetFrostPoint.Text = referanceThunder.setPointFrostPoint.ToString("0.000");
                labelSetFrostPoint.Text = genThunder.setPointData.setPointFrostPoint.ToString("0.000");
                //labelSetDewPoint.Text = referanceThunder.setPointDewPoint.ToString("0.000");
                labelSetDewPoint.Text = genThunder.setPointData.setPointDewPoint.ToString("0.000");
                //labelSetPPMv.Text = referanceThunder.setPointPPMv.ToString("0.000");
                labelSetPPMv.Text = genThunder.setPointData.setPointPPMv.ToString("0.000");
                //labelSetPPMw.Text = referanceThunder.setPointPPMw.ToString("0.000");
                labelSetPPMw.Text = genThunder.setPointData.setPointPPMw.ToString("0.000");
                //labelSetRH.Text = referanceThunder.setPointRH.ToString("0.000");
                labelSetRH.Text = genThunder.setPointData.setPointRH.ToString("0.000");
                //labelSetSaturPSI.Text = referanceThunder.setPointSaturPSI.ToString("0.000");
                labelSetSaturPSI.Text = genThunder.setPointData.setPointSaturPSI.ToString("0.000");
                //labelSetSaturC.Text = referanceThunder.setPointSaturC.ToString("0.000");
                labelSetSaturC.Text = genThunder.setPointData.setPointSaturC.ToString("0.000");
                //labelSetTestPSI.Text = referanceThunder.setPointTestPSI.ToString("0.000");
                labelSetTestPSI.Text = genThunder.setPointData.setPointTestPSI.ToString("0.000");
                //labelSetTestC.Text = referanceThunder.setPointTestC.ToString("0.000");
                labelSetTestC.Text = genThunder.setPointData.setPointTestC.ToString("0.000");
                //labelSetFlow.Text = referanceThunder.setPointFlowRate.ToString("0.000");
                labelSetFlow.Text = genThunder.setPointData.setPointFlowRate.ToString("0.000");

                //Indicating what setup the Thunder is trying to achive.

                //Resetting the back colors to indicate.
                Color colorNormal = System.Drawing.Color.White;
                Color colorSetPoint = System.Drawing.Color.LightGreen;

                labelSetFrostPoint.BackColor = colorNormal;
                labelSetDewPoint.BackColor = colorNormal;
                labelSetPPMv.BackColor = colorNormal;
                labelSetPPMw.BackColor = colorNormal;
                labelSetRH.BackColor = colorNormal;
                labelSetSaturPSI.BackColor = colorNormal;
                labelSetSaturC.BackColor = colorNormal;
                labelSetTestPSI.BackColor = colorNormal;
                labelSetTestC.BackColor = colorNormal;
                labelSetFlow.BackColor = colorNormal;

                switch (genThunder.setPointData.setPointMode)// referanceThunder.setPointMode)
                {
                    case 0:
                        labelSetFrostPoint.BackColor = colorSetPoint;
                        break;

                    case 1:
                        labelSetDewPoint.BackColor = colorSetPoint;
                        break;

                    case 2:
                        labelSetPPMv.BackColor = colorSetPoint;
                        break;

                    case 3:
                        labelSetPPMw.BackColor = colorSetPoint;
                        break;

                    case 4:
                        labelSetRH.BackColor = colorSetPoint;
                        break;

                    case 5:
                        labelSetSaturPSI.BackColor = colorSetPoint;
                        break;
                }
            }
            catch
            {

            }

        }

        public void updateChamberDisplay()
        {
            switch (ProgramSettings.ChamberController)
            {
                case "Watlow":
                    //Disabling the humidity section as it is unneeded.
                    labelChamberHumiditySetPoint.Enabled = false;
                    labelChamberHumidity.Enabled = false;
                    labelChamberHumidityAvg.Enabled = false;
                    labelChamberHumidityRamp.Enabled = false;

                    //Display the new data.
                    labelChamberAirTempSetPoint.Text = chamberWatlow.ProcessCurrentSetPoint(chamberWatlow.currentRawSPAirTemp).ToString("0.00");
                    labelChamberAirTemp.Text = chamberWatlow.ProcessCurrentTemp(chamberWatlow.currentRawAirTemp).ToString("0.00");
                    labelChamberAirTempRamp.Text = chamberWatlow.ProcessPowerOutput(chamberWatlow.currentRawAirTempRamp).ToString("0.00");
                    break;

                case "Thermotron":
                    //Enabling the humidity labels as that are needed.
                    labelChamberHumiditySetPoint.Enabled = true;
                    labelChamberHumidity.Enabled = true;
                    labelChamberHumidityAvg.Enabled = true;
                    labelChamberHumidityRamp.Enabled = true;

                    //Display the new data.
                    labelChamberAirTempSetPoint.Text = chamberThermotron.CurrentChamberSPPC.ToString("0.00");
                    labelChamberAirTemp.Text = chamberThermotron.CurrentChamberPC.ToString("0.00");
                    labelChamberAirTempRamp.Text = chamberThermotron.CurrentChamberPTHTL.ToString();

                    labelChamberHumiditySetPoint.Text = chamberThermotron.CurrentChamberSPRH.ToString("0.00");
                    labelChamberHumidity.Text = chamberThermotron.CurrentChamberRH.ToString("0.00");
                    labelChamberHumidityRamp.Text = chamberThermotron.CurrentChamberRHTHTL.ToString();
                    break;
            }
        }

        public void reportErrorMessageBox(string message, string title)
        {
            MessageBox.Show(this, message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        
        public void updateDataGrid(int counterNumberOfRadiosondes)
        {
            dataGridViewRadiosondes.Rows.Add(counterNumberOfRadiosondes - 1);
        }

        public void updateDataGridData()
        {

            double[] currentEnclosureData = getReferanceData(ProgramSettings.referanceCorrectionMode); //0 = AT 1 = humidity

            switch (ProgramSettings.radiosondeType)
            {
                case "iMet-1":
                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        dataGridViewRadiosondes.Rows[i].Cells[0].Value = ValidSondeArryiMet1[i].ComPort.PortName;
                        dataGridViewRadiosondes.Rows[i].Cells[1].Value = ValidSondeArryiMet1[i].getTrackingID();
                        dataGridViewRadiosondes.Rows[i].Cells[2].Value = ValidSondeArryiMet1[i].getPacketCount();
                        dataGridViewRadiosondes.Rows[i].Cells[3].Value = (Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentEnclosureData[0]).ToString("0.00");
                        dataGridViewRadiosondes.Rows[i].Cells[4].Value = ValidSondeArryiMet1[i].getRHFreqEPlusE();
                        dataGridViewRadiosondes.Rows[i].Cells[5].Value = ValidSondeArryiMet1[i].getFirmwareVersion();

                        //Graphic update to let you know if the radiosonde is ready or not.
                        //Temp pass/fail
                        if (Math.Abs(Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentEnclosureData[0]) < ProgramSettings.tolAcceptableAirTempDiff)
                        {
                            dataGridViewRadiosondes.Rows[i].Cells[3].Style.BackColor = ProgramSettings.colorReady;
                        }
                        else
                        {
                            dataGridViewRadiosondes.Rows[i].Cells[3].Style.BackColor = ProgramSettings.colorFail;
                        }

                        //Humidity pass/fail
                        if (Convert.ToDouble(ValidSondeArryiMet1[i].getRHFreqEPlusE()) <= (ProgramSettings.tolHumidityFreqMean + ProgramSettings.tolHumidityFrewPlusMinus) &&
                            Convert.ToDouble(ValidSondeArryiMet1[i].getRHFreqEPlusE()) >= (ProgramSettings.tolHumidityFreqMean - ProgramSettings.tolHumidityFrewPlusMinus))
                        {
                            dataGridViewRadiosondes.Rows[i].Cells[4].Style.BackColor = ProgramSettings.colorReady;
                        }
                        else
                        {
                            dataGridViewRadiosondes.Rows[i].Cells[4].Style.BackColor = ProgramSettings.colorFail;
                        }
                    }
                    break;

                case "Bell202":

                    for (int bell = 0; bell < ValidSondeArrayiMet1U.Length; bell++)
                    {
                        dataGridViewRadiosondes.Rows[bell].Cells[0].Value = bell;
                        dataGridViewRadiosondes.Rows[bell].Cells[1].Value = ValidSondeArrayiMet1U[bell].CalData.SerialNumber;
                        dataGridViewRadiosondes.Rows[bell].Cells[2].Value = ValidSondeArrayiMet1U[bell].CalData.PacketNumber;
                        dataGridViewRadiosondes.Rows[bell].Cells[3].Value = (Convert.ToDouble(ValidSondeArrayiMet1U[bell].CalData.Temperature) - currentEnclosureData[0]).ToString("0.00");
                        dataGridViewRadiosondes.Rows[bell].Cells[4].Value = ValidSondeArrayiMet1U[bell].CalData.HumidityFrequency;
                        dataGridViewRadiosondes.Rows[bell].Cells[5].Value = ValidSondeArrayiMet1U[bell].FirmwareVersion;

                        //Graphic update to let you know if the radiosonde is ready or not.
                        //Temp pass/fail
                        if (Math.Abs(Convert.ToDouble(ValidSondeArrayiMet1U[bell].CalData.Temperature) - currentEnclosureData[0]) < ProgramSettings.tolAcceptableAirTempDiff)
                        {
                            dataGridViewRadiosondes.Rows[bell].Cells[3].Style.BackColor = ProgramSettings.colorReady;
                        }
                        else
                        {
                            dataGridViewRadiosondes.Rows[bell].Cells[3].Style.BackColor = ProgramSettings.colorFail;
                        }

                        //Humidity pass/fail
                        if (Convert.ToDouble(ValidSondeArrayiMet1U[bell].CalData.HumidityFrequency) <= (ProgramSettings.tolHumidityFreqMean + ProgramSettings.tolHumidityFrewPlusMinus) &&
                            Convert.ToDouble(ValidSondeArrayiMet1U[bell].CalData.HumidityFrequency) >= (ProgramSettings.tolHumidityFreqMean - ProgramSettings.tolHumidityFrewPlusMinus))
                        {
                            dataGridViewRadiosondes.Rows[bell].Cells[4].Style.BackColor = ProgramSettings.colorReady;
                        }
                        else
                        {
                            dataGridViewRadiosondes.Rows[bell].Cells[4].Style.BackColor = ProgramSettings.colorFail;
                        }
                    }

                    break;
            }
        }

        public void updateDataGridClear()
        {
            dataGridViewRadiosondes.Rows.Clear();
            if (dataGridViewRadiosondes.Rows.Count > 0)
            {
                for (int i = 0; i < dataGridViewRadiosondes.Rows.Count; i++)
                {
                    try
                    {
                        dataGridViewRadiosondes.Rows.RemoveAt(i);
                    }
                    catch { }
                }
            }
        }

        //Updated the screen with the current Referance Sensor TU diff's.
        private void updateReferanceDifferance(bool dataReady)
        {

            //Displaying data.
            labelReferanceAirTempAvg.Text = CalibrationStatus.currentDiffReferanceTemp.ToString("0.000");
            labelReferanceHumidityAvg.Text = CalibrationStatus.currentDiffReferanceHumidity.ToString("0.000");

            if (dataReady)
            {
                //Changing the background color to indicate good data.
                labelReferanceAirTempAvg.BackColor = ProgramSettings.colorReady;
                labelReferanceHumidityAvg.BackColor = ProgramSettings.colorReady;

                
            }
            else
            {
                //Changing the background color to indicate bad data or not ready.
                labelReferanceAirTempAvg.BackColor = ProgramSettings.colorInProcess;
                labelReferanceHumidityAvg.BackColor = ProgramSettings.colorInProcess;
            }
        }

        //Update the screen with the chambers current TU diff's.
        private void updateChamberDifferance(bool dataReady)
        {
            //Displaying data.
            labelChamberAirTempAvg.Text = CalibrationStatus.currentDiffChamberTemp.ToString("0.000");
            labelChamberHumidityAvg.Text = CalibrationStatus.currentDiffChamberHumidity.ToString("0.000");

            if (dataReady)
            {
                //Changing the background color to indicate good data.
                labelChamberAirTempAvg.BackColor = ProgramSettings.colorReady;
                labelChamberHumidityAvg.BackColor = ProgramSettings.colorReady;

                
            }
            else
            {
                //Changing the background color to indicate bad or not ready data.
                labelChamberAirTempAvg.BackColor = ProgramSettings.colorInProcess;
                labelChamberHumidityAvg.BackColor = ProgramSettings.colorInProcess;
            }
        }

        //Update for Calibration Status
        public void updateCalibrationStatus(double currentATSetPoint, double currentUSetPoint, double currentAT, double currentU,
            double currentATDiff, double currentUDiff,
            bool currentChamberReady, double currentChamberReadyCount,
            bool currentEnclosureReady, double currentEnclosureReadyCount,
            bool currrentThunderReady, double currentThunderReadyCount)
        {
            //Displaying data elapsed
            DateTime currentTime = DateTime.Now;
            TimeSpan elapseTime = currentTime - CalibrationStatus.calibrationStartTime;
            labelCalibraitonRunTime.Text = elapseTime.Hours.ToString("00") + ":" + elapseTime.Minutes.ToString("00") + ":" + elapseTime.Seconds.ToString("00") + "." + elapseTime.Milliseconds.ToString("0000");
            //labelCalibraitonRunTime.Text = (currentTime - CalibrationStatus.calibrationStartTime).ToString();

            //Displaying temp and humidity data.
            //Temp data
            labelCalibrationCurrentATSetPoint.Text = currentATSetPoint.ToString("0.00");
            labelCalibrationCurrentAT.Text = currentAT.ToString("0.00");
            labelCalibrationCurrentATDiff.Text = currentATDiff.ToString("0.00");
            //Humidity Data
            labelCalibrationCurrentUSetPoint.Text = currentUSetPoint.ToString("0.00");
            labelCalibrationCurrentU.Text = currentU.ToString("0.00");
            labelCalibrationCurrentUDiff.Text = currentUDiff.ToString("0.00");

            //Graphic good/bad stuff for TU
            if (Math.Abs(Convert.ToDouble(currentATDiff)) <= Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 2]))
            {
                if (Math.Abs(currentATDiff) <= Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 3]))
                {
                    labelCalibrationCurrentATDiff.BackColor = ProgramSettings.colorPass;
                }
                else
                {
                    labelCalibrationCurrentATDiff.BackColor = ProgramSettings.colorReady;
                }                
            }
            else
            {
                labelCalibrationCurrentATDiff.BackColor = ProgramSettings.colorFail;
            }

            if (Math.Abs(Convert.ToDouble(currentUDiff)) <= Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 9]))
            {
                if (Math.Abs(Convert.ToDouble(currentUDiff)) <= Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 10]))
                {
                    labelCalibrationCurrentUDiff.BackColor = ProgramSettings.colorPass;
                }
                else
                {
                    labelCalibrationCurrentUDiff.BackColor = ProgramSettings.colorReady;
                }
            }
            else
            {
                labelCalibrationCurrentUDiff.BackColor = ProgramSettings.colorFail;
            }

            //Displaying Stable true/false for chamber,enclosure, and thunder.
            //Displaying counts
            labelCalibrationChamberStatus.Text = currentChamberReadyCount.ToString("0.00");
            labelCalibrationEnclouserStatus.Text = currentEnclosureReadyCount.ToString("0.00");
            labelCalibrationThunderStatus.Text = currentThunderReadyCount.ToString("0.00");

            //Now the graphic good/bad.
            if (currentChamberReady)
            {
                labelCalibrationChamberStatus.BackColor = ProgramSettings.colorPass;
            }
            else 
            {
                labelCalibrationChamberStatus.BackColor = ProgramSettings.colorFail; 
            }

            if (currentEnclosureReady)
            {
                labelCalibrationEnclouserStatus.BackColor = ProgramSettings.colorPass; 
            }
            else
            { 
                labelCalibrationEnclouserStatus.BackColor = ProgramSettings.colorFail; 
            }

            //Thunder good/bad
            if (!CalibrationStatus.prepThunder)
            {
                if (currrentThunderReady)
                {
                    labelCalibrationThunderStatus.BackColor = ProgramSettings.colorPass;
                }
                else
                {
                    labelCalibrationThunderStatus.BackColor = ProgramSettings.colorFail;
                }
            }
            else
            {
                labelCalibrationThunderStatus.BackColor = System.Drawing.Color.Gold;
            }



        }

        //Reseting Calibration Status display
        public void updateCalibrationStatusReset()
        {
            groupBoxCalibrationStatus.Enabled = false;
            labelCalibraitonRunTime.Text = "0.00";
            labelCalibrationCurrentATSetPoint.Text = "0.00";
            labelCalibrationCurrentAT.Text = "0.00";
            labelCalibrationCurrentATDiff.Text = "0.00";

            labelCalibrationCurrentU.Text = "0.00";
            labelCalibrationCurrentUDiff.Text = "0.00";
            labelCalibrationCurrentUSetPoint.Text = "0.00";

            labelCalibrationChamberStatus.Text = "";
            labelCalibrationChamberStatus.BackColor = System.Drawing.Color.White;

            labelCalibrationEnclouserStatus.Text = "";
            labelCalibrationEnclouserStatus.BackColor = System.Drawing.Color.White;

            labelCalibrationRadiosondeStatus.Text = "";
            labelCalibrationRadiosondeStatus.BackColor = System.Drawing.Color.White;

            labelCalibrationThunderStatus.Text = "";
            labelCalibrationThunderStatus.BackColor = System.Drawing.Color.White;

        }

        public void resetCalibrationStatus()
        {
            //Clearing all the label of calibation data.
            labelCalibraitonRunTime.Text = "";

            labelCalibrationChamberStatus.Text = "";
            labelCalibrationChamberStatus.BackColor = System.Drawing.Color.White;
            labelCalibrationEnclouserStatus.Text = "";
            labelCalibrationEnclouserStatus.BackColor = System.Drawing.Color.White;
            labelCalibrationThunderStatus.Text = "";
            labelCalibrationThunderStatus.BackColor = System.Drawing.Color.White;
            labelCalibrationRadiosondeStatus.Text = "";
            labelCalibrationRadiosondeStatus.BackColor = System.Drawing.Color.White;

            labelCalibrationCurrentAT.Text = "";
            labelCalibrationCurrentATDiff.Text = "";
            labelCalibrationCurrentATDiff.BackColor = System.Drawing.Color.White;
            labelCalibrationCurrentATSetPoint.Text = "";

            labelCalibrationCurrentU.Text = "";
            labelCalibrationCurrentUDiff.Text = "";
            labelCalibrationCurrentUDiff.BackColor = System.Drawing.Color.White;
            labelCalibrationCurrentUSetPoint.Text = "";

            //Disableing the groupbox.
            groupBoxCalibrationStatus.Enabled = false;

            //Resetting the calibration button.
            buttonStartCalibration.Text = "Start";
        }

        private void updateCurrentSelectedRadiosonde(int radiosondeIndex)
        {

        }

        #endregion

        #region Methods pertaining to the calibration process.
        private void buttonStartCalibration_Click(object sender, EventArgs e)
        {
            //Starting the calibration process.
            if (buttonStartCalibration.Text == "Start")
            {
                //Checking to make sure the components needed for calibration are running.
                string errorReport = "";
                
                //Checking to make sure the radiosonde mode is selected.
                if (comboBoxRadiosondeType.Text == "Radiosonde Type")
                {
                    errorReport += "Radiosonde type not selected. Select and type and try again.";
                }


                //Checking chamber controller
                switch (ProgramSettings.ChamberController)
                {
                    case "Watlow":
                        if (chamberWatlow == null || !chamberWatlow.chamber.sp.IsOpen)
                        {
                            if(errorReport != ""){ errorReport += "\n";}
                            errorReport += "Chamber Disconnected.";
                        }
                        break;

                    case "Thermotron":
                        if (ProgramSettings.ChamberControllerComPort.ToLower().Contains("com"))
                        {
                            if (chamberThermotron == null || !chamberThermotron.ComPort.IsOpen)
                            {
                                if (errorReport != "") { errorReport += "\n"; }
                                errorReport += "Chamber Disconnected.";
                            }
                            else
                            {

                            }
                        }
                        break;
                }

                //Cheaking referance sensors
                switch (ProgramSettings.calibrationReferanceSensor)
                {
                    case "HMP234":
                        if (referanceHMP234 == null || !referanceHMP234.ComPort.IsOpen)
                        {
                            if (errorReport != "") { errorReport += "\n"; }
                            errorReport += "Referance Disconnected.";
                        }
                        break;

                    case "EPlusE":
                        if (referanceEE31 == null || !referanceEE31.ComPort.IsOpen)
                        {
                            if (errorReport != "") { errorReport += "\n"; }
                            errorReport += "Referance Disconnected.";
                        }
                        break;
                }

                //Checking Humidity Generator
                switch (ProgramSettings.humiditySource)
                {
                    case "Thunder": //Thunder ready check.
                        if (genThunder == null)
                        {
                            if (errorReport != "") { errorReport += "\n"; }
                            errorReport += "Thunder Disconnected.";
                        }
                        break;
                        
                    case"Thermotron": //Checking to make sure the thermotron is online and ready.
                        if (chamberThermotron == null)
                        {
                            if (errorReport != "") { errorReport += "\n"; }
                            errorReport += "Thermotron Humidity Not Ready.";
                        }
                        break;
                }

                //If there is an error send that to screen.
                if (errorReport != "")
                {
                    MessageBox.Show("Can not start calibration because:\n\n" + errorReport + "\n\nCheck settings and try again.", "Calibration Start Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return; //Stops the calibration from happening if the system is not ready.
                }
                
                ///////////////////////////////////////////////////
                //Calibration is now going to start
                ///////////////////////////////////////////////////


                //Start time for the current calibration.
                CalibrationStatus.calibrationStartTime = DateTime.Now;

                CalibrationStatus.currentOverallLog = ProgramSettings.RAWFileLOC + DateTime.Now.ToString("yyyyMMdd-HHmm");

                //Creating dir for the current calibrarion.
                DirectoryInfo di = Directory.CreateDirectory(CalibrationStatus.currentOverallLog);
                

                ProgramStatus.currentEventLog = ProgramSettings.RAWFileLOC + DateTime.Now.ToString("yyyyMMdd-HHmm") + ".log";

                //Enable logging the old way.
                ProgramStatus.statusRadiosondeLogging = true;

                //Setting the current program status.
                CalibrationStatus.currentProgramMode = "running";

                //Enabling calibration status group box.
                groupBoxCalibrationStatus.Enabled = true;

                //Setting the calibration process is active
                CalibrationStatus.statusCalibrationInProcess = true;

                //Setting up the radiosonde background worker.
                if (backgroundWorkerCollectRadiosondeData == null || ValidSondeArryiMet1 == null)
                {
                    startRadiosondeCollection();
                }

                //Starting up Enviroment and Health checking.
                startCalibrationEviromentStability();

                //Indicating the calibration has started.
                CalibrationStatus.statusCalibrationInProcess = true;

                //Start background worker that checks to see if set points have been reached and if the conditions are stable.
                startCalibrationProcess();

                //Start performance log. The log will help me determin the best settings for calibration.
                startProformanceLog();

                //Starting the chamber if needed.
                if (ProgramSettings.ChamberController == "Thermotron")
                {
                    chamberThermotron.startChamberManualMode();
                }


                //Setting the button text to stop to handle stopping the calibration.
                buttonStartCalibration.Text = "Stop";
                return;
            }

            //To Stop the calibration.
            if (buttonStartCalibration.Text == "Stop")
            {
                //Checking to make sure you want to stop current calibration.
                DialogResult checkStopCalibration = MessageBox.Show("Are you sure you want to stop the calibration?", "Stop Calibration", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

                if (checkStopCalibration == DialogResult.Yes)
                {
                    CalibrationStatus.currentProgramMode = "abort";
                    stopCurrentCalibration();
                    buttonStartCalibration.Text = "Start";
                }
                else
                {
                    return;
                }
            }

        }

        private void stopCurrentCalibration()
        {
            //Stopping Calibration Process.
            stopCalibrationProcess();

            //Stopping Evnviroment checking
            stopCalibrationEviromentStability();

            //Stopping radiosonde and resetting datagrid
            stopRadiosondeCollection();
            System.Threading.Thread.Sleep(500);
            dataGridViewRadiosondes.Rows.Clear(); //needs work but will do for now.

            //Clearing anything left in memory that might be hanging around.
            System.GC.Collect(); 
        }

        public bool getStatusCalibrationInProcess()
        {
            return CalibrationStatus.statusCalibrationInProcess;
        }

        #region Main Calibration methods. This section does most of the processing.

        public void startCalibrationProcess()
        {
            //This will start the main process that runs the calibration.
            backgroundWorkerCalibrationProcess = new BackgroundWorker();
            backgroundWorkerCalibrationProcess.WorkerSupportsCancellation = true;
            backgroundWorkerCalibrationProcess.DoWork += new DoWorkEventHandler(backgroundWorkerCalibrationProcess_DoWork);
            backgroundWorkerCalibrationProcess.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerCalibrationProcess_RunWorkerCompleted);
            backgroundWorkerCalibrationProcess.RunWorkerAsync();

            //Updating the display
            updateFormDisplay();
        }

        public void stopCalibrationProcess()
        {
            backgroundWorkerCalibrationProcess.CancelAsync();
            CalibrationStatus.currentCalibrationPoint = 0;
            CalibrationStatus.statusCalibrationInProcess = false;

            //Updating the display
            updateFormDisplay();
        }

        public void updateFormDisplay()
        {
            if (CalibrationStatus.statusCalibrationInProcess)
            {
                comboBoxRadiosondeType.Enabled = false;
            }
            else
            {
                comboBoxRadiosondeType.Enabled = true;
            }
        }

        void backgroundWorkerCalibrationProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            //Main processing will be done here. Other threads will feed it, but this runs the train.
            
            //CalibrationStatus.currentCalibrationPoint = 0; //Starting at the fist cal point.

            DateTime TimeCalibrationPointStarted;

            for (int i = 0; i < (ProgramSettings.calibrationSetPoint.Length / 22); i++) //This is the main loop for processing set points.
            {
                //Something to shutdown the process thread.
                if (backgroundWorkerCalibrationProcess.CancellationPending)
                {
                    break;
                }

                TimeCalibrationPointStarted = DateTime.Now;

                //This is a test item remove later.
                CalibrationStatus.okToStartCollectingData = false;                

                updateCalibrationStatusLabel("Going to step: " + i.ToString());
                logProgramEvent("Going to step: " + i.ToString());
                //Setting attched equpment to the proper settings for this set point.
                //Setting Air Temp of Chamber
                //This section if where the ratio temp background worker would come in.... maybe later.
                CommunicationControl.commControlChamber = false; //temp stop of pooling

                //If the setting is set to "same" then use the current chamber set point.
                double chamberSetPoint = 0;
                if (ProgramSettings.calibrationSetPoint[i, 4] == "same")
                {
                    ProgramSettings.calibrationSetPoint[i, 4] = ProgramSettings.calibrationSetPoint[i - 1, 4];
                    chamberSetPoint = Convert.ToDouble(ProgramSettings.calibrationSetPoint[i - 1, 4]);

                }
                else //This set the chamber really low to bring the temp down quickly. In the envoriment stability I am going to reset the chamber temp to the set point wanted.
                {
                    double[] currentChamber = getChamberData();
                    
                    double[] currentEnclouserData = getReferanceData(ProgramSettings.referanceCorrectionMode);

                    if (ProgramSettings.ChamberController == "Watlow")
                    {

                        if (currentEnclouserData[0] > Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 1]) + Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 2]) ||
                            currentEnclouserData[0] < Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 1]) - Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 2]))
                        {

                            chamberSetPoint = -7;

                        }
                        else
                        {
                            chamberSetPoint = Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 4]);
                        }
                    }
                    else
                    {
                        chamberSetPoint = Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 4]);
                        
                        //Purging the high humidity enviroment.
                        if (CalibrationStatus.currentCalibrationPoint != 0) //This doesn't need to run at the start only when the system is moving from high temp to low.
                        {
                            if ((currentChamber[0] - chamberSetPoint) > 10)
                            {
                                //Close humidity ports if needed.
                                resetValves();  //Closeing all connections.
                                relayController.commandRelayBoard("nitrogenValve", true); //Opening nitrogen. This should be replaced with a high flow nitrogen purge.

                                //wait for humidity of enclosure to drop.
                                double[] refData = getReferanceData(false); //Getting the starting referance reading.
                                double purgeSetpoint = 5;   //The purge down limit.

                                while (refData[1] > purgeSetpoint && !backgroundWorkerCalibrationProcess.CancellationPending)   //Waiting for the humidity of the enclouser drop below the purgeSetpoint.
                                {
                                    System.Diagnostics.Debug.WriteLine("Waiting for humidity to drop. " + (refData[1] - purgeSetpoint).ToString("0.00"));    //Outputing what is going on.
                                    updateCalibrationStatusLabel("Waiting for humidity to drop. " + (refData[1] - purgeSetpoint).ToString("0.00"));                //Outputing what is going on.
                                    System.Threading.Thread.Sleep(1000);    //Sleeping thread.
                                    refData = getReferanceData(false);      //Updating the referance information.
                                }

                                System.Diagnostics.Debug.WriteLine("Humidity Purge Complete.");
                                updateMainStatusLabel("Humidity Purge Complete.");

                            }
                        }
                    }

                    

                    //chamberSetPoint = Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 4]);
                }



                //Setting the chamber conditons
                setChamberATSetPoint(chamberSetPoint);

                if (CalibrationStatus.currentCalibrationPoint == 0)
                {
                    //This will need an alert... when that is ready.
                    if (ProgramSettings.ChamberController == "Watlow" && tennyChamberRelay == null)
                    {
                        MessageBox.Show("Start Chamber", "Chamber Alert", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        logProgramEvent("Manual Tenny Chamber Start.");
                    }

                    if (ProgramSettings.ChamberController == "Watlow" && tennyChamberRelay != null)
                    {
                        tennyChamberRelay.triggerRelay(true);
                        logProgramEvent("Auto Tenny Chamber Start.");
                    }
       
                }

                //updating the status.
                updateCalibrationStatusLabel("Setting Chamber to: " + ProgramSettings.calibrationSetPoint[i, 4]);
                logProgramEvent("Setting Chamber to: " + ProgramSettings.calibrationSetPoint[i, 4]);

                
                //Resetting the thunder prep bool
                CalibrationStatus.prepThunder = false;

                //Setting humidity generator.
                commandHumidityGenerator(Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 12]), Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 13]), Convert.ToDouble(ProgramSettings.calibrationSetPoint[i, 14]), ProgramSettings.calibrationSetPoint[i, 15]);

                updateCalibrationStatusLabel("Setting Thunder to (SATUR psi: " +  ProgramSettings.calibrationSetPoint[i, 12] +
                    ")(SATIR °C: " +  ProgramSettings.calibrationSetPoint[i, 13] +
                    ")(Flow: " + ProgramSettings.calibrationSetPoint[i, 14] + ")");
                logProgramEvent("Setting Thunder to (SATUR psi: " + ProgramSettings.calibrationSetPoint[i, 12] +
                    ")(SATIR °C: " + ProgramSettings.calibrationSetPoint[i, 13] +
                    ")(Flow: " + ProgramSettings.calibrationSetPoint[i, 14] + ")" + ")(Mode: " + ProgramSettings.calibrationSetPoint[i, 15] + ")");


                //Set flow valves. This should be done by the relay controller, but because we don't have one yet it will be done by hand.
                //An alert should go out.
                commandValve(i);
                 
                //A little break to allow for the equipment to respond.
                System.Threading.Thread.Sleep(2000);

                //Now i am waiting for the set point to be met, and conditions to stablize.
                //int testLoop = 0

                ////////////////////////////////////////////////////////////
                //System will now wait until the calibration stablity is met
                /////////////////////////////////////////////////////////////

                //A little pause to provent the calibration from skipping and cal point. This should give the over process some time to start
                //moving the eviroment and also set the set points to the next point there by marking the eviroment unstable.
                if (CalibrationStatus.okToStartCollectingData)
                {
                    string message = "Calibration run started at: " + CalibrationStatus.calibrationStartTime.ToString("yyyyMMdd-HH:mm:ss") + "\n" +
                        "Is showing OK to start Callection Data: " + CalibrationStatus.okToStartCollectingData.ToString();

                    //Sending out user alert.
                    alertUser("System Error", message, 0);
                    
                    //Waiting and setting the Calibration Stationg ok to false;
                    System.Threading.Thread.Sleep(10000);
                    CalibrationStatus.okToStartCollectingData = false;


                    //This message box was for user interuption. This has been replaced with an auto bypass.
                    /*
                    DialogResult errorRecover = MessageBox.Show(message + "\n" + "Would you like to set the Start Collection Data to False?", "Calibration Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

                    if (errorRecover == DialogResult.Yes)
                    {
                        CalibrationStatus.okToStartCollectingData = false;
                    }
                    */

                }


                while (!CalibrationStatus.okToStartCollectingData)
                {
                    if (backgroundWorkerCalibrationProcess.CancellationPending)
                    {
                        break;
                    }

                    //Calc of time passed at calibration point.
                    TimeSpan timePassedAtThisPoint = DateTime.Now - TimeCalibrationPointStarted;

                    //Outputing current time waiting for stablity.
                    updateCalibrationStatusLabel("Waiting for stable " + timePassedAtThisPoint.TotalMinutes.ToString("0.000"));

                    //Pauseing the thread as it doesn't need to be realtime.
                    System.Threading.Thread.Sleep(1000);

                }

                //Logged Radiosonde packets.
                int counterLoggedPackets = 0;
                logProgramEvent("Starting Data Logging for step: " + CalibrationStatus.currentCalibrationPoint.ToString());

                //Collecting an extra leading referance packet.
                updateCalibrationStatusLabel("Logging Calibration Step: " + CalibrationStatus.currentCalibrationPoint.ToString() + " Logging extra referance packet.");
                dataFromReferanceReady.WaitOne();
                logRefData();

                int dataToTake = 30; //This indicates that number of packets to log during calibration.

                while(counterLoggedPackets < dataToTake)
                {
                    if (backgroundWorkerCalibrationProcess.CancellationPending) { break; }

                    updateCalibrationStatusLabel("Logging Calibration Step: " + CalibrationStatus.currentCalibrationPoint.ToString() + " Packet " + counterLoggedPackets.ToString() + " of " + (dataToTake - 1).ToString() + ".");
                    //Waiting for radiosonde data to come in.
                    dataFromRadiosondesReady.WaitOne();
                    //Logging all valid radiosonde data into file.
                    switch (ProgramSettings.radiosondeType)
                    {
                        case "iMet-1":

                            for (int rsdata = 0; rsdata < ValidSondeArryiMet1.Length; rsdata++)
                            {
                                logSondeData(rsdata);
                            }
                            break;

                        case "Bell202":
                            for (int rsdata = 0; rsdata < ValidSondeArrayiMet1U.Length; rsdata++)
                            {
                                logSondeData(rsdata);
                            }
                            break;
                    }
                    
                    //Waiting for referance data to come in.
                    //This could be the palce for a request for data,,, that or just before I log the radiosonde data, then log it on this side.
                    dataFromReferanceReady.WaitOne();
                    logRefData();
                    counterLoggedPackets++;
                }

                //Collecting an extra following referance packet.
                updateCalibrationStatusLabel("Logging Calibration Step: " + CalibrationStatus.currentCalibrationPoint.ToString() + " Logging extra referance packet.");
                dataFromReferanceReady.WaitOne();
                logRefData();
                
                CalibrationStatus.currentCalibrationPoint++;

                //Detriming if we have reached the end of calibration.
                if(CalibrationStatus.currentCalibrationPoint > (ProgramSettings.calibrationSetPoint.Length / 22))
                {
                    CalibrationStatus.statusCalibrationInProcess = false;
                }

                //Indicating that status need to be cleared.
                CalibrationStatus.clearCalibrationStatus = true;
                
            }

            //Reporting if the cal has ended or been aborted.
            if (CalibrationStatus.currentProgramMode == "running")
            {
                updateCalibrationStatusLabel("Calibration data collection complete.");
                logProgramEvent("Calibration data collection complete.");
            }
            else
            {
                updateCalibrationStatusLabel("Calibration aborted.");
                logProgramEvent("Calibration aborted.");
            }

            backgroundWorkerCollectRadiosondeData.CancelAsync();

            if (checkBoxShutdownAfter.Checked)
            {
                System.Threading.Thread postChamber = new System.Threading.Thread(this.warmAndShutdown);
                postChamber.Name = "Post Chamber Thread";
                postChamber.Start();
            }

        }

        void backgroundWorkerCalibrationProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //This method should retun the chamber to room-ish temp, purg the thunder, and stop the thunder.
            //Also it should reset any condions set when the cal was running.

            //indicating that the calibrationa has ended.
            CalibrationStatus.statusCalibrationInProcess = false;



            //Setting the program mode.
            if (CalibrationStatus.currentProgramMode == "running")
            {
                CalibrationStatus.currentProgramMode = "postProcess";
            }
            
            //Resetting the calibration to the start.
            //Might need to be removed....... I think this might be causeing a restart issue.
            CalibrationStatus.currentCalibrationPoint = 0;

            //Setting chamber to room-ish temp.
            setChamberATSetPoint(25);
            
            logProgramEvent("Chamber set to the end.");


            //Setting humidity generator.
            logProgramEvent("Stopping Thunder");
            commandHumidityGenerator(17, 17, 0.1, "STP");
            logProgramEvent("Thunder Stopped");

            //Set Valves post calibration.
            switch (ProgramSettings.modeValve)
            {
                case "manual":
                    MessageBox.Show("Set valves in the following order:\n\nWARNING THESE VALVES MUST BE SET IN THIS ORDER\n\n" +
                         "Valve C: True" + "\n" +
                          "Valve B: False" + "\n" +
                          "Valve A: False",
                           "Set Valves Alert", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case "auto":
                    //Closing both valve
                    resetValves();
                    logProgramEvent("Closing Nitrogen Valve and Venting Humidity Gen.");
                    break;

            }


            //Maybe a pause is need here to allow the other threads to catch up and then shutdown everything else.
            

            string[] listOfRadiosondes = null; //List of sondes that have good data.

            
            if (CalibrationStatus.currentProgramMode == "postProcess")
            {
                //Checking to make sure the data collected is good before processing it.
                //This is done to prevent bad data from crashing the system.
                listOfRadiosondes = checkCalibrationData(CalibrationStatus.currentOverallLog);

                if (checkBoxPostCorrect.Checked)
                {
                    //Referance sensor correcton application.
                    correctHumidityDataFile(CalibrationStatus.currentOverallLog + "\\" + ProgramSettings.calibrationReferanceSensor + ".u_ref", ProgramSettings.calibrationReferanceSensor);

                    //Calculating Coeficents
                    string[,] calReport = calcCoefficients(listOfRadiosondes, CalibrationStatus.currentOverallLog + "\\" + ProgramSettings.calibrationReferanceSensor + "-Corrected.u_ref");
                }
                else
                {
                    //Calculating Coeficents
                    string[,] calReport = calcCoefficients(listOfRadiosondes, CalibrationStatus.currentOverallLog + "\\" + ProgramSettings.calibrationReferanceSensor + ".u_ref");
                }

                //Load coefficents to the radiosondes.
                updateMainStatusLabel("Loading Coef's");
                loadAllRadiosondeCoef();

                //Alerting user that calibration is complete
                alertUser("Calibration Complete", "E+E Calibration Complete", 0);

                //Move all cal files to master storage location.
                moveCalToStorage();

            }

            //Shutdown radiosondes.
            BeginInvoke(new sendDataToScreen(this.stopRadiosondeCollection));

            //Clear data grid.
            BeginInvoke(new sendDataToScreen(this.updateDataGridClear));


            // Reset display and settings to normal/the beggining of the calibration.
            BeginInvoke(new sendDataToScreen(this.updateCalibrationStatusReset));

            //Reset program
            BeginInvoke(new sendDataToScreen(this.resetCalibrationStatus));

            //Stopping the proformace log
            backgroundWorkerProformanceLog.CancelAsync();

            //Report
            if (CalibrationStatus.currentProgramMode == "postProcess")
            {
                calibrationReport(CalibrationStatus.currentOverallLog + "\\");
            }

            //Calibraton has finished. Setting the program to idle state.
            CalibrationStatus.currentProgramMode = "idle";

            //Enabling the combobox to allow another cal to be run.
            CalibrationStatus.statusCalibrationInProcess = false;
            BeginInvoke(new sendDataToScreen(updateFormDisplay));
            
        }

        private void warmAndShutdown()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;

            
                double curEncTemp = -99.99;

                while (curEncTemp < 10 && checkBoxShutdownAfter.Checked)
                {
                    double[] incomingRef = getReferanceData(true);
                    curEncTemp = incomingRef[0];
                    System.Threading.Thread.Sleep(10000);
                }

                if (curEncTemp > 9.5 && checkBoxShutdownAfter.Checked)
                {
                    if (ProgramSettings.ChamberController == "Watlow")
                    {
                        tennyChamberRelay.triggerRelay(false);
                        logProgramEvent("Shutting Down Chamber");
                    }
                    if (ProgramSettings.ChamberController == "Thermotron")  //Stoping the chamber after calibration is warmed up.
                    {
                        chamberThermotron.stopChamberManualMode();
                    }
                }
            
        }

        /// <summary>
        /// Method checks the collected data for bad sondes and reports them as bad.
        /// </summary>
        /// <returns></returns>
        public bool checkRadiosondeCalData(string desiredRadiosonde)
        {
            bool result = false;
            int countBadPacket = 0;

            string[] referanceData = System.IO.File.ReadAllLines(ProgramSettings.RAWFileLOC + "20110926-1337\\"  + "HMP234.u_ref");
            string[] radiosondeData = System.IO.File.ReadAllLines(ProgramSettings.RAWFileLOC + "20110926-1337\\_" + desiredRadiosonde + ".u_raw"); 

            return result;
        }

        /// <summary>
        /// Method loads coefs to radiosondes
        /// </summary>
        public void loadCoefToSonde(string sondeTrackingNumber)
        {
            string coefFileToLoad = "";

            //Providing option for loading when calibration is not running. Also allows reload after calibration.
            if (CalibrationStatus.currentOverallLog == "")
            {
                MessageBox.Show("No current directory set. Please select the location of the cal file for: " + sondeTrackingNumber, "Cal File");

                //User to select desired radiosonde files
                OpenFileDialog sondeFiles = new OpenFileDialog();
                sondeFiles.Title = "Radiosonde Calibration File";
                sondeFiles.InitialDirectory = ProgramSettings.RAWFileLOC;
                sondeFiles.Filter = "Coef Files (*.u_cal)|*.u_cal|All files (*.*)|*.*";

                DialogResult sondeFilesToCalc = sondeFiles.ShowDialog();
                if (sondeFilesToCalc == DialogResult.OK)
                {
                    coefFileToLoad = sondeFiles.FileName;
                }
                else
                {
                    MessageBox.Show("No Coef Files Provided. Canceling.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
            else
            {
                coefFileToLoad = CalibrationStatus.currentOverallLog + "\\_" + sondeTrackingNumber + ".u_cal";
            }

            switch (ProgramSettings.radiosondeType)
            {
                case "iMet-1":
                    //
                    
                    break;

                case "Bell202":
                    if (ValidSondeArrayiMet1U != null)
                    {
                        for (int i = 0; i < ValidSondeArrayiMet1U.Length; i++)
                        {
                            if (sondeTrackingNumber == ValidSondeArrayiMet1U[i].CalData.SerialNumber)
                            {
                                string[] coefData = System.IO.File.ReadAllLines(coefFileToLoad);

                                for (int x = 0; x < 5; x++)
                                {
                                    ValidSondeArrayiMet1U[i].setCoefficient(36 + x, Convert.ToDouble(coefData[1 + x]));
                                }
                            }
                        }
                    }
                    else
                    {
                        updateMainStatusLabel("Error writing coefs to: " + sondeTrackingNumber);
                    }

                    break;
            }
        }

        public void loadAllRadiosondeCoef()
        {
            switch (ProgramSettings.radiosondeType)
            {
                case "iMet-1":
                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        loadCoefToSonde(ValidSondeArryiMet1[i].getTrackingID());
                    }

                    break;

                case "Bell202":
                    for (int i = 0; i < ValidSondeArrayiMet1U.Length; i++)
                    {
                        updateMainStatusLabel("Loading coef's to: " + ValidSondeArrayiMet1U[i].CalData.SerialNumber);
                        try
                        {
                            loadCoefToSonde(ValidSondeArrayiMet1U[i].CalData.SerialNumber);
                            logProgramEvent("Loaded coef's for: " + ValidSondeArrayiMet1U[i].CalData.SerialNumber);
                        }
                        catch
                        {
                            updateMainStatusLabel("Unable to load: " + ValidSondeArrayiMet1U[i].CalData.SerialNumber);
                            logProgramEvent("Unable to load: " + ValidSondeArrayiMet1U[i].CalData.SerialNumber);
                        }
                    }

                    break;
            }
        }

        public void moveCalToStorage()
        {
            //All the radiosonde cals in the current calibration location.
            string[] files = System.IO.Directory.GetFiles(CalibrationStatus.currentOverallLog, "*.u_cal");
            
            //Getting the file name.
            string[] fileName = new string[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                string[] fileBreakdown = files[i].Split('\\');
                fileName[i] = fileBreakdown[fileBreakdown.Length - 1];
            }

            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                logProgramEvent("Network Connection Detected");

                if (System.IO.Directory.Exists(ProgramSettings.CoefLOC))
                {
                    logProgramEvent("Network Location Found");

                    for (int i = 0; i < files.Length; i++)
                    {
                        try
                        {
                            logProgramEvent("Coping " + fileName[i] + " from " + files[i] + " to " + ProgramSettings.CoefLOC + "\\" + fileName[i]);
                            
                            //If the file exsist then ask if you want to override
                            if (System.IO.File.Exists(ProgramSettings.CoefLOC + "\\" + fileName[i]))
                            {
                                DialogResult overrideFile = MessageBox.Show(fileName[i] + " coeffient file found overwrite?", "Overwrite?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                                if (overrideFile == DialogResult.Yes)
                                {
                                    System.IO.File.Copy(files[i], ProgramSettings.CoefLOC + "\\" + fileName[i],true);
                                }
                            }
                            else
                            {
                                System.IO.File.Copy(files[i], ProgramSettings.CoefLOC + "\\" + fileName[i]);
                            }
                        }
                        catch(Exception e)
                        {
                            logProgramEvent("Error coping " + fileName[i] + "," + e.Message);
                        }
                    }
                }
                else
                {
                    logProgramEvent("Network Location Not Found");
                }
            }
        }

        /// <summary>
        /// Method produces the final report generated after the calibration as completed.
        /// </summary>
        /// <param name="dirLoc"></param>
        public void calibrationReport(string dirLoc)
        {
            //Making sure the dirLoc is formatted corrctly.
            if (dirLoc.Substring(dirLoc.Length - 1, 1) != "\\")
            {
                dirLoc = dirLoc + "\\";
            }

            //Final Report building storage
            List<string> finalReportElement = new List<string>();
            string[] rawProformanceFile = System.IO.Directory.GetFiles(dirLoc, "*.u_rpf");

            string reportFile = dirLoc.Substring(0, dirLoc.Length - 1) + "-Coefficient Log.csv";

            double passNumber = .8;

            foreach (string sonde in rawProformanceFile)
            {
                string acctableDataPercent = "";
                string sondeID = "";

                string[] tempFileIn = System.IO.File.ReadAllLines(sonde);

                //Getting sonde ID
                string[] sondeIDElement = tempFileIn[0].Split('_');
                sondeID = sondeIDElement[1].Substring(0, sondeIDElement[1].Length - 2);

                //Getting amount of good data
                string[] acctableDataPrecentElement = tempFileIn[1].Split('=');
                acctableDataPercent = acctableDataPrecentElement[1];

                double airTempPrecent = 0;
                double humidityCount = 0;
                double internalTemp = 0;

                //Loading all the data into array.
                bool[,] proformaceData = new bool[tempFileIn.Length - 4, 3];
                for (int i = 0; i < (proformaceData.Length/3)-5; i++)
                {
                    string[] dataIn = tempFileIn[i + 5].Split(',');
                    if (Convert.ToBoolean(dataIn[2]) == true) { airTempPrecent++; }
                    if (Convert.ToBoolean(dataIn[3]) == true) { humidityCount++; }
                    if (Convert.ToBoolean(dataIn[4]) == true) { internalTemp++; }

                }
                //Determing what passed or failed.
                bool AT = false;
                bool UC = false;
                bool IT = false;
                bool overallPF = false;

                if (airTempPrecent / ((proformaceData.Length / 3) -5) > passNumber) { AT = true; }
                if (humidityCount / ((proformaceData.Length / 3) - 5) > passNumber) { UC = true; }
                if (internalTemp / ((proformaceData.Length / 3) - 5) > passNumber) { IT = true; }
                if (AT && UC && IT) { overallPF = true; }



                finalReportElement.Add(sondeID + "\t" + overallPF.ToString() + "\t" + AT.ToString() + "," + (airTempPrecent / ((proformaceData.Length / 3) - 5) * 100).ToString("0.0") + "%" + "\t" +
                                                                                       UC.ToString() + "," + (humidityCount / ((proformaceData.Length / 3) - 5) * 100).ToString("0.0") + "%" + "\t" +
                                                                                       IT.ToString() + "," + (internalTemp / ((proformaceData.Length / 3) - 5 )* 100).ToString("0.0") + "%");

            }


            //Procenting the report

            string tempOutputReport = "";
            foreach( string item in finalReportElement)
            {
                tempOutputReport = tempOutputReport + item + "\n";
            }

            DialogResult outputReport = MessageBox.Show("Radiosonde Calibration Report.\nID\tPass\tAT\tUC\tIT\n" + tempOutputReport + "Would you like to output report?","Calibration Report",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if (outputReport == DialogResult.Yes)
            {
                string outPutFile = dirLoc + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "-Reprot.csv";
                TextWriter reportMaker = File.AppendText(outPutFile);
                reportMaker.WriteLine("Report for calibration at: " + dirLoc);
                reportMaker.WriteLine("");
                reportMaker.WriteLine("SondeID,Pass/Fail,AT,,UC,,IT");
                foreach (string item in finalReportElement)
                {
                    //Converting the line for csv format.
                    string[] tempBreakDown = item.Split('\t');
                    string totalCSVOutput = "";
                    for (int i = 0; i < tempBreakDown.Length; i++)
                    {
                        totalCSVOutput = totalCSVOutput + tempBreakDown[i] + ",";
                    }
                    reportMaker.WriteLine(totalCSVOutput);
                    
                }
                reportMaker.Close();
                System.Diagnostics.Process.Start(outPutFile);
            }

        }

        public void commandChamber(double desiredAirTemp)
        {
            CommunicationControl.commControlChamber = false;
            System.Threading.Thread.Sleep(1000);
            switch (ProgramSettings.ChamberController)
            {
                case "Watlow":
                    chamberWatlow.setAirTemp(desiredAirTemp);
                    break;

                case "Thermotron":
                    if (desiredAirTemp < 6) //If the temp is set to low shutting down the humidity gen.
                    {
                        chamberThermotron.setOptions(64);
                    }
                    chamberThermotron.setChamberTemp(desiredAirTemp);
                    chamberThermotron.startChamberManualMode();
                    break;
            }
            System.Threading.Thread.Sleep(1000);
            CommunicationControl.commControlChamber = true;

        }

        /// <summary>
        /// Method takes in the desire enclouser air temp and return the chamber temp that must be set to achive that desired air temp.
        /// </summary>
        /// <param name="desiredEnclouserTemp"></param>
        /// <returns></returns>
        public double correctChamberAirTempNeeded(double desiredEnclouserTemp)
        {
            return (ProgramSettings.coeffcientsChamber[2] * Math.Pow(desiredEnclouserTemp, 2) + ProgramSettings.coeffcientsChamber[1] * desiredEnclouserTemp + ProgramSettings.coeffcientsChamber[0]);
        }

        #endregion

        /* 
        //Background worker for enviroment statblility checking
        public BackgroundWorker backgroundWorkerEnviromentStability;
        */
        #region Methods to check calibration equiptment status, stabliity, health, and more.

        public void startCalibrationEviromentStability()
        {
            backgroundWorkerEnviromentStability = new BackgroundWorker();
            backgroundWorkerEnviromentStability.WorkerSupportsCancellation = true;
            backgroundWorkerEnviromentStability.DoWork += new DoWorkEventHandler(backgroundWorkerEnviromentStability_DoWork);
            backgroundWorkerEnviromentStability.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerEnviromentStability_RunWorkerCompleted);
            backgroundWorkerEnviromentStability.RunWorkerAsync();
        }

        public void stopCalibrationEviromentStability()
        {
            backgroundWorkerEnviromentStability.CancelAsync();
        }

        public void updateEnviromentStabilyElements()
        {
            if (ProgramSettings.calibrationSetPoint.Length/22 > CalibrationStatus.currentCalibrationPoint)
            {

                //Remove all elements form the array.
                calibrationEnviroment.getElementsArrayList().Clear();

                //Stability time before calibration will happen.
                int checkStabilityMin = 0;

                try
                {
                    checkStabilityMin = Convert.ToInt16(Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 7]) / 60);
                }
                catch
                {
                    checkStabilityMin = Convert.ToInt16(Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint - 1, 7]) / 60);
                }


                //updateing the elements
                calibrationEnviroment.addEnviromentElement("chamberAirTemp", //Name
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 4]) - Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 5]), //Setting lower acceptable level
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 4]) + Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 5]), //Setting upper acceptable lvel
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 4]), //Setting Set Point
                                                            checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                calibrationEnviroment.addEnviromentElement("chamberAirTempRate", //Name
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 6]) - Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 6]) * 2, //Setting lower acceptable level
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 6]), //Setting upper acceptable lvel
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 6]), //Setting Set Point
                                                            checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                calibrationEnviroment.addEnviromentElement("enclouserAirTemp", //Name
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 1]) - Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 2]), //Setting lower acceptable level
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 1]) + Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 2]), //Setting upper acceptable lvel
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 1]), //Setting Set Point
                                                            checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                calibrationEnviroment.addEnviromentElement("enclouserAirTempRate", //Name
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 3]) - Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 3]) * 2, //Setting lower acceptable level
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 3]), //Setting upper acceptable lvel
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 3]), //Setting Set Point
                                                            checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                calibrationEnviroment.addEnviromentElement("enclouserHumidity", //Name
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 8]) - Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 9]), //Setting lower acceptable level
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 8]) + Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 9]), //Setting upper acceptable lvel
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 8]), //Setting Set Point
                                                            checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                calibrationEnviroment.addEnviromentElement("enclouserHumidityRate", //Name
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 10]) - Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 10]) * 2, //Setting lower acceptable level
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 10]), //Setting upper acceptable lvel
                                                            Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 10]), //Setting Set Point
                                                            checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                switch (ProgramSettings.humiditySource)
                {
                    case "Thunder":

                        calibrationEnviroment.addEnviromentElement("thunderSatPressure", //Name
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 12]) - 1, //Setting lower acceptable level
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 12]) + 1, //Setting upper acceptable lvel
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 12]), //Setting Set Point
                                                                    checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                        calibrationEnviroment.addEnviromentElement("thunderSatTemp", //Name
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 13]) - 1, //Setting lower acceptable level
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 13]) + 1, //Setting upper acceptable lvel
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 13]), //Setting Set Point
                                                                    checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable.

                        calibrationEnviroment.addEnviromentElement("thunderFlowRate", //Name
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 14]) - 1, //Setting lower acceptable level
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 14]) + 1, //Setting upper acceptable lvel
                                                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 14]), //Setting Set Point
                                                                    checkStabilityMin * 240); //Setting the amount of points reqired to be true to be stable
                        break;

                    case "Thermotron":
                        break;
                }
            }
        }

        void backgroundWorkerEnviromentStability_DoWork(object sender, DoWorkEventArgs e)
        {
            if (CalibrationStatus.currentProgramMode == "running")
            {
                //This is the brain of the calibration. This background worker makes desisions on stability, and adjustments
                //to chamber settings.

                Int32 counter = 0;

                int localCurrentCalibrationPoint = CalibrationStatus.currentCalibrationPoint;

                System.Threading.Thread.CurrentThread.IsBackground = true;

                //Setting up eviromental stability checking object
                //calibrationEnviroment = new stabilityEnviroment();

                //Setting up the elements of the enviroment
                updateEnviromentStabilyElements();

                //Timer to make sure the chmaber setting is sent more then once per error.
                DateTime lastTimeRan = DateTime.Now;
                
                //Starting loop that will check the elements and handle changes like preping the Thunder and setting the corect chamber set point.
                while (!backgroundWorkerEnviromentStability.CancellationPending)
                {
                    if (CalibrationStatus.currentProgramMode != "running")
                    {
                        break;
                    }

                    //If the calibrationg point doesn't match the current assablised point then clear and reset.
                    if (localCurrentCalibrationPoint != CalibrationStatus.currentCalibrationPoint)
                    {
                        updateEnviromentStabilyElements();
                        localCurrentCalibrationPoint = CalibrationStatus.currentCalibrationPoint;
                    }

                    //Collecting enviromental conditions.
                    double[] currentChamberData = getChamberData(); //0 = AT 1 = humidity
                    double currentChamberATRate = CalibrationStatus.currentDiffChamberTemp;
                    double[] currentEnclosureData = getReferanceData(ProgramSettings.referanceCorrectionMode); //0 = AT 1 = humidity
                    double currentEnclosureATRate = CalibrationStatus.currentDiffReferanceTemp;
                    double currentEnclosureHumidityRate = CalibrationStatus.currentDiffReferanceHumidity;
                    //double currentThunderSatPressure = referanceThunder.setPointSaturPSI;

                    double currentThunderSatPressure = 0;
                    double currentThunderSatTemp = 0;
                    double currentThunderFlowRate = 0;

                    switch (ProgramSettings.humiditySource)
                    {
                        case "Thunder":
                            currentThunderSatPressure = genThunder.setPointData.setPointSaturPSI;
                            //double currentThunderSatTemp = referanceThunder.setPointSaturC;
                            currentThunderSatTemp = genThunder.setPointData.setPointSaturC;
                            //double currentThunderFlowRate = referanceThunder.setPointFlowRate;
                            currentThunderFlowRate = genThunder.setPointData.setPointFlowRate;
                            break;

                        case "Thermotron":
                            break;
                    }
                    //Calculating current enclouser differance for both AT and U
                    double currentEnclosureATDifferance = 999;
                    double currentEnclosureHumidityDifferance = 999;

                    foreach (elementStabilityEnviroment.elementStabilityEnviroment element in calibrationEnviroment.getElementsArrayList())
                    {
                        if (element.getName() == "enclouserAirTemp")
                        {
                            currentEnclosureATDifferance = currentEnclosureData[0] - element.getSetPoint();
                        }

                        if (element.getName() == "enclouserHumidity")
                        {
                            currentEnclosureHumidityDifferance = currentEnclosureData[1] - element.getSetPoint();
                        }
                    }


                    //Checking elements to see they meet the required eviroment needs to calibrate.
                    bool reportChamber = calibrationEnviroment.checkEnviromentElement("chamberAirTemp", currentChamberData[0]);
                    bool reportChamberAirTempRate = calibrationEnviroment.checkEnviromentElement("chamberAirTempRate", currentChamberATRate);
                    bool reportEnclosure = calibrationEnviroment.checkEnviromentElement("enclouserAirTemp", currentEnclosureData[0]);
                    bool reportEnclosureAirTempRate = calibrationEnviroment.checkEnviromentElement("enclouserAirTempRate", currentEnclosureATRate);
                    bool reportEnclosureHumidity = calibrationEnviroment.checkEnviromentElement("enclouserHumidity", currentEnclosureData[1]);
                    bool reportEnclosureHumidityRate = calibrationEnviroment.checkEnviromentElement("enclouserHumidityRate", currentEnclosureHumidityRate);
                    
                    //Creating the variables to represent the humidityGen.... This is to  set things up..
                    bool reportThunderFlowRate = false;

                    switch (ProgramSettings.humiditySource)
                    {
                        case "Thunder":
                            bool reportThunderSatPressure = calibrationEnviroment.checkEnviromentElement("thunderSatPressure", currentThunderSatPressure);
                            bool reprotThunderSatTemp = calibrationEnviroment.checkEnviromentElement("thunderSatTemp", currentThunderSatTemp);
                            reportThunderFlowRate = calibrationEnviroment.checkEnviromentElement("thunderFlowRate", currentThunderFlowRate);
                            break;

                        case "Thermotron":
                            reportThunderFlowRate = true;
                            break;

                    }
                    //Calculating the total readiness of a piece of equipment.
                    double totalChamberStatus = 0;
                    double totalEnclouserStatus = 0;
                    double totalThunderStatus = 0;

                    foreach (elementStabilityEnviroment.elementStabilityEnviroment element in calibrationEnviroment.getElementsArrayList())
                    {
                        //Chamber elements (2 items)
                        if (element.getName() == "chamberAirTemp") { totalChamberStatus += Convert.ToDouble(element.getReferancePointCount()); }
                        if (element.getName() == "chamberAirTempRate") { totalChamberStatus += Convert.ToDouble(element.getReferancePointCount()); }

                        //Enclouser elements (4 items)
                        if (element.getName() == "enclouserAirTemp") { totalEnclouserStatus += Convert.ToDouble(element.getReferancePointCount()); }
                        if (element.getName() == "enclouserAirTempRate") { totalEnclouserStatus += Convert.ToDouble(element.getReferancePointCount()); }
                        if (element.getName() == "enclouserHumidity") { totalEnclouserStatus += Convert.ToDouble(element.getReferancePointCount()); }
                        if (element.getName() == "enclouserHumidityRate") { totalEnclouserStatus += Convert.ToDouble(element.getReferancePointCount()); }

                        switch (ProgramSettings.humiditySource)
                        {
                            case "Thunder":
                                //Thunder elements (3 items)
                                if (element.getName() == "thunderSatPressure") { totalThunderStatus += Convert.ToDouble(element.getReferancePointCount()); }
                                if (element.getName() == "thunderSatTemp") { totalThunderStatus += Convert.ToDouble(element.getReferancePointCount()); }
                                if (element.getName() == "thunderFlowRate") { totalThunderStatus += Convert.ToDouble(element.getReferancePointCount()); }
                                break;

                            case "Thrmotron":
                                break;
                        }
                    }

                    //Getting the average.
                    totalChamberStatus = totalChamberStatus / 2;
                    totalEnclouserStatus = totalEnclouserStatus / 4;
                    totalThunderStatus = totalThunderStatus / 3;


                    //Updating screen
                    try
                    {

                        BeginInvoke(new updateCalibrationStatusDel(updateCalibrationStatus), new object[] 
                        { 

                            calibrationEnviroment.getElementSetPoint("enclouserAirTemp"),
                            calibrationEnviroment.getElementSetPoint("enclouserHumidity"),
                            currentEnclosureData[0],
                            currentEnclosureData[1],
                            currentEnclosureATDifferance,
                            currentEnclosureHumidityDifferance,
                            reportChamber,
                            totalChamberStatus,
                            reportEnclosure,
                            totalEnclouserStatus,
                            reportThunderFlowRate,
                            totalThunderStatus                        
                        });

                    }
                    catch { }

                    //Checking to correct chamber setpoint to real setpoint if internal enclouser is at the high end of the desired range.
                    double currentAETR = 9999;
                    try
                    {
                        currentAETR = Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 2]);
                    }
                    catch
                    {
                        currentAETR = Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint - 1, 2]);
                    }


                    if (currentEnclosureATDifferance < currentAETR &&
                        getChamberATSetPoint() != calibrationEnviroment.getElementSetPoint("chamberAirTemp"))
                    {
                        TimeSpan timeSinceLastChammberCommand = DateTime.Now - lastTimeRan;
                        if (timeSinceLastChammberCommand.TotalMinutes > 1)
                        {
                            setChamberATSetPoint(calibrationEnviroment.getElementSetPoint("chamberAirTemp"));
                            lastTimeRan = DateTime.Now;
                        }
                    }

                    //Checking elements to see if the enviroment is ready for calibration.
                    int currentStabilityTime = 99999;
                    try
                    {
                        currentStabilityTime = Convert.ToInt16(Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 7]));
                    }
                    catch
                    {
                        currentStabilityTime = Convert.ToInt16(Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint - 1, 7]));
                    }

                    int itemsReady = 0;
                    foreach (elementStabilityEnviroment.elementStabilityEnviroment element in calibrationEnviroment.getElementsArrayList())
                    {
                        if (element.getReferancePointCount() == (currentStabilityTime / 60) * 240)
                        {
                            itemsReady++;
                        }
                    }

                    if (itemsReady == calibrationEnviroment.enviromentElements.Count)
                    {
                        CalibrationStatus.okToStartCollectingData = true;
                    }

                    //counter++;
                    //updateMainStatusLabel(counter.ToString());

                    System.Threading.Thread.Sleep(250);
                }
            }

            System.Threading.Thread.Sleep(3000); //Think this might be causing the window to close.
            
        }

        void backgroundWorkerEnviromentStability_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //A little pause to allow setting to be changed by other parts of the program. This is done if the calibration has finished to provent this thread from
            //restarting when not needed.
            System.Threading.Thread.Sleep(5000);

            //The the checking thread goes down this will alert that something has gone wrong and will then restart this thread if the calibration is still running.
            if (CalibrationStatus.statusCalibrationInProcess && CalibrationStatus.currentProgramMode == "running")
            {
                updateMainStatusLabel("Enviroment Stability thread has crashed. Restarting...");
                logProgramEvent("Enviroment Stability thread has crashed. Restarting...");
            }
            else
            {
                updateMainStatusLabel("Enviroment Stability thread has shutdown.");
                logProgramEvent("Enviroment Stability thread has shutdown.");

                //Reseting Calibration Status section of the program.
                BeginInvoke(new sendDataToScreen(this.resetCalibrationStatus));
            }
        }

        #endregion

        #endregion

        #region Methods for extrnal control of updated data.

        public void setCommunicationControls(bool current)
        {
            CommunicationControl.commControlChamber = current;
            CommunicationControl.commControlHunmidityGen = current;
            CommunicationControl.commControlRadiosonde = current;
        }

        #endregion

        #region Section for creating data file pertaining to radiosonde data and sensor data.

        private void logValidRadiosondeData()
        {
            //Getting the full logging date and setting up veriables for the at and U.
            string fullDataAndTime = DateTime.Now.ToString("ddd MMM dd HH:mm:ss yyyy");
            double currentAirTemp = 0;
            double currentHumidity = 0;

            //getting current refreance data.
            switch (ProgramSettings.calibrationReferanceSensor)
            {
                case "HMP234":
                    currentAirTemp = referanceHMP234.CurrentTemp;
                    currentHumidity = referanceHMP234.CurrentHumidity;
                    break;

                case "EPlusE":

                    //Getting the data from the referance sensor
                    double[] refData = getReferanceData(false);

                    //Sorting the data out.
                    currentAirTemp = refData[0];
                    currentHumidity = refData[1];
                    break;
            }


            switch (ProgramSettings.radiosondeType)
            {
                case "iMet-1":
                    #region Logging for Gary's data for iMet-1 radiosondes.

                    if (File.Exists(CalibrationStatus.currentOverallLog + ".csv") == false)
                    {
                        // create a writer and open the file
                        TextWriter currentLoggingCheck = File.AppendText(CalibrationStatus.currentOverallLog + ".csv");


                        // write a line of text to the file
                        currentLoggingCheck.WriteLine("TimeStamp(sec),SerialNum,TrackingNum,TUProbeNum,FirmwareVer,Pressure(hPa),PressCnt,PressRefCnt,PressTemp(degC),PressTempCnt,PressTempRefCnt,AirTemp(degC),AirTempCnt,AirTempRefCnt,InternTemp(degC),InternTempCnt,InternTempRefCnt,Humidity(RH),HumCnt,TransHumidity(RH),HumTemp(degC),HumTempCnt,HumTempRefCnt,BattVolt(V),BattVoltCnt,BattVoltRefCnt,AmbPressure,AmbTemperature,AmbHumidity,PressureDiff,TemperatureDiff,HumidityDiff,");

                        // close the stream
                        currentLoggingCheck.Close();

                    }

                    // create a writer and open the file
                    TextWriter currentLogging = File.AppendText(CalibrationStatus.currentOverallLog + ".csv");

                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        //Note i am not writing pressure data..... might want to think about that..... This code will need to be edited later if the two need to be run at the same time.
                        // write a line of text to the file
                        currentLogging.WriteLine(fullDataAndTime + "," + ValidSondeArryiMet1[i].getSerialNum() + "," + ValidSondeArryiMet1[i].getTrackingID() + "," +
                            ValidSondeArryiMet1[i].getTUProbeID() + "," + ValidSondeArryiMet1[i].getFirmwareVersion() + "," +
                            ValidSondeArryiMet1[i].getPressure() + "," + ValidSondeArryiMet1[i].getPressureCount() + "," +
                            ValidSondeArryiMet1[i].getPressureRefCount() + "," + ValidSondeArryiMet1[i].getPressureTemp() + "," +
                            ValidSondeArryiMet1[i].getPressureTempCount() + "," + ValidSondeArryiMet1[i].getPressureTempRefCount() + "," +
                            ValidSondeArryiMet1[i].getAirTemp() + "," + ValidSondeArryiMet1[i].getAirTempCount() + "," +
                            ValidSondeArryiMet1[i].getAirTempRefCount() + "," + ValidSondeArryiMet1[i].getInternalTemp() + "," +
                            ValidSondeArryiMet1[i].getInternalTempCount() + "," + ValidSondeArryiMet1[i].getInternalTempRefCount() + "," +
                            ValidSondeArryiMet1[i].getRH() + "," + ValidSondeArryiMet1[i].getRHFreqEPlusE() + "," +
                            ValidSondeArryiMet1[i].getRHTranslated() + "," + ValidSondeArryiMet1[i].getHumidityTemp() + "," +
                            ValidSondeArryiMet1[i].getHumidityTempCount() + "," + ValidSondeArryiMet1[i].getHumidityTempRefCount() + "," +
                            ValidSondeArryiMet1[i].getBatteryVoltage() + "," + ValidSondeArryiMet1[i].getBatteryVoltageCount() + "," +
                            ValidSondeArryiMet1[i].getBatteryVoltageRefCount() + "," + "0" + "," +
                            currentAirTemp.ToString() + "," + currentHumidity.ToString() + "," +
                            (Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - Convert.ToDouble(0)).ToString() + "," +
                            (Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentAirTemp).ToString() + "," +
                            (Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity).ToString() + ","
                            );

                    }
                    // close the stream
                    currentLogging.Close();

                    #endregion

                    break;

                case "Bell202":

                    if (File.Exists(CalibrationStatus.currentOverallLog + ".csv") == false)
                    {
                        // create a writer and open the file
                        TextWriter currentLoggingCheck = File.AppendText(CalibrationStatus.currentOverallLog + ".csv");


                        // write a line of text to the file
                        currentLoggingCheck.WriteLine("TimeStamp(sec),SerialNum,TrackingNum,TUProbeNum,FirmwareVer,Pressure(hPa),PressCnt,PressRefCnt,PressTemp(degC),PressTempCnt,PressTempRefCnt,AirTemp(degC),AirTempCnt,AirTempRefCnt,InternTemp(degC),InternTempCnt,InternTempRefCnt,Humidity(RH),HumCnt,TransHumidity(RH),HumTemp(degC),HumTempCnt,HumTempRefCnt,BattVolt(V),BattVoltCnt,BattVoltRefCnt,AmbPressure,AmbTemperature,AmbHumidity,PressureDiff,TemperatureDiff,HumidityDiff,");

                        // close the stream
                        currentLoggingCheck.Close();

                    }

                    // create a writer and open the file
                    TextWriter currentBell202Logging = File.AppendText(CalibrationStatus.currentOverallLog + ".csv");

                    for (int i = 0; i < ValidSondeArrayiMet1U.Length; i++)
                    {
                        //Note i am not writing pressure data..... might want to think about that..... This code will need to be edited later if the two need to be run at the same time.
                        // write a line of text to the file
                        currentBell202Logging.WriteLine(fullDataAndTime + "," + ValidSondeArrayiMet1U[i].CalData.SondeID + "," +ValidSondeArrayiMet1U[i].CalData.SerialNumber + "," +
                            ValidSondeArrayiMet1U[i].CalData.ProbeID + "," + ValidSondeArrayiMet1U[i].FirmwareVersion + "," +
                            ValidSondeArrayiMet1U[i].CalData.Pressure + "," + ValidSondeArrayiMet1U[i].CalData.PressureCounts + "," +
                            ValidSondeArrayiMet1U[i].CalData.PressureRefCounts + "," + ValidSondeArrayiMet1U[i].CalData.PressureTemperature + "," +
                            ValidSondeArrayiMet1U[i].CalData.PressureTempCounts + "," + ValidSondeArrayiMet1U[i].CalData.PressureTempRefCounts + "," +
                            ValidSondeArrayiMet1U[i].CalData.Temperature + "," + ValidSondeArrayiMet1U[i].CalData.TemperatureCounts + "," +
                            ValidSondeArrayiMet1U[i].CalData.TemperatureRefCounts + "," + ValidSondeArrayiMet1U[i].CalData.InternalTemp + "," +
                            "0" + "," + "0" + "," + //Internal air temp count + Internal Air Temp Count Ref
                            ValidSondeArrayiMet1U[i].CalData.Humidity + "," + ValidSondeArrayiMet1U[i].CalData.HumidityFrequency + "," +
                            "0" + "," + "0" + "," + // ValidSondeArryiMet1[i].getRHTranslated() + "," + ValidSondeArryiMet1[i].getHumidityTemp() + "," +
                            "0" + "," + "0" + "," + // ValidSondeArryiMet1[i].getHumidityTempCount() + "," + ValidSondeArryiMet1[i].getHumidityTempRefCount() + "," +
                            "5" + "," + "0" + "," + // ValidSondeArryiMet1[i].getBatteryVoltage() + "," + ValidSondeArryiMet1[i].getBatteryVoltageCount() + "," +
                            "0" + "," + "0" + "," + // ValidSondeArryiMet1[i].getBatteryVoltageRefCount() + "," + "0" + "," +
                            currentAirTemp.ToString() + "," + currentHumidity.ToString() + "," +
                            (Convert.ToDouble(ValidSondeArrayiMet1U[i].CalData.Pressure) - Convert.ToDouble(0)).ToString() + "," +
                            (Convert.ToDouble(ValidSondeArrayiMet1U[i].CalData.Temperature) - currentAirTemp).ToString() + "," +
                            (Convert.ToDouble(ValidSondeArrayiMet1U[i].CalData.Humidity) - currentHumidity).ToString() + ","
                            );

                    }
                    // close the stream
                    currentBell202Logging.Close();


                    break;

            }
        }

        public void logRefData()
        {
            double currentAirTemp = 0;
            double currentHumidity = 0;
            DateTime currentTime = DateTime.Now;
            TimeSpan elapseTime = currentTime - CalibrationStatus.calibrationStartTime;
            string currentLogFileName = CalibrationStatus.currentOverallLog + "\\" + ProgramSettings.calibrationReferanceSensor + ".u_ref";

            //Getting current referance data
            double[] currentRefferanceData = getReferanceData(ProgramSettings.referanceCorrectionMode);
            currentAirTemp = currentRefferanceData[0];
            currentHumidity = currentRefferanceData[1];

            if (File.Exists(currentLogFileName) == false)
            {
                // create a writer and open the file
                TextWriter currentLoggingCheck = File.AppendText(currentLogFileName);


                // write a line of text to the file
                currentLoggingCheck.WriteLine("Humidity Calibration Process - Raw Humidity Data");
                currentLoggingCheck.WriteLine("");
                currentLoggingCheck.WriteLine("Date Time" + CalibrationStatus.calibrationStartTime.ToString("yyyy-MM-dd") + "\t" + CalibrationStatus.calibrationStartTime.ToString("HH:mm:ss"));
                currentLoggingCheck.WriteLine("");
                currentLoggingCheck.WriteLine("TimeStamp(sec),Temp(°C),Humidity(%RH)");

                // close the stream
                currentLoggingCheck.Close();

            }

            // create a writer and open the file    
            TextWriter currentLogging = File.AppendText(currentLogFileName);

            currentLogging.WriteLine(elapseTime.TotalSeconds.ToString("0.000") + "," +
                currentAirTemp.ToString("0.000") + "," +
                currentHumidity.ToString("0.000"));

            //Closing the streem.
            currentLogging.Close();
            //Log data to a file here.
        }

        /// <summary>
        /// Method for checking to make sure that the sonde is giving good data.
        /// Returns true if it matchs something close to what is expected.
        /// </summary>
        /// <param name="indexValidRadiosonde"></param>
        /// <returns></returns>
        public bool checkSondeDataValid(int indexValidRadiosonde)
        {
            return true;

            //Stopping this check as it doesn't conform to the new requirments.
            /*
            bool[] dataState = new bool[1];
            bool returnedState = false;
            
            //Checking for valid tracking number.
            if (ValidSondeArryiMet1[indexValidRadiosonde].getTrackingID().Substring(0, 1).ToUpper() == "S")
            {
                dataState[0] = true;
            }

            //Final check for pass/fail. This checks all pass critera and returns false if even one in the array is false.
            for (int i = 0; i < dataState.Length; i++)
            {
                if (dataState[i])
                {
                    returnedState = true;
                }
                else
                {
                    return false;
                }
            }
            return returnedState;
             */ 
        }

        public void logSondeData(int indexVaildRadiosond)
        {
            DateTime currentTime = DateTime.Now;
            TimeSpan elapseTime = currentTime - CalibrationStatus.calibrationStartTime;

            switch (ProgramSettings.radiosondeType)
            {
                case "iMet-1":
                    #region Logging for the iMet-1 radiosonde. This creates the data used in Justin's calibration process.

                    //Checking to make sure the id of the radiosonde data is good before logging it.
                    if (checkSondeDataValid(indexVaildRadiosond))
                    {

                        string currentLogFileName = CalibrationStatus.currentOverallLog + "\\_" + ValidSondeArryiMet1[indexVaildRadiosond].getTrackingID() + ".u_raw";

                        if (File.Exists(currentLogFileName) == false)
                        {
                            // create a writer and open the file
                            TextWriter currentLoggingCheck = File.AppendText(currentLogFileName);


                            // write a line of text to the file
                            currentLoggingCheck.WriteLine("Humidity Calibration Process - Raw Radiosonde Data");
                            currentLoggingCheck.WriteLine("");
                            currentLoggingCheck.WriteLine("Date Time: " + CalibrationStatus.calibrationStartTime.ToString("yyyy-MM-dd") + "\t" + CalibrationStatus.calibrationStartTime.ToString("HH:mm:ss"));
                            currentLoggingCheck.WriteLine("");
                            currentLoggingCheck.WriteLine("TimeStamp(sec),HumFreq(Hz),AirTemp(°C),InternalTemp(°C)");

                            // close the stream
                            currentLoggingCheck.Close();

                        }

                        // create a writer and open the file
                        TextWriter currentLogging = File.AppendText(currentLogFileName);

                        currentLogging.WriteLine(elapseTime.TotalSeconds.ToString("0.000") + "," +
                            ValidSondeArryiMet1[indexVaildRadiosond].getRHFreqEPlusE() + "," +
                            ValidSondeArryiMet1[indexVaildRadiosond].getAirTemp() + "," +
                            ValidSondeArryiMet1[indexVaildRadiosond].getInternalTemp());

                        //Closing the streem.
                        currentLogging.Close();
                    }
                    else
                    {
                        updateCalibrationStatusLabel("Error is sonde data.");
                        logProgramEvent("Error in logging data. Bad sonde data.");
                    }

                    #endregion
                    break;

                case "Bell202":
                    #region Logging for bell202 radiosonde.
                    string currentBell202LogFileName = CalibrationStatus.currentOverallLog + "\\_" + ValidSondeArrayiMet1U[indexVaildRadiosond].CalData.SerialNumber + ".u_raw";

                    if (File.Exists(currentBell202LogFileName) == false)
                    {
                        // create a writer and open the file
                        TextWriter currentLoggingCheck = File.AppendText(currentBell202LogFileName);


                        // write a line of text to the file
                        currentLoggingCheck.WriteLine("Humidity Calibration Process - Raw Radiosonde Data");
                        currentLoggingCheck.WriteLine("");
                        currentLoggingCheck.WriteLine("Date Time: " + CalibrationStatus.calibrationStartTime.ToString("yyyy-MM-dd") + "\t" + CalibrationStatus.calibrationStartTime.ToString("HH:mm:ss"));
                        currentLoggingCheck.WriteLine("");
                        currentLoggingCheck.WriteLine("TimeStamp(sec),HumFreq(Hz),AirTemp(°C),InternalTemp(°C)");

                        // close the stream
                        currentLoggingCheck.Close();

                    }

                    // create a writer and open the file
                    TextWriter currentBell202Logging = File.AppendText(currentBell202LogFileName);

                    currentBell202Logging.WriteLine(elapseTime.TotalSeconds.ToString("0.000") + "," +
                        ValidSondeArrayiMet1U[indexVaildRadiosond].CalData.HumidityFrequency + "," +
                        ValidSondeArrayiMet1U[indexVaildRadiosond].CalData.Temperature + "," +
                        ValidSondeArrayiMet1U[indexVaildRadiosond].CalData.InternalTemp);

                    //Closing the streem.
                    currentBell202Logging.Close();
                    #endregion
                    break;
            }
        }

        public void logProgramEvent(string eventMessage)
        {
            DateTime currentTime = DateTime.Now;
            string currentLogFileName = CalibrationStatus.currentOverallLog + ".log";
            
            //Proventing the logging of data to a ".log" file.
            if (CalibrationStatus.currentOverallLog == "")
            {
                return;
            }

            if (File.Exists(currentLogFileName) == false)
            {
                // create a writer and open the file
                TextWriter currentLoggingCheck = File.AppendText(currentLogFileName);


                // write a line of text to the file
                currentLoggingCheck.WriteLine("Date,Time,Event");

                // close the stream
                currentLoggingCheck.Close();

            }

            // create a writer and open the file
            TextWriter currentLogging = File.AppendText(currentLogFileName);

            retryWrite:
            try
            {
                currentLogging.WriteLine(currentTime.ToString("yyyyMMdd") + "," + currentTime.ToString("HH:mm:s.ff") + "," + eventMessage);
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                goto retryWrite;
            }

            //Closing the streem.
            currentLogging.Close();
        }

        public void logRawRadiosondeCalPerformace(string radiosondeRawDataLOC, object[] data)
        {
            string rawProFileName = radiosondeRawDataLOC.Substring(0, radiosondeRawDataLOC.Length - 3) + "rpf";

            //Checking to see if log already exsist and if so delete it.
            if (System.IO.File.Exists(rawProFileName))
            {
                System.IO.File.Delete(rawProFileName);
            }

            //getting name for reprot
            string[] sondeNameElements = radiosondeRawDataLOC.Split('\\');
            
            //Getting the performace data from the data object.
            bool[,] resultsPassFail = (bool[,])data[1];

            //Writing log file
            TextWriter proLog = File.AppendText(rawProFileName);
            proLog.WriteLine("Proformance log for: " + sondeNameElements[sondeNameElements.Length - 1]);
            proLog.WriteLine("Amount of acceptable data = " + data[0].ToString());
            proLog.WriteLine("");
            proLog.WriteLine("Packet,AT,U Count,IT");

            for (int i = 0; i < resultsPassFail.Length / 3; i++)
            {
                proLog.WriteLine(i.ToString() + "," + resultsPassFail[i, 0].ToString() + "," + resultsPassFail[i, 1].ToString() + "," + resultsPassFail[i, 2]);
            }

            proLog.Close();
            
        }

        public void logRawRadiosondeCalPerformace2(string radiosondeRawDataLOC, object[] incoming)
        {
            string rawProFileName = radiosondeRawDataLOC.Substring(0, radiosondeRawDataLOC.Length - 3) + "rpf";

            //Checking to see if log already exsist and if so delete it.
            if (System.IO.File.Exists(rawProFileName))
            {
                System.IO.File.Delete(rawProFileName);
            }

            //getting name for reprot
            string[] sondeNameElements = radiosondeRawDataLOC.Split('\\');

            List<analyseCalData.reportData> data = (List<analyseCalData.reportData>)incoming[1];

            //Writing log file
            TextWriter proLog = File.AppendText(rawProFileName);
            proLog.WriteLine("Proformance log for: " + sondeNameElements[sondeNameElements.Length - 1]);
            proLog.WriteLine("Amount of acceptable data = " + incoming[0].ToString());
            proLog.WriteLine("");
            proLog.WriteLine("RefTime,SondeTime,AT,U Count,IT");

            for (int i = 0; i < data.Count; i++)
            {
                proLog.WriteLine(data[i].refTime.ToString("HH:mm:ss.ffff") + "," + data[i].sondeRawTime.ToString("HH:mm:ss.ffff") + "," +
                    data[i].airTemp.ToString() + "," + data[i].humidityFreq.ToString() + "," + data[i].internalTemp.ToString());
            }

            proLog.Close();

        }

        #endregion

        #region Methods for change rates

        #region Referance Sensors
        public void startReferanceDifferance()
        {
            backgroundWorkerReferanceDifferance = new BackgroundWorker();
            backgroundWorkerReferanceDifferance.DoWork += new DoWorkEventHandler(backgroundWorkerReferanceDifferance_DoWork);
            backgroundWorkerReferanceDifferance.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerReferanceDifferance_RunWorkerCompleted);
            backgroundWorkerReferanceDifferance.WorkerSupportsCancellation = true;
            backgroundWorkerReferanceDifferance.RunWorkerAsync();
        }

        public void stopReferanceDifferance()
        {
            backgroundWorkerReferanceDifferance.CancelAsync();
        }

        void backgroundWorkerReferanceDifferance_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime[] dataPacketTime = new DateTime[180];
            double[] dataPacketTemp = new double[180];
            double[] dataPacketHumidity = new double[180];

            int counterCurrentPacket = 0;

            for (int a = 0; a < dataPacketTime.Length; a++)
            {
                dataPacketTime[a] = DateTime.Now;
                dataPacketTemp[a] = 99.999;
                dataPacketHumidity[a] = 99.999;
            }

            while (!backgroundWorkerReferanceDifferance.CancellationPending)
            {
                refTUDataDone.WaitOne(); //Waiting for referance data to finish.
                DateTime currentDateTime = DateTime.Now;
                //Resetting counter if reached the end of the array.
                if (counterCurrentPacket == dataPacketTime.Length) { counterCurrentPacket = 0; }

                try
                {
                    double[] currentRefData = getReferanceData(ProgramSettings.referanceCorrectionMode);
                    dataPacketTime[counterCurrentPacket] = currentDateTime;
                    dataPacketTemp[counterCurrentPacket] = currentRefData[0];
                    dataPacketHumidity[counterCurrentPacket] = currentRefData[1];

                    /*
                    switch (ProgramSettings.calibrationReferanceSensor)
                    {
                        case "HMP234":
                            dataPacketTime[counterCurrentPacket] = currentDateTime;
                            dataPacketTemp[counterCurrentPacket] = referanceHMP234.CurrentTemp;
                            dataPacketHumidity[counterCurrentPacket] = referanceHMP234.CurrentHumidity;
                            break;

                        case "EPlusE":
                            dataPacketTime[counterCurrentPacket] = currentDateTime;
                            dataPacketTemp[counterCurrentPacket] = referanceHMP234.CurrentTemp;
                            dataPacketHumidity[counterCurrentPacket] = referanceHMP234.CurrentHumidity;
                            break;
                    }
                    */
                }
                catch
                {
                    logProgramEvent("Error doing referacne differances.");
                }

                int counterCloseTimePackets = 9000; // How many match?
                int indexOfTimeThatMatches = 0;

                int[] dataPointsPast = new int[15];
                int indexOfPastMatches = 0;
                int[] dataPointsCurrent = new int[15];
                int indexOfCurrentMatches = 0;

                for (int test = 0; test < dataPacketTime.Length; test++)
                {
                    for (int a = 0; a < dataPacketTime.Length; a++)
                    {
                        if (indexOfCurrentMatches < dataPointsCurrent.Length)
                        {
                            if (dataPacketTime[a] >= currentDateTime.AddMilliseconds(-600 - (1000 * test)) && dataPacketTime[a] <= currentDateTime.AddMilliseconds(+600 - (1000 * test)))
                            {
                                dataPointsCurrent[indexOfCurrentMatches] = a;
                                indexOfCurrentMatches++;
                            }
                        }
                    }

                    for (int b = 0; b < dataPacketTime.Length; b++)
                    {
                        if (indexOfPastMatches < dataPointsPast.Length)
                        {
                            if (dataPacketTime[b] >= currentDateTime.AddMilliseconds(-600 - (1000 * test)).AddMinutes(-1) && dataPacketTime[b] <= currentDateTime.AddMilliseconds(+600 - (1000 * test)).AddMinutes(-1))
                            {
                                dataPointsPast[indexOfPastMatches] = b;
                                indexOfPastMatches++;
                            }
                        }
                    }

                    if (dataPacketTime[test] >= currentDateTime.AddMilliseconds(-60500) && dataPacketTime[test] <= currentDateTime.AddMilliseconds(-59500))
                    {
                        indexOfTimeThatMatches = test;
                        counterCloseTimePackets++;
                    }
                }

                //if (counterCloseTimePackets != 9000)
                if (indexOfCurrentMatches == dataPointsCurrent.Length && indexOfPastMatches == dataPointsPast.Length)
                {
                    double boxCarDiffReferanceTempPast = 0;
                    double boxCarDiffReferanceTempCurrent = 0;
                    double boxCarDiffReferanceHumidityPast = 0;
                    double boxCarDiffReferanceHumidityCurrent = 0;

                    //Working the averages.
                    for (int x = 0; x < dataPointsCurrent.Length; x++)
                    {
                        //Adding up the ait temp totals.
                        boxCarDiffReferanceTempCurrent = boxCarDiffReferanceTempCurrent + dataPacketTemp[dataPointsCurrent[x]];
                        boxCarDiffReferanceTempPast = boxCarDiffReferanceTempPast + dataPacketTemp[dataPointsPast[x]];

                        //Humidity Totals.
                        boxCarDiffReferanceHumidityCurrent = boxCarDiffReferanceHumidityCurrent + dataPacketHumidity[dataPointsCurrent[x]];
                        boxCarDiffReferanceHumidityPast = boxCarDiffReferanceHumidityPast + dataPacketHumidity[dataPointsPast[x]];

                    }

                    boxCarDiffReferanceTempCurrent = boxCarDiffReferanceTempCurrent / dataPointsCurrent.Length;
                    boxCarDiffReferanceTempPast = boxCarDiffReferanceTempPast / dataPointsCurrent.Length;

                    boxCarDiffReferanceHumidityCurrent = boxCarDiffReferanceHumidityCurrent / dataPointsCurrent.Length;
                    boxCarDiffReferanceHumidityPast = boxCarDiffReferanceHumidityPast / dataPointsCurrent.Length;

                    CalibrationStatus.currentDiffReferanceTemp = boxCarDiffReferanceTempPast - boxCarDiffReferanceTempCurrent;
                    CalibrationStatus.currentDiffReferanceHumidity = boxCarDiffReferanceHumidityPast - boxCarDiffReferanceHumidityCurrent;

                    CalibrationStatus.currentRawDiffReferanceTemp = dataPacketTemp[indexOfTimeThatMatches] - dataPacketTemp[counterCurrentPacket];
                    CalibrationStatus.currentRawDiffReferanceHumidity = dataPacketHumidity[indexOfTimeThatMatches] - dataPacketHumidity[counterCurrentPacket];

                    try
                    {
                        BeginInvoke(new updateTUDiff(updateReferanceDifferance), new object[] { true });
                
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        BeginInvoke(new updateTUDiff(updateReferanceDifferance), new object[] { false });
                    }
                    catch { }
                }

                //Indexing counter to the next pack in the array.
                counterCurrentPacket++;
            }
        }

        void backgroundWorkerReferanceDifferance_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            updateMainStatusLabel("Referacne sensor differance stopped.");
        }
        #endregion

        #region Chamber Sensors
        public void startChamberDifferance()
        {
            backgroundWorkerChamberReferanceDifferance = new BackgroundWorker();
            backgroundWorkerChamberReferanceDifferance.DoWork += new DoWorkEventHandler(backgroundWorkerChamberReferanceDifferance_DoWork);
            backgroundWorkerChamberReferanceDifferance.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerChamberReferanceDifferance_RunWorkerCompleted);
            backgroundWorkerChamberReferanceDifferance.WorkerSupportsCancellation = true;
            backgroundWorkerChamberReferanceDifferance.RunWorkerAsync();
        }

        public void stopChamberDifferance()
        {
            backgroundWorkerChamberReferanceDifferance.CancelAsync();
        }

        void backgroundWorkerChamberReferanceDifferance_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime[] dataPacketTime = new DateTime[180];
            double[] dataPacketTemp = new double[180];
            double[] dataPacketHumidity = new double[180];

            int counterCurrentPacket = 0;

            //Setting all date time data to now.
            for (int a = 0; a < dataPacketTime.Length; a++)
            {
                dataPacketTime[a] = DateTime.Now;
            }

            while (!backgroundWorkerChamberReferanceDifferance.CancellationPending)
            {
                refChamberDataDone.WaitOne(); //Waiting for referance data to finish.
                DateTime currentDateTime = DateTime.Now;
                //Resetting counter if reached the end of the array.
                if (counterCurrentPacket == dataPacketTime.Length) { counterCurrentPacket = 0; }

                switch (ProgramSettings.ChamberController)
                {
                    case "Watlow":
                        dataPacketTime[counterCurrentPacket] = currentDateTime;
                        dataPacketTemp[counterCurrentPacket] = chamberWatlow.ProcessCurrentTemp(chamberWatlow.currentRawAirTemp);
                        break;

                    case "Thermotron":
                        dataPacketTime[counterCurrentPacket] = currentDateTime;
                        dataPacketTemp[counterCurrentPacket] = chamberThermotron.CurrentChamberC;
                        dataPacketHumidity[counterCurrentPacket] = chamberThermotron.CurrentChamberRH;
                        break;
                }

                int counterCloseTimePackets = 9000; // How many match?
                int indexOfTimeThatMatches = 0;

                int[] dataPointsPast = new int[15];
                int indexOfPastMatches = 0;
                int[] dataPointsCurrent = new int[15];
                int indexOfCurrentMatches = 0;

                for (int test = 0; test < dataPacketTime.Length; test++)
                {
                    for (int a = 0; a < dataPacketTime.Length; a++)
                    {
                        if (indexOfCurrentMatches < dataPointsCurrent.Length)
                        {
                            if (dataPacketTime[a] >= currentDateTime.AddMilliseconds(-600 - (1000 * test)) && dataPacketTime[a] <= currentDateTime.AddMilliseconds(+600 - (1000 * test)))
                            {
                                dataPointsCurrent[indexOfCurrentMatches] = a;
                                indexOfCurrentMatches++;
                            }
                        }
                    }

                    for (int b = 0; b < dataPacketTime.Length; b++)
                    {
                        if (indexOfPastMatches < dataPointsPast.Length)
                        {
                            if (dataPacketTime[b] >= currentDateTime.AddMilliseconds(-600 - (1000 * test)).AddMinutes(-1) && dataPacketTime[b] <= currentDateTime.AddMilliseconds(+600 - (1000 * test)).AddMinutes(-1))
                            {
                                dataPointsPast[indexOfPastMatches] = b;
                                indexOfPastMatches++;
                            }
                        }
                    }

                    
                    if (dataPacketTime[test] >= currentDateTime.AddMilliseconds(-60750) && dataPacketTime[test] <= currentDateTime.AddMilliseconds(-59250))
                    {
                        indexOfTimeThatMatches = test;
                        counterCloseTimePackets++;
                    }
                    

                }

                //if (counterCloseTimePackets != 9000)
                if(indexOfCurrentMatches == dataPointsCurrent.Length && indexOfPastMatches == dataPointsPast.Length)
                {
                    double boxCarDiffChamberTempPast = 0;
                    double boxCarDiffChamberTempCurrent = 0;
                    double boxCarDiffChamberHumidityPast = 0;
                    double boxCarDiffChamberHumidityCurrent = 0;

                    //Working the averages.
                    for (int x = 0; x < dataPointsCurrent.Length; x++)
                    {
                        //Adding up the ait temp totals.
                        boxCarDiffChamberTempCurrent = boxCarDiffChamberTempCurrent + dataPacketTemp[dataPointsCurrent[x]];
                        boxCarDiffChamberTempPast = boxCarDiffChamberTempPast + dataPacketTemp[dataPointsPast[x]];

                        //Humidity Totals.
                        boxCarDiffChamberHumidityCurrent = boxCarDiffChamberHumidityCurrent + dataPacketHumidity[dataPointsCurrent[x]];
                        boxCarDiffChamberHumidityPast = boxCarDiffChamberHumidityPast + dataPacketHumidity[dataPointsPast[x]];

                    }

                    boxCarDiffChamberTempCurrent = boxCarDiffChamberTempCurrent / dataPointsCurrent.Length;
                    boxCarDiffChamberTempPast = boxCarDiffChamberTempPast / dataPointsCurrent.Length;

                    boxCarDiffChamberHumidityCurrent = boxCarDiffChamberHumidityCurrent / dataPointsCurrent.Length;
                    boxCarDiffChamberHumidityPast = boxCarDiffChamberHumidityPast / dataPointsCurrent.Length;

                    CalibrationStatus.currentDiffChamberTemp = boxCarDiffChamberTempPast - boxCarDiffChamberTempCurrent;
                    CalibrationStatus.currentDiffChamberHumidity = boxCarDiffChamberHumidityPast - boxCarDiffChamberHumidityCurrent;

                    CalibrationStatus.currentRawDiffChamberTemp = dataPacketTemp[indexOfTimeThatMatches] - dataPacketTemp[counterCurrentPacket];
                    CalibrationStatus.currentRawDiffChamberHumidity = dataPacketHumidity[indexOfTimeThatMatches] - dataPacketHumidity[counterCurrentPacket];


                    try
                    {
                        BeginInvoke(new updateTUDiff(updateChamberDifferance), new object[] { true });
                    }
                    catch
                    { }
                }
                else
                {
                    try
                    {
                        BeginInvoke(new updateTUDiff(updateChamberDifferance), new object[] { false });
                    }
                    catch { }
                }

                //Indexing counter to the next pack in the array.
                counterCurrentPacket++;
            }
        }

        void backgroundWorkerChamberReferanceDifferance_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            updateMainStatusLabel("Referacne sensor differance stopped.");
        }

        #endregion

        #endregion

        #region Method for alerting the user.

        public void alertUser(string subject, string message, int level)
        {
            Boolean messageError = false;
            if (CurrentUserSettings.alertAudio)
            {
                
            }

            if (CurrentUserSettings.alertTxtMessage)
            {
                messageError = textMessage.alertTextMessageSend(CurrentUserSettings.alertTxtMessageNumber, CurrentUserSettings.alertTxtMessageCarrier, subject, message, ProgramSettings.OutgoingAddress,
                    ProgramSettings.OutgoingServer, ProgramSettings.OutgoingPort, ProgramSettings.OutgoingUsername, ProgramSettings.OutgoingPassword, ProgramSettings.OutgoingSSL);
            }

            if (CurrentUserSettings.alertEmail)
            {

            }

            //Reporting error if it happens.
            if (!messageError)
            {
                updateCalibrationStatusLabel("Unable to message user.");
                logProgramEvent("Unable to message user. - " + CurrentUserSettings.CurrentUser);
            }

        }

        #endregion

        #region Methods for controling the internal envorment

        public void prepThunder(double currentATDiff)
        {
            //Making sure that the this doesn't run right when the cal starts.
            TimeSpan timeFromStart = DateTime.Now - CalibrationStatus.calibrationStartTime;

            if (timeFromStart.TotalMinutes >= 10)
            {

                if (CalibrationStatus.currentCalibrationPoint < ProgramSettings.calibrationSetPoint.Length / 22)
                {
                    try
                    {
                        if (Math.Abs(currentATDiff) < 1 && ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 15] == "GEN")
                        {
                            if (!CalibrationStatus.prepThunder)
                            {
                                CalibrationStatus.prepThunder = true;

                                //Setting the humidity gen

                                commandHumidityGenerator(Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 12]),
                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 13]),
                                    Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 14]),
                                    ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 15]);

                                updateCalibrationStatusLabel("Preping Thunder to (SATUR psi: " + ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 12] +
                                    ")(SATIR °C: " + ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 13] +
                                    ")(Flow: " + ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 14] + ")");
                                logProgramEvent("Preping Thunder to (SATUR psi: " + ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 12] +
                                    ")(SATIR °C: " + ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 13] +
                                    ")(Flow: " + ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint + 1, 14] + ")");
                                
                            }
                        }
                    }
                    catch (Exception errorThunderPrep)
                    {
                        logProgramEvent("Thunder: " + errorThunderPrep.Message);
                    }
                }
            }
        }

        private bool commandValve(int currentCalStep)
        {
            switch (ProgramSettings.humiditySource)
            {
                case "Thunder":
                    string valveFirst = "";
                    string valveSecond = "";
                    string valveThird = "";

                    // Valve A = Nitrogen Feed
                    // Valve B = Humidity Feed
                    // Valve C = Humidity Vent


                    //Setting the order of the change.
                    if (ProgramSettings.calibrationSetPoint[currentCalStep, 19] == "A")
                    {
                        valveFirst = ProgramSettings.calibrationSetPoint[currentCalStep, 16];
                        valveSecond = ProgramSettings.calibrationSetPoint[currentCalStep, 17];
                        valveThird = ProgramSettings.calibrationSetPoint[currentCalStep, 18];
                    }
                    else
                    {
                        valveFirst = ProgramSettings.calibrationSetPoint[currentCalStep, 18];
                        valveSecond = ProgramSettings.calibrationSetPoint[currentCalStep, 17];
                        valveThird = ProgramSettings.calibrationSetPoint[currentCalStep, 16];
                    }

                    //Depending on mode to change the valves.
                    switch (ProgramSettings.modeValve)
                    {
                        case "manual": //Manual mode. This will create an onscreen message of what needs to be done and send a txt meassage out to the user.


                            //Sending out text message...
                            alertUser("Valve Change Needed.", "Set valves in the following order:\n\nWARNING THESE VALVES MUST BE SET IN THIS ORDER\n\n" +
                                "Valve " + ProgramSettings.calibrationSetPoint[currentCalStep, 19] + ": " + valveFirst + "\n" +
                                "Valve " + ProgramSettings.calibrationSetPoint[currentCalStep, 20] + ": " + valveSecond + "\n" +
                                "Valve " + ProgramSettings.calibrationSetPoint[currentCalStep, 21] + ": " + valveThird, 0);

                            //Sending message to the screen.
                            MessageBox.Show("Set valves in the following order:\n\nWARNING THESE VALVES MUST BE SET IN THIS ORDER\n\n" +
                            "Valve " + ProgramSettings.calibrationSetPoint[currentCalStep, 19] + ": " + valveFirst + "\n" +
                            "Valve " + ProgramSettings.calibrationSetPoint[currentCalStep, 20] + ": " + valveSecond + "\n" +
                            "Valve " + ProgramSettings.calibrationSetPoint[currentCalStep, 21] + ": " + valveThird,
                            "Set Valves Alert", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            break;

                        case "auto": //Auto mode will control two valves one 2-way for the nitrogen one 3-way for the humidity.
                            //Closing both valve
                            resetValves();

                            //Setting Valves
                            relayController.commandRelayBoard("nitrogenValve", Convert.ToBoolean(ProgramSettings.calibrationSetPoint[currentCalStep, 16])); 
                            relayController.commandRelayBoard("humidityGenValve", Convert.ToBoolean(ProgramSettings.calibrationSetPoint[currentCalStep, 17]));

                            break;

                    }
                    break;
                case "Thermotron":
                    break;
            }

            return true;

        }

        public void resetValves()
        {
            switch (ProgramSettings.humiditySource)
            {
                case "Thunder":
                    //Closing both valve
                    relayController.commandRelayBoard("humidityGenValve", false);
                    relayController.commandRelayBoard("nitrogenValve", false);
                    break;

                case "Thermotron":
                    break;
            }
        }


        #endregion

        #region Methods for processing calibration data and loading radiosonds.

        private void calulateCoefficentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                calcCoefficients(null, null);
            }
            catch(Exception error)
            {
                updateMainStatusLabel(error.Message);
            }
        }

        private void hMP234ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //User to select the desired referance file.
            OpenFileDialog refernaceFile = new OpenFileDialog();
            refernaceFile.Title = "Calibration Referance File";
            refernaceFile.InitialDirectory = ProgramSettings.RAWFileLOC + "\\";
            refernaceFile.Filter = "Cal Files (*.u_ref)|HMP*.u_ref|All files (*.*)|*.*";

            DialogResult referanceReturn = refernaceFile.ShowDialog();
            if (referanceReturn == DialogResult.OK)
            {
                correctHumidityDataFile(refernaceFile.FileName, "HMP234");
            }
            else
            {
                MessageBox.Show("No Referance Files Provided. Canceling.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }


        private void correctReferanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            string filePath = Application.ExecutablePath.ToString();
            string[] pBreakDown = filePath.Split('\\');
            filePath = "";
            for (int p = 0; p < pBreakDown.Length - 1; p++)
            {
                filePath += pBreakDown[p] + "\\";
            }

            string[] refCoef = System.IO.File.ReadAllLines(filePath + "refSensorCorrection.coef");

            correctReferanceToolStripMenuItem.DropDownItems.Clear();



            foreach (string cof in refCoef)
            {
                if (cof.Contains("EE31") || cof.Contains("HMP"))
                {

                    System.Windows.Forms.ToolStripMenuItem option = new ToolStripMenuItem();
                    option.Name = cof;
                    option.Text = cof;
                    option.Click += new EventHandler(option_Click);

                    correctReferanceToolStripMenuItem.DropDownItems.Add(option);
                }
            }

            
        }

        void option_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolStripMenuItem incomingMenuItem = sender as System.Windows.Forms.ToolStripMenuItem;

            //User to select the desired referance file.
            OpenFileDialog refernaceFile = new OpenFileDialog();
            refernaceFile.Title = "Calibration Referance File";
            refernaceFile.InitialDirectory = ProgramSettings.RAWFileLOC + "\\";
            refernaceFile.Filter = "Cal Files (*.u_ref)|*.u_ref|All files (*.*)|*.*";

            DialogResult referanceReturn = refernaceFile.ShowDialog();
            if (referanceReturn == DialogResult.OK)
            {
                correctHumidityDataFile(refernaceFile.FileName, incomingMenuItem.Text);
            }
            else
            {
                MessageBox.Show("No Referance Files Provided. Canceling.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        public string[,] calcCoefficients(string[] sondeFilesToProcess, string referanceToCalc)
        {
      
            //If no sonde files provided ask the user to provide the sonde names.
            string workingDir = "";
            if (sondeFilesToProcess == null)
            {
                //User to select desired radiosonde files
                OpenFileDialog sondeFiles = new OpenFileDialog();
                sondeFiles.Title = "Radiosonde Calibration Files";
                sondeFiles.Multiselect = true;
                sondeFiles.InitialDirectory = ProgramSettings.RAWFileLOC;
                sondeFiles.Filter = "Raw Files (*.u_raw)|*.u_raw|All files (*.*)|*.*";

                DialogResult sondeFilesToCalc = sondeFiles.ShowDialog();
                if (sondeFilesToCalc == DialogResult.OK)
                {
                    sondeFilesToProcess = sondeFiles.FileNames;
                    string[] workingDirElement = sondeFilesToProcess[0].Split('\\');
                    workingDir = workingDirElement[2] + "\\";
                }
                else
                {
                    MessageBox.Show("No Radiosonde Files Provided. Canceling.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return null ;
                }

            }

            //Process Report
            string[,] report = new string[sondeFilesToProcess.Length, 4];


            //If no referance file ask the user to provide one.
            if (referanceToCalc == null)
            {
                //User to select the desired referance file.
                OpenFileDialog refernaceFile = new OpenFileDialog();
                refernaceFile.Title = "Calibration Referance File";
                refernaceFile.InitialDirectory = ProgramSettings.RAWFileLOC + "\\" + workingDir;
                refernaceFile.Filter = "Cal Files (*.u_ref)|*.u_ref|All files (*.*)|*.*";

                DialogResult referanceReturn = refernaceFile.ShowDialog();
                if (referanceReturn == DialogResult.OK)
                {
                    referanceToCalc = refernaceFile.FileName;
                }
                else
                {
                    MessageBox.Show("No Referance Files Provided. Canceling.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return report;
                }
            }

            Update();

            object[] localCoefData = new object[sondeFilesToProcess.Length];
            int counter = 0; //Counting the loops
            foreach (string radiosondes in sondeFilesToProcess)
            {
                updateMainStatusLabel("Calculating " + radiosondes);
                Update();
                try
                {
                    //calc for calibration in tenny
                    double[] tempCoefficients = calculatorHumidity.getEECoefficients(referanceToCalc, radiosondes);

                    //Calc for calibraiton in Thermotron
                    //double[] tempCoefficients = calculatorHumidity.getEECoefficientsNoLow(referanceToCalc, radiosondes);
                    localCoefData[counter] = (object)tempCoefficients;
                    calculatorHumidity.writeEECalFiles(tempCoefficients, radiosondes.Remove(radiosondes.Length - 5, 5) + "u_cal");
                    
                    //Getting overall Error
                    double[] humidityError = calculatorHumidity.analyzeEECalibration(tempCoefficients, referanceToCalc, radiosondes);
                    double errorStDev = calculatorHumidity.calculateStDev(humidityError);
                    double errorMean = calculatorHumidity.calculateMean(humidityError);
                    report[counter, 0] = radiosondes;
                    report[counter, 1] = "True";
                    report[counter, 2] = errorStDev.ToString("0.0000");
                    report[counter, 3] = errorMean.ToString("0.0000");

                }
                catch
                {
                    report[counter, 0] = radiosondes;
                    report[counter, 1] = "False";
                    report[counter, 2] = "0";
                    report[counter, 3] = "0";
                }
                counter++;
            }

            //Logging results
            string[] coefReport = new string[report.Length + 2];
            coefReport[0] = (report.Length/4).ToString() + " Coefficient Files created.";
            coefReport[1] = "Raw File,Pass,StDev,Mean";
            //Looping to fill in the rest.
            for (int r = 2; r < coefReport.Length/4; r++)
            {
                coefReport[r] = report[r - 2, 0] + "," + report[r - 2, 1] + "," + report[r - 2, 2] + "," + report[r - 2, 3];
            }

            //Writing log for this calc.
            System.IO.File.WriteAllLines( CalibrationStatus.currentOverallLog + "-Coefficient Log.csv", coefReport);
            

            //Writing data to a master calc log file
            if(!System.IO.File.Exists(ProgramSettings.RAWFileLOC + "\\Master Calc Log.csv"))
            {
                System.IO.File.AppendAllText(ProgramSettings.RAWFileLOC + "\\Master Calc Log.csv","Date,Time,Name,Raw File,Pass,StDev,Mean,Coef1,Coef2,Coef3,Coef4,Coef5\n");
            }

            for (int r = 0; r < report.Length / 4; r++)
            {
                //extracting the coef data from the storage object.
                double[] tempCoef = (double[])localCoefData[r];

                //Sorting out the serial number / Tracking number of the radiosonde calibrated.
                string[] tempNameArray = report[r,0].Split('\\');
                string tempName = tempNameArray[tempNameArray.Length - 1];
                tempName = tempName.Substring(1, tempName.Length - 7);

                System.IO.File.AppendAllText(ProgramSettings.RAWFileLOC + "\\Master Calc Log.csv" ,DateTime.Now.ToString("yyyyMMdd") + "," + DateTime.Now.ToString("HH:mm:ss.ff") + "," +
                    tempName + "," + report[r, 0] + "," + report[r, 1] + "," + report[r, 2] + "," + report[r, 3] + "," +
                    tempCoef[0] + "," + tempCoef[1] + "," + tempCoef[2] + "," + tempCoef[3] + "," + tempCoef[4] + "\n");
            }

            //Updating screen
            updateMainStatusLabel("Coeffients calculations complete.");

            //Returning report
            return report;
        }

        private string[] checkCalibrationData(string dirLoc)
        {
            //Making sure the dirLoc is formatted corrctly.
            if (dirLoc.Substring(dirLoc.Length - 1, 1) != "\\")
            {
                dirLoc = dirLoc + "\\";
            }

            string[] rawCalFiles = System.IO.Directory.GetFiles(dirLoc, "*.u_raw");
            List<string> radiosondePassed = new List<string>();

            string[] referanceFile;

            referanceFile = System.IO.Directory.GetFiles(dirLoc, "*.u_ref");
            
            //Getting the percentage of correct data points from all the current Radiosondes.
            analyseCalData.analyseEPlusECalData checkData = new analyseCalData.analyseEPlusECalData();
            object[] precentData = new object[rawCalFiles.Length];

            for (int gCal = 0; gCal < rawCalFiles.Length; gCal++)
            {
                updateMainStatusLabel("Checking Data From: " + rawCalFiles[gCal]);
                Update();
                precentData[gCal] = checkData.checkData2(rawCalFiles[gCal], referanceFile[0], //Files
                ProgramSettings.tolAcceptableAirTempDiff, ProgramSettings.tolHumidityFreqMean, ProgramSettings.tolHumidityFrewPlusMinus); //what to compare to.
                
                //Writing a raw performance file for each radiosonde.
                //logRawRadiosondeCalPerformace(rawCalFiles[gCal], (object[])precentData[gCal]);
                logRawRadiosondeCalPerformace2(rawCalFiles[gCal], (object[])precentData[gCal]);

                //Adding the good sondes into a list.
                object[] data = (object[])precentData[gCal];
                if ((double)data[0] > .8)
                {
                    radiosondePassed.Add(rawCalFiles[gCal]);
                    updateMainStatusLabel(rawCalFiles[gCal] + "(" + Convert.ToDouble(data[0]).ToString("0.000") + ")" + " - Data is good.");
                }
                else
                {
                    updateMainStatusLabel(rawCalFiles[gCal] + "(" + Convert.ToDouble(data[0]).ToString("0.000") + ")" + " - Data is bad.");
                }
                Update();
            }

            return radiosondePassed.ToArray();
        }

        /// <summary>
        /// Method corrects the referance sensor file data collected durring the e+e calibration process.
        /// </summary>
        /// <param name="sensorFile"></param>
        /// <param name="sensorType"></param>
        private void correctHumidityDataFile(string sensorFile, string sensorType)
        {

            string[] fileLines = null;
            string[] corrected = null;


            string filePath = Application.ExecutablePath.ToString();
            string[] pBreakDown = filePath.Split('\\');
            filePath = "";
            for (int p = 0; p < pBreakDown.Length - 1; p++)
            {
                filePath += pBreakDown[p] + "\\";
            }

            
            //Getting and formating the coef for referance
            List<double> coef = new List<double>();

            string[] refCoefLines = System.IO.File.ReadAllLines(filePath + "refSensorCorrection.coef");

            for (int c = 0; c < refCoefLines.Length; c++)
            {
                if (refCoefLines[c].Contains(sensorType))
                {
                    for (int add = 1; add < refCoefLines.Length - c; add++)
                    {
                        if (!refCoefLines[c + add].Contains("#END"))
                        {
                            coef.Add(Convert.ToDouble(refCoefLines[c + add]));
                        }
                        else
                        {
                            break;
                        }
                    }
                }

            }

            //Checking to make sure a correction was found.
            if(coef.Count == 0)
            {
                MessageBox.Show("No Correction was found for this sensor");
                return;
            }



            //Building correcting object and processing raw data.
            sensorCorrection.sensorCorrection corrSensor = new sensorCorrection.sensorCorrection(coef.ToArray());
            fileLines = System.IO.File.ReadAllLines(sensorFile);
            corrected = new string[fileLines.Length];

            for (int i = 0; i < fileLines.Length; i++)
            {
                string activeLine = fileLines[i];

                if (i > 4)
                {
                    string[] elements = activeLine.Split(',');

                    elements[2] = (Convert.ToDouble(elements[2]) + corrSensor.getCorrectedHumidity(Convert.ToDouble(elements[2]))).ToString("0.000");
                    activeLine = elements[0] + "," + elements[1] + "," + elements[2];

                }

                corrected[i] = activeLine;
            }

            //Writing processed data to corrected file.
            System.IO.File.WriteAllLines(sensorFile.Substring(0, sensorFile.Length - 6) + "-Corrected.u_ref", corrected);
            
            /*
            switch (sensorType)
            {
                case "HMP234":
                    fileLines = System.IO.File.ReadAllLines(sensorFile);
                    corrected = new string[fileLines.Length];

                    List<double> coef = new List<double>();
                    
                    string[] refCoefLines = System.IO.File.ReadAllLines(filePath + "refSensorCorrection.coef");

                    for (int c = 0; c < refCoefLines.Length; c++)
                    {
                        if (refCoefLines[c].Contains("HMP233"))
                        {
                            for (int add = 1; add < refCoefLines.Length - c; add++)
                            {
                                if (!refCoefLines[c + add].Contains("#END"))
                                {
                                    coef.Add(Convert.ToDouble(refCoefLines[c + add]));
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }


                    sensorCorHMP234Blk correctHMP = new sensorCorHMP234Blk();
                    correctHMP.setCoefficient(coef.ToArray());

                    for (int i = 0; i < fileLines.Length; i++)
                    {
                        string activeLine = fileLines[i];

                        if (i > 4)
                        {
                            string[] elements = activeLine.Split(',');

                            elements[2] = (Convert.ToDouble(elements[2]) + correctHMP.getCorrectedHumidity(Convert.ToDouble(elements[2]))).ToString("0.000");
                            activeLine = elements[0] + "," + elements[1] + "," + elements[2];

                        }

                        corrected[i] = activeLine;
                    }



                    System.IO.File.WriteAllLines(sensorFile.Substring(0,sensorFile.Length-6) + "-Corrected.u_ref", corrected);

                    break;

                case "EPlusE":
                    fileLines = System.IO.File.ReadAllLines(sensorFile);
                    corrected = new string[fileLines.Length];

                    List<double> coef2 = new List<double>();
                    string[] refCoefLines2 = System.IO.File.ReadAllLines(filePath + "refSensorCorrection.coef");

                    for (int c = 0; c < refCoefLines2.Length; c++)
                    {
                        if (refCoefLines2[c].Contains("EE31-PFTD9025/AB3-T52 SN:1035931303996A"))
                        {
                            for (int add = 1; add < refCoefLines2.Length - c; add++)
                            {
                                if (!refCoefLines2[c + add].Contains("#END"))
                                {
                                    coef2.Add(Convert.ToDouble(refCoefLines2[c + add]));
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }



                    sensorCorEE31 correctEPlusE = new sensorCorEE31();
                    correctEPlusE.setCoefficient(coef2.ToArray());

                    for (int i = 0; i < fileLines.Length; i++)
                    {
                        string activeLine = fileLines[i];

                        if (i > 4)
                        {
                            string[] elements = activeLine.Split(',');

                            elements[2] = (Convert.ToDouble(elements[2]) + correctEPlusE.getCorrectedHumidity(Convert.ToDouble(elements[2]))).ToString("0.000");
                            activeLine = elements[0] + "," + elements[1] + "," + elements[2];

                        }

                        corrected[i] = activeLine;
                    }



                    System.IO.File.WriteAllLines(sensorFile.Substring(0, sensorFile.Length - 6) + "-Corrected.u_ref", corrected);
                    break;
            }

            
            */

            
        }

        #endregion

        #region These methods for for collecting data about the calibration for a reprot

        void startProformanceLog()
        {
            backgroundWorkerProformanceLog = new BackgroundWorker();
            backgroundWorkerProformanceLog.WorkerSupportsCancellation = true;
            backgroundWorkerProformanceLog.DoWork += new DoWorkEventHandler(backgroundWorkerProformanceLog_DoWork);
            backgroundWorkerProformanceLog.RunWorkerAsync();
        }

        void backgroundWorkerProformanceLog_DoWork(object sender, DoWorkEventArgs e)
        {
            string currentLogFileName = CalibrationStatus.currentOverallLog + "-ProformanceLog.csv";
            double currentAirTemp = 0;
            double currentHumidity = 0;
            double currentChamberAT = 0;
            double currentChamberU = 0;
            double currentATDiff = 0;
            double currentUDiff = 0;


            while (!backgroundWorkerProformanceLog.CancellationPending && CalibrationStatus.currentProgramMode == "running")
            {
                DateTime logtime = DateTime.Now;
                if (File.Exists(currentLogFileName) == false)
                {
                    // create a writer and open the file
                    TextWriter currentLoggingCheck = File.AppendText(currentLogFileName);


                    // write a line of text to the file
                    currentLoggingCheck.WriteLine("Date,Time,Elapsed Time,Chamber ATSP,Chamber AT,EnclosureATSP,Enclosure AT,Enclosure USP,Enclosure U,AT Diff, U Diff");

                    // close the stream
                    currentLoggingCheck.Close();

                }

                //Setting up and collecting all the data need form sensors.
                double[] incomingRef = getReferanceData(false);
                currentAirTemp = incomingRef[0];
                currentHumidity = incomingRef[1];

                /*
                switch (ProgramSettings.calibrationReferanceSensor)
                {
                    case "HMP234":
                        currentAirTemp = referanceHMP234.CurrentTemp;
                        currentHumidity = referanceHMP234.CurrentHumidity;
                        break;

                    case "EPlusE":
                        currentAirTemp = referanceEE31.currentAirTemp;
                        currentHumidity = referanceEE31.currentHumidity;
                        break;
                }
                */

                switch (ProgramSettings.ChamberController)
                {
                    case "Watlow":
                        currentChamberAT = chamberWatlow.ProcessCurrentTemp(chamberWatlow.currentRawAirTemp);
                        currentChamberU = 0;
                        break;

                    case "Thermotron":
                        currentChamberAT = chamberThermotron.CurrentChamberC;
                        currentChamberU = chamberThermotron.CurrentChamberRH;
                        break;
                }

                try
                {
                    currentATDiff = Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 1]) - currentAirTemp;
                    currentUDiff = Convert.ToDouble(ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 8]) - currentHumidity;
                }
                catch
                { }

                TimeSpan elapseTime = logtime - CalibrationStatus.calibrationStartTime;


                // create a writer and open the file
                TextWriter currentLogging = File.AppendText(currentLogFileName);

                try
                {
                    currentLogging.WriteLine(logtime.ToString("yyyyMMdd") + "," + logtime.ToString("HH:mm:ss.fff") + "," + elapseTime.TotalSeconds.ToString() + "," +
                        ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 4] + "," +
                        currentChamberAT.ToString() + "," +
                        ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 1] + "," +
                        currentAirTemp.ToString() + "," +
                        ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 8] + "," +
                        currentHumidity.ToString() + "," + currentATDiff.ToString() + "," + currentUDiff.ToString());
                }
                catch
                { }

                //Closing the stream.
                currentLogging.Close();

                //Taking a pause
                System.Threading.Thread.Sleep(1000);
            }
        }

        #endregion

        #region Methods tied to controls

        /// <summary>
        /// Method sets the type of radiosonde that is going to be used.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxRadiosondeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProgramSettings.radiosondeType = comboBoxRadiosondeType.SelectedItem.ToString();
        }

        #endregion



        #region These are test/junk methods and should be removed at the end of development

        #region Data Grid controls

        private void dataGridViewRadiosondes_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e) //Datagrid controls.
        {
            if (ValidSondeArryiMet1 != null)
            {
                switch (e.ColumnIndex)
                {
                    case 1:
                        updateCurrentSelectedRadiosonde(e.RowIndex);
                        break;
                }
            }
        }

        private void dataGridViewRadiosondes_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                MessageBox.Show("This is a test.");

            }
        }

        #endregion

        private void chamberControlsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string value= "";
            Bill.Interfaces.InputBox("Set Air Temp","Input air temp:",ref value);

            ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 4] = value;
            updateEnviromentStabilyElements();
            //commandChamber(Convert.ToDouble(value));
            setChamberATSetPoint(Convert.ToDouble(value));


            //Logging the change.
            logProgramEvent("Setting Chamber to: " + ProgramSettings.calibrationSetPoint[CalibrationStatus.currentCalibrationPoint, 4].ToString());
           

        }

        private void thunderControlsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string value = "";
            //Bill.Interfaces.InputBox("Set Thunder RH", "Input RH:", ref value);

            
            commandHumidityGenerator(10, 17, 5, "GEN");

        }


        public BackgroundWorker backgroundWorkerTest;
        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] listOfRadiosondes = null;

            listOfRadiosondes = checkCalibrationData(@"G:\EPlusECalData\20130531-1150\30 Records");

            //Calculating Coeficents
            string[,] calReport = calcCoefficients(listOfRadiosondes, @"G:\EPlusECalData\20130531-1150\30 Records" + "\\" + "HMP234-Corrected.u_ref");

            calibrationReport(@"G:\EPlusECalData\20130531-1150\30 Records\");
        }

        void backgroundWorkerTest_DoWork(object sender, DoWorkEventArgs e)
        {
            commandHumidityGenerator(17, 17, 0.1, "STP");

            /*
            string value = "";
            Bill.Interfaces.InputBox("Sat Pressure", "Input", ref value);
            commandHumidityGenerator(Convert.ToDouble(value), 17, 5, "STP");
             */ 
        }

        private void changeUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Current User doing the work form.
            string value = "";
            while (value == "")
            {
                if (Bill.Interfaces.InputBox("User Name", "Your Name:", ref value) == DialogResult.OK)
                {
                    CurrentUserSettings.CurrentUser = value;
                    updateMainStatusLabel("Loaded... " + "Current User: " + value);

                    if (!loadUserSettingsFromFile(value))
                    {
                        MessageBox.Show("User Not Found. Please check settings and try again.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
        }

        private void thunderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerHumidityGenerator.IsBusy)
            {
                stopHumidityGen();
            }

            if (!backgroundWorkerHumidityGenerator.IsBusy)
            {
                startHumidityGen();
            }
        }

        private void envStableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            backgroundWorkerEnviromentStability.CancelAsync();
        }

        private void startLoggingProformanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!backgroundWorkerProformanceLog.IsBusy)
            {
                //This method starts logging proformance data manuely.
                CalibrationStatus.calibrationStartTime = DateTime.Now;
                CalibrationStatus.currentOverallLog = ProgramSettings.RAWFileLOC + DateTime.Now.ToString("yyyyMMdd-HHmm") + "-Env-Test";
                startProformanceLog();
            }
            else
            {
                MessageBox.Show("Performance Log Error. Logging already started.", "Performance Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void relayControlsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commandValve(0);
        }

        #endregion

        private void calcCorrectChamberTempToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Code for test to make sure the chamber air correction is correct.

            string value = "";
            Bill.Interfaces.InputBox("Desired Enclouser Temp", "Enter Desired Enclouser Temp:", ref value);
            value = correctChamberAirTempNeeded(Convert.ToDouble(value)).ToString();
            MessageBox.Show("Corrected Chamber Temp: " + value, "Corrected Chamber Temp");
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startRadiosondeCollection();
            stopToolStripMenuItem.Enabled = true;
            startToolStripMenuItem.Enabled = false;
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                stopRadiosondeCollection();
            }
            catch
            { }

            stopToolStripMenuItem.Enabled = false;
            startToolStripMenuItem.Enabled = true;
        }

        private void checkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog calFolder = new FolderBrowserDialog();
            //calFolder.SelectedPath = ProgramSettings.RAWFileLOC;
            calFolder.Description = "Calibration Run Folder";
            DialogResult results = calFolder.ShowDialog();

            if (results == DialogResult.OK)
            {
                string check = calFolder.SelectedPath.ToString();
                string[] testingCalData = checkCalibrationData(check);

                //Compling message
                string totalMessage = "";
                for (int i = 0; i < testingCalData.Length; i++)
                {
                    totalMessage += testingCalData[i] + "\n";
                }

                /*
                //Sending message to screen
                DialogResult createReport = MessageBox.Show("Check Data Report\n" + totalMessage,"Data Report",MessageBoxButtons.YesNo,MessageBoxIcon.Information,MessageBoxDefaultButton.Button2);

                //If they want a report file making one.
                if (createReport == DialogResult.Yes)
                {
                    System.IO.File.WriteAllLines(ProgramSettings.RAWFileLOC + "\\" + DateTime.Now.ToString("yyyyMMdd-HHmm") + "-Data Check Report.txt", testingCalData);
                }
                
                */

                calibrationReport(check);

            }

            
            
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void labelChamerRelay_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (ProgramSettings.ChamberController == "Watlow")
            {
                string mode = "";
                if (tennyChamberRelay.systemState.relayState)
                {
                    mode = "Running";
                }
                else
                {
                    mode = "Stopped";
                }

                DialogResult changeMode = MessageBox.Show("Chamber is currently " + mode + ". Do you wish to change it?", "Chamber Relay State", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (changeMode == DialogResult.Yes)
                {
                    if (tennyChamberRelay.systemState.relayState)
                    {
                        tennyChamberRelay.triggerRelay(false);
                    }
                    else
                    {
                        tennyChamberRelay.triggerRelay(true);
                    }
                }
            }


            if (ProgramSettings.ChamberController == "Thermotron")
            {
                if (DialogResult.Yes == MessageBox.Show("Start Chamber", "Chamber", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    chamberThermotron.startChamberManualMode();
                }
                
            }

        }

    }
}
